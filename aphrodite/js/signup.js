(function($) {
    "use strict";
    /*==================================================================
    [ validateSignup ]*/
    var input = $('.validateSignup-input .input100Signup');

    $('.validateSignup-form').on('submit', function() {
        var check = true;

        for (var i = 0; i < input.length; i++) {
            if (validateSignup(input[i]) == false) {
                showvalidateSignup(input[i]);
                check = false;
            }
        }

        return check;
    });

    $('.validateSignup-form .input100Signup').each(function() {
        $(this).focus(function() {
            hidevalidateSignup(this);
        });
    });

    function validateSignup(input) {
        if ($(input).attr('name') == 'username' || $(input).attr('type') == 'text') {
            if ($(input).val().trim() == '') {
                return false;
            }
        } else if ($(input).attr('type') == 'password') {
             if ($(input).val().trim() == '') {
                return false;
            }
        }
    }

    function showvalidateSignup(input) {
        var thisAlert = $(input).parent();
        $(thisAlert).addClass('alert-validateSignup');
    }

    function hidevalidateSignup(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('alert-validateSignup');
    }



})(jQuery);