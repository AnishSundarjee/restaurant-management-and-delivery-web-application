<?php
session_start();
$date = date(DATE_ATOM);

//Setup database connectivity
$host="127.0.0.1";
$user="root";
$password="password";
$db="aphroditedb";
$dbPort = "3306";

$connect=mysqli_connect($host,$user,$password,$db,$dbPort);

function cleanData($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
if(! $connect){
    die('Could not connect connect: ') ;
}

$json_array = array();
if(isset($_SESSION["userName"])){
    $userId = cleanData($_SESSION["userName"]);
    $GetOrderDetailsSp = "CALL spGetOrderDetails('$userId');"; 
    if($retvalue = mysqli_query($connect,$GetOrderDetailsSp)){
        // print_r($retvalue);
        while($row=mysqli_fetch_assoc($retvalue)){
            $json_array[]=$row;
        }
    }
    if(! $retvalue){
        echo mysqli_error($connect);
    }
    $jsonResponse = json_encode($json_array);
}
else {header("location: ../controller/logout.php");}

    // echo '<pre>';
    // print_r($json_array);
    // echo '</pre>';

if(isset($_GET['isSignUp'])){
    $SignInValue=$_GET['isSignUp'];
    if($SignInValue=='0'){
       //echo "<script type='text/javascript'>alert('User account already exists, Please try to login. $SignInValue');</script>";
        $errorReceived = "User account already exists, Please try to login.";
        $SignupSuccessReceived = "";
    }
    else if($SignInValue=='1'){
        $SignupSuccessReceived = "User account has been created, Please try to login.";
        $errorReceived = "";
    }
    else if($SignInValue=='2'){
        $errorReceived = "User account already exists, Please try to login.";
        $SignupSuccessReceived = "";
    }
    else if($SignInValue=='3'){
        echo "<script type='text/javascript'>alert('Something went wrong with your request, please try again. $SignInValue');</script>";
        $errorReceived = "Email already exists, Please use another email ID.";
        $SignupSuccessReceived = "";
    }
    else if($SignInValue=='4'){
        echo "<script type='text/javascript'>alert('Password mismatch $SignInValue');</script>";
        $errorReceived = "Password mismatch.";
        $SignupSuccessReceived = "";
    }
    else if($SignInValue=='5'){
        echo "<script type='text/javascript'>alert('Some values are missing $SignInValue');</script>";
        $errorReceived = "Some values are missing.";
        $SignupSuccessReceived = "";
    }
    else{
        echo "<script type='text/javascript'>alert('Something went wrong in your SignUp process, Please try again.');</script>";
        $errorReceived = "Something went wrong in your SignUp process, Please try again.";
        $SignupSuccessReceived = "";
    }
}

if(isset($_SESSION["isLoggedIn"])){
    $isLogedin = $_SESSION["isLoggedIn"];
    if($isLogedin == '0'){
        $errorReceived = "User does not exist.";
        $SignupSuccessReceived = "";
    }
    else if($isLogedin == '1'){
        $errorReceived = "User Account is inactive.";
        $SignupSuccessReceived = "";
    }
    else if($isLogedin == '2'){
       $errorReceived = "";
       $SignupSuccessReceived = "";
    }
    else if($isLogedin == '3'){
        $errorReceived = "Incorrect password.";
        $SignupSuccessReceived = "";
    }
}

?>

<!doctype html>
<html class="no-js" lang="zxx">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Order Tracking || Afredieti</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Favicons -->
	<link rel="shortcut icon" href="../images/favicon.ico">
	<link rel="apple-touch-icon" href="../images/icon.png">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/plugins.css">
	<link rel="stylesheet" href="../style.css">
	<!-- Cusom css -->
   <link rel="stylesheet" href="../css/custom.css">
   <script src="../js/vendor/jquery-3.2.1.min.js"></script>
   <!-- Data tables CDNs -->
   <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.20/datatables.min.css"/>
   <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.20/datatables.min.js"></script>


	<!-- Modernizer js -->
	<script src="../js/vendor/modernizr-3.5.0.min.js"></script>
</head>
<body>
<!-- Main wrapper -->
<div class="wrapper" id="wrapper">
    <!-- Start Header Area -->
    <header class="htc__header bg--white">
        <!-- Start Mainmenu Area -->
        <div id="sticky-header-with-topbar" class="mainmenu__wrap sticky__header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-sm-4 col-md-6 order-1 order-lg-1">
                        <div class="logo">
                            <a href="index.php">
                                <img src="../images/logo/foody.png" alt="logo images">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-9 col-sm-4 col-md-2 order-3 order-lg-2">
                        <div class="main__menu__wrap">
                            <nav class="main__menu__nav d-none d-lg-block">
                                <ul class="mainmenu">
                                    <li class="drop"><a href="index.php">Home</a></li>
                                    <li><a href="about-us.php">About</a></li>
                                    <li class="drop"><a href="#">Menu Options</a>
                                        <ul class="dropdown__menu">
                                            <li><a href="menu-list.php">Menu List</a></li>
                                            <li><a href="resturantView.php">Business List</a></li>
                                        </ul>
                                    </li>
                                    
                                    <?php
                                        if(isset($isLogedin)){
                                            if($isLogedin == '2') {
                                            ?>
                                                <li><a href="Cart.php">Cart</a></li>
                                            <?php
                                            }
                                        }
                                    ?>
                                    <?php
                                        if(isset($isLogedin)){
                                            if($isLogedin == '2') {
                                            ?>
                                                <li><a href="orderTracking.php">Order Tracking</a></li>
                                            <?php
                                            }
                                        }
                                    ?>
                                    
                                    <li><a href="contact.php">Contact</a></li>
                                    <?php
                                        if(isset($isLogedin)){
                                            if($isLogedin == '2') {
                                            ?>
                                                <li><a href="setupPayment.php">Profile</a></li>
                                            <?php
                                            }
                                        }
                                    ?>
                                </ul>
                            </nav>
                            
                        </div>
                    </div>
                    <div class="col-lg-1 col-sm-4 col-md-4 order-2 order-lg-3">
                        <div class="header__right d-flex justify-content-end">
                            <?php
                                if(isset($isLogedin)){
                                    if($isLogedin == '2') {
                                    ?>
                                        <div class="log__in">
                                            <a href="../controller/logout.php">Logout  <i class="fa fa-close"></i></a>
                                        </div>
                                        <div class="shopping__cart">
                                            <a href="cart.php"><i class="zmdi zmdi-shopping-basket"></i></a>
                                            <?php
                                            if(isset($cartCountValue)){
                                                if($cartCountValue>0){
                                                    ?>
                                                    <div class="shop__qun">
                                                        <span><?php echo $cartCountValue;?></span>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>
                                    <?php
                                    }
                                    else{
                                        ?>
                                            <div class="log__in">
                                                <a class="accountbox-trigger" href="#">SignIn/SignUp  <i class="zmdi zmdi-account-o"></i></a>
                                            </div>
                                        <?php
                                    }
                                }
                                else{
                                    ?>
                                        <div class="log__in">
                                            <a class="accountbox-trigger" href="#">SignIn/SignUp<i class="zmdi zmdi-account-o"></i></a>
                                        </div>
                                    <?php
                                }
                            ?>
                            
                        </div>
                    </div>
                </div>
                <!-- Mobile Menu -->
                <div class="mobile-menu d-block d-lg-none"></div>
                <!-- Mobile Menu -->
            </div>
        </div>
        <!-- End Mainmenu Area -->
    </header>
    <!-- End Header Area -->
    <!-- Start Bradcaump area -->
    <div class="ht__bradcaump__area bg-image--28">
    <h5 style="color:#e60000;"><?php echo $errorReceived; ?></h5>
    <h5 style="color:green;"><?php echo $SignupSuccessReceived; ?></h5>
        <div class="ht__bradcaump__wrap d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="bradcaump__inner text-center brad__white">
                            <h2 class="bradcaump-title">Order Tracking</h2>
                            <nav class="bradcaump-inner">
                                <a class="breadcrumb-item" href="index.php">Home</a>
                                <span class="brd-separetor"><i class="zmdi zmdi-long-arrow-right"></i></span>
                                <span class="breadcrumb-item active">Order Tracking</span>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Bradcaump area -->
    <!-- Start Section area -->
    <section class="food__service bg--white section-padding--lg">
            <div class="container">
                <div class="row">
                    <!-- Start Order Tracking area -->
                    <div class="wrap-input100 validate-input" data-validate="Valid Order ID is required: eg.1">
                        <input class="input100" type="text" name="myInput" id="myInput" onkeyup="_searchByOrderId()" placeholder="Search for Order No..">
                        <span class="focus-input100"></span>
                        <span class="symbol-input100">
                            <i class="fa fa-search" aria-hidden="true"></i>
                        </span>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-condensed" id="mydata">
                            <thead>
                                <tr>
                                    <th>OrderId</th>  
                                    <th>DeliveryMethod</th>  
                                    <th>Business Name</th>  
                                    <th>Business Address</th>  
                                    <th>Building Description</th>  
                                    <th>RoomNo</th>  
                                    <th>Delivery Time</th>  
                                    <th>Delivery Date</th>  
                                    <th>Meal Name</th>  
                                    <th>Quantity</th>  
                                    <th>Total Price</th>  
                                    <th>Order Status</th>  
                                    <th>Action</th>  
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <!-- End Order Tracking area -->
                </div>
            </div>
    </section>
    <!-- End Section area -->
    <!-- Start Footer Area -->
    <footer class="footer__area footer--1">
        <div class="footer__wrapper bg__cat--1 section-padding--lg">
            <div class="container">
                <div class="row">
                    <!-- Start Single Footer -->
                    <div class="col-md-6 col-lg-4 col-sm-12">
                        <div class="footer">
                            <h2 class="ftr__title">About Afredieti</h2>
                            <div class="footer__inner">
                                <div class="ftr__details">
                                    <p>The true plessures in life begin with a single food delivery,</p>
                                    <div class="ftr__address__inner">
                                        <div class="ftr__address">
                                            <div class="ftr__address__icon">
                                                <i class="zmdi zmdi-home"></i>
                                            </div>
                                            <div class="frt__address__details">
                                                <p>USP. ICT building TL3 Suva, Fiji</p>
                                            </div>
                                        </div>
                                        <div class="ftr__address">
                                            <div class="ftr__address__icon">
                                                <i class="zmdi zmdi-phone"></i>
                                            </div>
                                            <div class="frt__address__details">
                                                <p><a href="#">+679 555-5555</a></p>
                                                <p><a href="#">+679 555-5555</a></p>
                                            </div>
                                        </div>
                                        <div class="ftr__address">
                                            <div class="ftr__address__icon">
                                                <i class="zmdi zmdi-email"></i>
                                            </div>
                                            <div class="frt__address__details">
                                                <p><a href="#">bigboyvamps540@gmail.com</a></p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <ul class="social__icon">
                                        <li><a href="#"><i class="zmdi zmdi-facebook"></i></a></li>
                                        <li><a href="#"><i class="zmdi zmdi-google"></i></a></li>
                                        <li><a href="#"><i class="zmdi zmdi-instagram"></i></a></li>
                                        <li><a href="#"><i class="zmdi zmdi-rss"></i></a></li>
                                    </ul> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Single Footer -->
                    <!-- Start Single Footer -->
                    <div class="col-md-6 col-lg-4 col-sm-12 sm--mt--40">
                        <div class="footer gallery">
                            <h2 class="ftr__title">Our Gallery</h2>
                            <div class="footer__inner">
                                <ul class="sm__gallery__list">
                                        <li><a href="#"><img src="../images/gallery/sm-img/7.jpg" alt="gallery images"></a></li>
                                        <li><a href="#"><img src="../images/gallery/sm-img/8.jpg" alt="gallery images"></a></li>
                                        <li><a href="#"><img src="../images/gallery/sm-img/9.jpg" alt="gallery images"></a></li>
                                        <li><a href="#"><img src="../images/gallery/sm-img/10.png" alt="gallery images"></a></li>
                                        <li><a href="#"><img src="../images/gallery/sm-img/11.jpg" alt="gallery images"></a></li>
                                        <li><a href="#"><img src="../images/gallery/sm-img/12.jpg" alt="gallery images"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- End Single Footer -->
                    <!-- Start Single Footer -->
                    <div class="col-md-6 col-lg-4 col-sm-12 md--mt--40 sm--mt--40">
                        <div class="footer">
                            <h2 class="ftr__title">Opening Time</h2>
                            <div class="footer__inner">
                                <ul class="opening__time__list">
                                    <li>Monday<span>.......</span>9am to 11pm</li>
                                    <li>Tuesday<span>.......</span>9am to 11pm</li>
                                    <li>Wednesday<span>.......</span>9am to 11pm</li>
                                    <li>Thursday<span>.......</span>9am to 11pm</li>
                                    <li>Friday<span>.......</span>9am to 11pm</li>
                                    <li>Saturday<span>.......</span>9am to 11pm</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- End Single Footer -->
                </div>
            </div>
        </div>
        <div class="copyright bg--theme">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="copyright__inner">
                            <div class="cpy__right--left">
                                <p>@All Right Reserved.<a href="https://freethemescloud.com/">Free themes Cloud</a></p>
                            </div>
                            <!-- <div class="cpy__right--right">
                                <a href="#">
                                    <img src="../images/icon/shape/2.png" alt="payment images">
                                </a>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- End Footer Area -->
    <!-- Login Form -->
    <div class="accountbox-wrapper">
        <div class="accountbox text-left">
            <ul class="nav accountbox__filters" id="myTab" role="tablist">
                <li>
                    <a class="active" id="log-tab" data-toggle="tab" href="#log" role="tab" aria-controls="log" aria-selected="true">Login</a>
                </li>
                <li>
                    <a id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Register</a>
                </li>
            </ul>
            <div class="accountbox__inner tab-content" id="myTabContent">
                <div class="accountbox__login tab-pane fade show active" id="log" role="tabpanel" aria-labelledby="log-tab">
                    <form class="login100-form validate-form ajax" id="loginform" name="loginform" method="post" action="../controller/login.php">

                        <div class="wrap-input100 validate-input" data-validate="Valid username is required: Employee/Student ID">
                            <input class="input100" type="text" name="username" id="username" placeholder="Username" required>
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
                                <i class="fa fa-user" aria-hidden="true"></i>
                            </span>
                        </div>

                        <div class="wrap-input100 validate-input" data-validate="Password is required">
                            <input class="input100" type="password" name="userPassword" id="userPassword" placeholder="Password" required>
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
                                <i class="fa fa-lock" aria-hidden="true"></i>
                            </span>
                        </div>
                        <div class="single-input">
                            <button type="submit" class="food__btn" ><span>LogIn</span></button>
                        </div>
                    </form>
                </div>
                <div class="accountbox__register tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <form class="login100-form validate-form ajax" id="signupform" name="signupform" method="post" action="../controller/signup.php">
                        <div class="wrap-input100 validate-input" data-validate="Valid FistName is required: John">
                            <input class="input100" type="text" name="fistName" id="fistName" placeholder="FistName" >
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
                                <i class="fa fa-user" aria-hidden="true"></i>
                            </span>
                        </div>
                        <div class="wrap-input100 validate-input" data-validate="Valid SurName is required: Doe">
                            <input class="input100" type="text" name="surName" id="surName" placeholder="SurName" required>
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
                                <i class="fa fa-user" aria-hidden="true"></i>
                            </span>
                        </div>
                        <div class="wrap-input100 validate-input" data-validate="Valid EmployeeID/StudentID is required: s1111111">
                            <input class="input100" type="text" name="userID" id="userID" placeholder="EmployeeID/StudentID" required>
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
                                <i class="fa fa-user" aria-hidden="true"></i>
                            </span>
                        </div>
                        <div class="wrap-input100 validate-input" data-validate="Valid Email is required: example@example.com">
                            <input class="input100" type="text" name="email" id="email" placeholder="Email" required>
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                            </span>
                        </div>
                        <div class="wrap-input100 validate-input" data-validate="Valid Phone Number is required: 555 5555">
                            <input class="input100" type="text" name="phoneNumber" id="phoneNumber" placeholder="phone Number" required>
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                            </span>
                        </div>
                        <div class="wrap-input100 validate-input" data-validate="Password is required">
                            <input class="input100" type="password" name="password" id="password" placeholder="Password" required>
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
                                <i class="fa fa-lock" aria-hidden="true"></i>
                            </span>
                        </div>
                        <div class="wrap-input100 validate-input" data-validate="Password is required">
                            <input class="input100" type="password" name="confrimPassword" id="confrimPassword" placeholder="Confirm Password" required >
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
                                <i class="fa fa-lock" aria-hidden="true"></i>
                            </span>
                        </div>
                        <label style="margin-vertical: 1em;color:#000;">Select Default Delivery Location</label>
                        <div class="row">
                            <div class="col-md-6 col-12 mb--20">                                 
                                <select name="DeliverySelector">
                                    <?php
                                    if($retvalue = mysqli_query($connect,$getDeliveryLocationList)){
                                        while($row=mysqli_fetch_array($retvalue)){
                                            ?> <option  value="<?php echo $row["BuildingId"];?>"><?php echo $row["BuildingDescription"] ?></option><?php
                                        }
                                    }
                                    mysqli_close($connect);
                                    ?>
                                </select>
                            </div>
                            <div class="wrap-input100 validate-input col-md-6 col-12 mb--20" data-validate="Valid Building Number is required.">
                                <input style="border-radius: 20;" type="text" name="roomNo" id="roomNo" placeholder="Room No." required>
                                <span class="focus-input100"></span>
                            </div>
                        </div>
                        <label style="margin-bottom: 1em;color:#000;">Select Gender *</label>
                            <input style="margin-right: 1.5em;" type="radio" id="radioMale" name="demo01" value="M" checked="checked"/><label for="radioMale">Male</label>
                            <input style="margin-right: 1.5em;" type="radio" id="radio0Female" name="demo01" value="F" /><label for="radio0Female">Female</label>
                        <div class="single-input">
                            <button type="submit" class="food__btn"><span>Sign Up</span></button>
                        </div>
                    </form>
                </div>
                <span class="accountbox-close-button"><i class="zmdi zmdi-close"></i></span>
            </div>
        </div>
    </div><!-- //Login Form -->
</div>

    <!-- JS Files -->
	<script src="../js/popper.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/plugins.js"></script>
    <script src="../js/active.js"></script>
    <script src="../js/login.js"></script>

    <script>  
        $(document).ready(function(){
            var jsonResponse = <?php echo $jsonResponse ?>;
            //declare array parse php into it
            var table=$('#mydata').dataTable( {
                searching: false,
                data: jsonResponse,
                order: [[ 0, "desc" ]],
                keys: {
                    clipboard: true
                },
                columns     :     [  
                    {     data     :     "OrderId" },  
                    {     data     :     "DeliveryMethod" },  
                    {     data     :     "BusinessName" },  
                    {     data     :     "BusinessAddress" },  
                    {     data     :     "BuildingDescription" },  
                    {     data     :     "RoomNo" },  
                    {     data     :     "TimeslotDescription" },  
                    {     data     :     "DeliveryDate" },  
                    {     data     :     "ItemName" },  
                    {     data     :     "OrderItemQuantity" },  
                    {     data     :     "OrderTotalPrice", render: $.fn.dataTable.render.number(',', '.', 2, '')},  
                    {     data     :     "OrderStatusDescription" }, 
                    {     data     :     "MainOrderId" ,render: function(data) {
                    return '<a class="btn btn-sm  btn-danger btnCopy" id="OrderId" style="margin-left:10px;font-size:10px;" onclick="_disputeOrder(\''+data+'\')"> Dispute</a>'
                        }
                    },  
                ],
            });
        });  
    </script> 

    <script>
        function _searchByOrderId() {
            // Declare variables
            var input, filter, table, tr, td, i, txtValue;
            input = document.getElementById("myInput");
            filter = input.value.toUpperCase();
            table = document.getElementById("mydata");
            tr = table.getElementsByTagName("tr");

            // Loop through all table rows, and hide those who don't match the search query
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[0];
                if (td) {
                    txtValue = td.textContent || td.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }
        
    </script>

    <script>
        function _disputeOrder (el) { 
            $.ajax({
            url: '../controller/cancelOrder.php',
            type: 'POST',
            data: {
                orderNum: el
            },
            success: function(msg) {
                if(msg==1){
                    alert('Selected Order has been succesfully deleted FREE OF CHARGE'); 
                }
                else if(msg==2){
                    alert('Selected order has been deleted, With no refund.');
                }
                else {
                    alert(msg);
                }
                window.location.href="orderTracking.php";
            }               
        });
        }
    </script>
</body>
</html>
