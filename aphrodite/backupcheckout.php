<?php
    session_start();

    //Setup database connectivity
    $host="127.0.0.1";
    $user="root";
    $password="password";
    $db="aphroditedb";
    $dbPort = "3306";

    $connect=mysqli_connect($host,$user,$password,$db,$dbPort);


    if(! $connect){
        die('Could not connect connect: ') ;
      }

        $getItemList = "CALL spFetchItemList();";
        $getDeliveryLocationList = "SELECT * FROM building_info;";
        $json_array = array();
        // if($retvalue = mysqli_query($connect,$getItemList)){
        //   //print_r($retvalue);
        //   while($row=mysqli_fetch_assoc($retvalue)){
        //       $json_array[]=$row;
        //   }
        // $jsonResponse= json_encode($json_array);
        // $file = 'test.json';
        // // Write the contents back to the file
        // file_put_contents($file, $jsonResponse);
        // }  
        // echo '<pre>';
        // print_r($json_array);
        // echo '</pre>';
        // if(! $retvalue){
        //   die('Cannot connect to SQL: ');
        // }
      

    if(isset($_GET['isSignUp'])){
        $SignInValue=$_GET['isSignUp'];
        if($SignInValue=='0'){
            echo "<script type='text/javascript'>alert('User account already exists, Please try to login. $SignInValue');</script>";
        }
        else if($SignInValue=='1'){
            echo "<script type='text/javascript'>alert('User account has been created, Please try to login. $SignInValue');</script>";
        }
        else if($SignInValue=='2'){
            echo "<script type='text/javascript'>alert('Email already exists, Please use another email ID. $SignInValue');</script>";
        }
        else if($SignInValue=='3'){
            echo "<script type='text/javascript'>alert('Something went wrong with your request, please try again. $SignInValue');</script>";
        }
        else if($SignInValue=='4'){
            echo "<script type='text/javascript'>alert('Password mismatch $SignInValue');</script>";
        }
        else if($SignInValue=='5'){
            echo "<script type='text/javascript'>alert('Some values are missing $SignInValue');</script>";
        }
        else{
            echo "<script type='text/javascript'>alert('Something went wrong in your SignUp process, Please try again.');</script>";
        }
    }
    
    if(isset($_SESSION["isLoggedIn"])){
        $isLogedin = $_SESSION["isLoggedIn"];
        if($isLogedin == '0'){
            echo "<script type='text/javascript'>alert('User does not exist. $isLogedin');</script>";
        }
        else if($isLogedin == '1'){
            echo "<script type='text/javascript'>alert('User Account is inactive. $isLogedin');</script>";
        }
        else if($isLogedin == '2'){
           // echo "<script type='text/javascript'>alert('Successfull login. $isLogedin');</script>";
        }
        else if($isLogedin == '3'){
            echo "<script type='text/javascript'>alert('Incorrect password. $isLogedin');</script>";
        }
    }

    if(isset($_SESSION["cartItemCount"])){
        $cartCountValue=$_SESSION["cartItemCount"];
    }
    
?>
<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Checkout ||  Aahar Food Delivery Html5 Template</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicons -->
    <link rel="shortcut icon" href="images/favicon.ico">
    <link rel="apple-touch-icon" href="images/icon.png">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/plugins.css">
    <link rel="stylesheet" href="style.css">

    <!-- Cusom css -->
   <link rel="stylesheet" href="css/custom.css">

    <!-- Modernizer js -->
    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
</head>
<body>
    <!--[if lte IE 9]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
    <![endif]-->

    <!-- Add your site or application content here -->
    
    <!-- <div class="fakeloader"></div> -->

    <!-- Main wrapper -->
    <div class="wrapper" id="wrapper">
        <!-- Start Header Area -->
        <header class="htc__header bg--white">
            <!-- Start Mainmenu Area -->
            <div id="sticky-header-with-topbar" class="mainmenu__wrap sticky__header">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-2 col-sm-4 col-md-6 order-1 order-lg-1">
                            <div class="logo">
                                <a href="index.php">
                                    <img src="images/logo/foody.png" alt="logo images">
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-9 col-sm-4 col-md-2 order-3 order-lg-2">
                            <div class="main__menu__wrap">
                                <nav class="main__menu__nav d-none d-lg-block">
                                    <ul class="mainmenu">
                                        <li class="drop"><a href="index.php">Home</a></li>
                                        <li><a href="about-us.php">About</a></li>
                                        <li class="drop"><a href="#">Menu Options</a>
                                            <ul class="dropdown__menu">
                                                <li><a href="menu-list.php">Menu List</a></li>
                                                <li><a href="resturantView.php">Business List</a></li>
                                            </ul>
                                        </li>
                                        
                                        <?php
                                            if(isset($isLogedin)){
                                                if($isLogedin == '2') {
                                                ?>
                                                    <li class="drop"><a href="#">Cart Options</a>
                                                        <ul class="dropdown__menu">
                                                            <li><a href="cart.php">Cart Page</a></li>
                                                            <li><a href="checkout.php">Checkout Page</a></li>
                                                        </ul>
                                                    </li>
                                                <?php
                                                }
                                            }
                                        ?>
                                        
                                        <li><a href="contact.php">Contact</a></li>
                                    </ul>
                                </nav>
                                
                            </div>
                        </div>
                        <div class="col-lg-1 col-sm-4 col-md-4 order-2 order-lg-3">
                            <div class="header__right d-flex justify-content-end">
                                <?php
                                    if(isset($isLogedin)){
                                        if($isLogedin == '2') {
                                        ?>
                                            <div class="log__in">
                                                <a href="controller/logout.php">Logout  <i class="fa fa-close"></i></a>
                                            </div>
                                            <div class="shopping__cart">
                                                <a href="cart.php"><i class="zmdi zmdi-shopping-basket"></i></a>
                                                <?php
                                                if(isset($cartCountValue)){
                                                    if($cartCountValue>0){
                                                        ?>
                                                        <div class="shop__qun">
                                                            <span><?php echo $cartCountValue;?></span>
                                                        </div>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </div>
                                        <?php
                                        }
                                        else{
                                            ?>
                                                <div class="log__in">
                                                    <a class="accountbox-trigger" href="#">SignIn/SignUp  <i class="zmdi zmdi-account-o"></i></a>
                                                </div>
                                            <?php
                                        }
                                    }
                                    else{
                                        ?>
                                            <div class="log__in">
                                                <a class="accountbox-trigger" href="#">SignIn/SignUp<i class="zmdi zmdi-account-o"></i></a>
                                            </div>
                                        <?php
                                    }
                                ?>
                                
                            </div>
                        </div>
                    </div>
                    <!-- Mobile Menu -->
                    <div class="mobile-menu d-block d-lg-none"></div>
                    <!-- Mobile Menu -->
                </div>
            </div>
            <!-- End Mainmenu Area -->
        </header>
        <!-- End Header Area -->
        <!-- Start Bradcaump area -->
        <div class="ht__bradcaump__area bg-image--18">
            <div class="ht__bradcaump__wrap d-flex align-items-center">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="bradcaump__inner text-center">
                                <h2 class="bradcaump-title">Checkout</h2>
                                <nav class="bradcaump-inner">
                                  <a class="breadcrumb-item" href="index.php">Home</a>
                                  <span class="brd-separetor"><i class="zmdi zmdi-long-arrow-right"></i></span>
                                  <span class="breadcrumb-item active">Checkout</span>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Bradcaump area --> 
         <section class="htc__checkout bg--white section-padding--lg">
            <!-- Checkout Section Start-->
            <div class="checkout-section">
                <div class="container">
                    <div class="row">
                       
                        <div class="col-lg-6 col-12 mb-30">
                               
                                <!-- Checkout Accordion Start -->
                                <div id="checkout-accordion">
                                   
                                    <!-- Checkout Method -->
                                    <div class="single-accordion">
                                        <a class="accordion-head" data-toggle="collapse" data-parent="#checkout-accordion" href="#checkout-method">1. checkout method</a>
                                        
                                        <div id="checkout-method" class="collapse">
                                            <div class="checkout-method accordion-body fix">
                                               
                                                <ul class="checkout-method-list">
                                                    <li class="active" data-form="checkout-login-form">Login</li>
                                                    <li data-form="checkout-register-form">Register</li>
                                                </ul>
                                                
                                                <form action="#" class="checkout-login-form">
                                                    <div class="row">
                                                        <div class="input-box col-md-6 col-12 mb--20"><input type="email" placeholder="Email Address"></div>
                                                        <div class="input-box col-md-6 col-12 mb--20"><input type="password" placeholder="Password"></div>
                                                        <div class="input-box col-12"><input type="submit" value="Login"></div>
                                                    </div>
                                                </form>
                                                
                                                <form action="#" class="checkout-register-form">
                                                    <div class="row">
                                                        <div class="input-box col-md-6 col-12 mb--20"><input type="text" placeholder="Your Name"></div>
                                                        <div class="input-box col-md-6 col-12 mb--20"><input type="email" placeholder="Email Address"></div>
                                                        <div class="input-box col-md-6 col-12 mb--20"><input type="password" placeholder="Password"></div>
                                                        <div class="input-box col-md-6 col-12 mb--20"><input type="password" placeholder="Confirm Password"></div>
                                                        <div class="input-box col-12"><input type="submit" value="Register"></div>
                                                    </div>
                                                </form>
                                                
                                            </div>
                                        </div>
                                        
                                    </div>
                                    
                                    <!-- Billing Method -->
                                    <div class="single-accordion">
                                        <a class="accordion-head collapsed" data-toggle="collapse" data-parent="#checkout-accordion" href="#billing-method">2. billing informatioon</a>
                                        <div id="billing-method" class="collapse show">

                                            <div class="accordion-body billing-method fix">

                                                <form action="#" class="billing-form checkout-form">
                                                    <div class="row">
                                                        <div class="col-12 mb--20">
                                                            <select>
                                                              <option value="1">Select a country</option>
                                                              <option value="2">bangladesh</option>
                                                              <option value="3">Algeria</option>
                                                              <option value="4">Afghanistan</option>
                                                              <option value="5">Ghana</option>
                                                              <option value="6">Albania</option>
                                                              <option value="7">Bahrain</option>
                                                              <option value="8">Colombia</option>
                                                              <option value="9">Dominican Republic</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-6 col-12 mb--20">                                 
                                                            <input type="text" placeholder="First Name">
                                                        </div>
                                                        <div class="col-md-6 col-12 mb--20">                             
                                                            <input type="text" placeholder="Last Name">
                                                        </div>
                                                        <div class="col-12 mb--20">                              
                                                            <input type="text" placeholder="Company Name">
                                                        </div>
                                                        <div class="col-12 mb--20">
                                                            <input placeholder="Street address" type="text">
                                                        </div>
                                                        <div class="col-12 mb--20">
                                                            <input placeholder="Apartment, suite, unit etc. (optional)" type="text">
                                                        </div>
                                                        <div class="col-12 mb--20">
                                                            <input placeholder="Town / City" type="text">
                                                        </div>
                                                        <div class="col-md-6 col-12 mb--20">                                 
                                                            <input type="text" placeholder="State / County">
                                                        </div>
                                                        <div class="col-md-6 col-12 mb--20">                                 
                                                            <input placeholder="Postcode / Zip" type="text">
                                                        </div>
                                                        <div class="col-md-6 col-12">                                 
                                                            <input type="email" placeholder="Email Address">
                                                        </div>
                                                        <div class="col-md-6 col-12">                                   
                                                            <input placeholder="Phone Number" type="text">
                                                        </div>                          
                                                    </div>
                                                </form>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <!-- Shipping Method -->
                                    <div class="single-accordion">
                                        <a class="accordion-head collapsed" data-toggle="collapse" data-parent="#checkout-accordion" href="#shipping-method">3. shipping informatioon</a>
                                        <div id="shipping-method" class="collapse">
                                            <div class="accordion-body shipping-method fix">
                                               
                                                <h5>shipping address</h5>
                                                <p><span>address&nbsp;</span>Bootexperts, Banasree D-Block, Dhaka 1219, Bangladesh</p>
                                                
                                                <button class="shipping-form-toggle">Ship to a different address?</button>
                                                
                                                <form action="#" class="shipping-form checkout-form">
                                                    <div class="row">
                                                        <div class="col-12 mb--20">
                                                            <select>
                                                              <option value="1">Select a country</option>
                                                              <option value="2">bangladesh</option>
                                                              <option value="3">Algeria</option>
                                                              <option value="4">Afghanistan</option>
                                                              <option value="5">Ghana</option>
                                                              <option value="6">Albania</option>
                                                              <option value="7">Bahrain</option>
                                                              <option value="8">Colombia</option>
                                                              <option value="9">Dominican Republic</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-6 col-12 mb--20">                                 
                                                            <input type="text" placeholder="First Name">
                                                        </div>
                                                        <div class="col-md-6 col-12 mb--20">                             
                                                            <input type="text" placeholder="Last Name">
                                                        </div>
                                                        <div class="col-12 mb--20">                              
                                                            <input type="text" placeholder="Company Name">
                                                        </div>
                                                        <div class="col-12 mb--20">
                                                            <input placeholder="Street address" type="text">
                                                        </div>
                                                        <div class="col-12 mb--20">
                                                            <input placeholder="Apartment, suite, unit etc. (optional)" type="text">
                                                        </div>
                                                        <div class="col-12 mb--20">
                                                            <input placeholder="Town / City" type="text">
                                                        </div>
                                                        <div class="col-md-6 col-12 mb--20">                                 
                                                            <input type="text" placeholder="State / County">
                                                        </div>
                                                        <div class="col-md-6 col-12 mb--20">                                 
                                                            <input placeholder="Postcode / Zip" type="text">
                                                        </div>
                                                        <div class="col-md-6 col-12">                                 
                                                            <input type="email" placeholder="Email Address">
                                                        </div>
                                                        <div class="col-md-6 col-12">                                   
                                                            <input placeholder="Phone Number" type="text">
                                                        </div>                          
                                                    </div>
                                                </form>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <!-- Payment Method -->
                                    <div class="single-accordion">
                                        <a class="accordion-head collapsed" data-toggle="collapse" data-parent="#checkout-accordion" href="#payment-method">4. Payment method</a>
                                        <div id="payment-method" class="collapse">
                                            <div class="accordion-body payment-method fix">
                                               
                                                <ul class="payment-method-list">
                                                    <li class="active">check / money order</li>
                                                    <li class="payment-form-toggle">credit card</li>
                                                </ul>
                                                
                                                <form action="#" class="payment-form">
                                                    <div class="row">
                                                        <div class="input-box col-12 mb--20">
                                                            <label for="card-name">Name on Card *</label>
                                                            <input type="text" id="card-name" />
                                                        </div>
                                                        <div class="input-box col-12 mb--20">
                                                            <label>Credit Card Type</label>
                                                            <select>
                                                                <option>Please Select</option>
                                                                <option>Credit Card Type 1</option>
                                                                <option>Credit Card Type 2</option>
                                                            </select>
                                                        </div>
                                                        <div class="input-box col-12 mb--20">
                                                            <label for="card-number">Credit Card Number *</label>
                                                            <input type="text" id="card-number" />
                                                        </div>
                                                        <div class="input-box col-12">
                                                            <div class="row">
                                                                <div class="input-box col-12">
                                                                    <label>Expiration Date</label>
                                                                </div>
                                                                <div class="input-box col-md-6 col-12 mb--20">
                                                                    <select>
                                                                        <option>Month</option>
                                                                        <option>Jan</option>
                                                                        <option>Feb</option>
                                                                        <option>Mar</option>
                                                                        <option>Apr</option>
                                                                        <option>May</option>
                                                                        <option>Jun</option>
                                                                        <option>Jul</option>
                                                                        <option>Aug</option>
                                                                        <option>Sep</option>
                                                                        <option>Oct</option>
                                                                        <option>Nov</option>
                                                                        <option>Dec</option>
                                                                    </select>
                                                                </div>
                                                                <div class="input-box col-md-6 col-12 mb--20">
                                                                    <select>
                                                                        <option>Year</option>
                                                                        <option>2015</option>
                                                                        <option>2016</option>
                                                                        <option>2017</option>
                                                                        <option>2018</option>
                                                                        <option>2019</option>
                                                                        <option>2020</option>
                                                                        <option>2021</option>
                                                                        <option>2022</option>
                                                                        <option>2023</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="input-box col-12">
                                                            <label for="card-Verify">Card Verification Number *</label>
                                                            <input type="text" id="card-Verify" />
                                                            <a href="#">What is it ?</a>
                                                        </div>
                                                    </div>
                                                </form>
                                            
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div><!-- Checkout Accordion Start -->
                            </div>
                            
                            <!-- Order Details -->
                            <div class="col-lg-6 col-12 mb-30">
                               
                                <div class="order-details-wrapper">
                                    <h2>your order</h2>
                                    <div class="order-details">
                                        <form action="#">
                                            <ul>
                                                <li><p class="strong">product</p><p class="strong">total</p></li>
                                                <li><p>Fishing Reel x1</p><p>$104.99</p></li>
                                                <li><p>Fishing Rods x1 </p><p>$85.99</p></li>
                                                <li><p class="strong">cart subtotal</p><p class="strong">$190.98</p></li>
                                                <li><p class="strong">shipping</p><p>
                                                    <input type="radio" name="order-shipping" id="flat" /><label for="flat">Flat Rate $ 7.00</label><br />
                                                    <input type="radio" name="order-shipping" id="free" /><label for="free">Free Shipping</label>
                                                </p></li>
                                                <li><p class="strong">order total</p><p class="strong">$190.98</p></li>
                                                <li><button class="food__btn">place order</button></li>
                                            </ul>
                                        </form>
                                    </div>
                                </div>
                                
                            </div>
                        
                    </div>
                </div>
            </div><!-- Checkout Section End-->             
         </section>   
        <!-- Start Footer Area -->
        <footer class="footer__area footer--1">
            <div class="footer__wrapper bg__cat--1 section-padding--lg">
                <div class="container">
                    <div class="row">
                        <!-- Start Single Footer -->
                        <div class="col-md-6 col-lg-4 col-sm-12">
                            <div class="footer">
                                <h2 class="ftr__title">About Afredieti</h2>
                                <div class="footer__inner">
                                    <div class="ftr__details">
                                        <p>The true plessures in life begin with a single food delivery,</p>
                                        <div class="ftr__address__inner">
                                            <div class="ftr__address">
                                                <div class="ftr__address__icon">
                                                    <i class="zmdi zmdi-home"></i>
                                                </div>
                                                <div class="frt__address__details">
                                                    <p>USP. ICT building TL3 Suva, Fiji</p>
                                                </div>
                                            </div>
                                            <div class="ftr__address">
                                                <div class="ftr__address__icon">
                                                    <i class="zmdi zmdi-phone"></i>
                                                </div>
                                                <div class="frt__address__details">
                                                    <p><a href="#">+679 555-5555</a></p>
                                                    <p><a href="#">+679 555-5555</a></p>
                                                </div>
                                            </div>
                                            <div class="ftr__address">
                                                <div class="ftr__address__icon">
                                                    <i class="zmdi zmdi-email"></i>
                                                </div>
                                                <div class="frt__address__details">
                                                    <p><a href="#">bigboyvamps540@gmail.com</a></p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <ul class="social__icon">
                                            <li><a href="#"><i class="zmdi zmdi-facebook"></i></a></li>
                                            <li><a href="#"><i class="zmdi zmdi-google"></i></a></li>
                                            <li><a href="#"><i class="zmdi zmdi-instagram"></i></a></li>
                                            <li><a href="#"><i class="zmdi zmdi-rss"></i></a></li>
                                        </ul> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Footer -->
                        <!-- Start Single Footer -->
                        <div class="col-md-6 col-lg-4 col-sm-12 sm--mt--40">
                            <div class="footer gallery">
                                <h2 class="ftr__title">Our Gallery</h2>
                                <div class="footer__inner">
                                    <ul class="sm__gallery__list">
                                        <li><a href="#"><img src="../images/gallery/sm-img/7.jpg" alt="gallery images"></a></li>
                                        <li><a href="#"><img src="../images/gallery/sm-img/8.jpg" alt="gallery images"></a></li>
                                        <li><a href="#"><img src="../images/gallery/sm-img/9.jpg" alt="gallery images"></a></li>
                                        <li><a href="#"><img src="../images/gallery/sm-img/10.png" alt="gallery images"></a></li>
                                        <li><a href="#"><img src="../images/gallery/sm-img/11.jpg" alt="gallery images"></a></li>
                                        <li><a href="#"><img src="../images/gallery/sm-img/12.jpg" alt="gallery images"></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Footer -->
                        <!-- Start Single Footer -->
                        <div class="col-md-6 col-lg-4 col-sm-12 md--mt--40 sm--mt--40">
                            <div class="footer">
                                <h2 class="ftr__title">Opening Time</h2>
                                <div class="footer__inner">
                                    <ul class="opening__time__list">
                                        <li>Saturday<span>.......</span>9am to 11pm</li>
                                        <li>Sunday<span>.......</span>9am to 11pm</li>
                                        <li>Monday<span>.......</span>9am to 11pm</li>
                                        <li>Tuesday<span>.......</span>9am to 11pm</li>
                                        <li>Wednesday<span>.......</span>9am to 11pm</li>
                                        <li>Thursday<span>.......</span>9am to 11pm</li>
                                        <li>Friday<span>.......</span>9am to 11pm</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Footer -->
                       
                    </div>
                </div>
            </div>
            <div class="copyright bg--theme">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="copyright__inner">
                                <div class="cpy__right--left">
                                    <p>@All Right Reserved.<a href="https://freethemescloud.com/">Free themes Cloud</a></p>
                                </div>
                                <div class="cpy__right--right">
                                    <a href="#">
                                        <img src="images/icon/shape/2.png" alt="payment images">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- End Footer Area -->
        <!-- Login Form -->
        <div class="accountbox-wrapper">
            <div class="accountbox text-left">
                <ul class="nav accountbox__filters" id="myTab" role="tablist">
                    <li>
                        <a class="active" id="log-tab" data-toggle="tab" href="#log" role="tab" aria-controls="log" aria-selected="true">Login</a>
                    </li>
                    <li>
                        <a id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Register</a>
                    </li>
                </ul>
                <div class="accountbox__inner tab-content" id="myTabContent">
                    <div class="accountbox__login tab-pane fade show active" id="log" role="tabpanel" aria-labelledby="log-tab">
                        <form class="login100-form validate-form ajax" id="loginform" name="loginform" method="post" action="controller/login.php">

                            <div class="wrap-input100 validate-input" data-validate="Valid username is required: Employee/Student ID">
                                <input class="input100" type="text" name="username" id="username" placeholder="Username" >
                                <span class="focus-input100"></span>
                                <span class="symbol-input100">
                                    <i class="fa fa-user" aria-hidden="true"></i>
                                </span>
                            </div>

                            <div class="wrap-input100 validate-input" data-validate="Password is required">
                                <input class="input100" type="password" name="userPassword" id="userPassword" placeholder="Password" >
                                <span class="focus-input100"></span>
                                <span class="symbol-input100">
                                    <i class="fa fa-lock" aria-hidden="true"></i>
                                </span>
                            </div>
                            <div class="single-input">
                                <button type="submit" class="food__btn" ><span>LogIn</span></button>
                            </div>
                        </form>
                    </div>
                    <div class="accountbox__register tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <form class="login100-form validate-form ajax" id="signupform" name="signupform" method="post" action="controller/signup.php">
                            <div class="wrap-input100 validate-input" data-validate="Valid FistName is required: John">
                                <input class="input100" type="text" name="fistName" id="fistName" placeholder="FistName" >
                                <span class="focus-input100"></span>
                                <span class="symbol-input100">
                                    <i class="fa fa-user" aria-hidden="true"></i>
                                </span>
                            </div>
                            <div class="wrap-input100 validate-input" data-validate="Valid SurName is required: Doe">
                                <input class="input100" type="text" name="surName" id="surName" placeholder="SurName" >
                                <span class="focus-input100"></span>
                                <span class="symbol-input100">
                                    <i class="fa fa-user" aria-hidden="true"></i>
                                </span>
                            </div>
                            <div class="wrap-input100 validate-input" data-validate="Valid EmployeeID/StudentID is required: s1111111">
                                <input class="input100" type="text" name="userID" id="userID" placeholder="EmployeeID/StudentID" >
                                <span class="focus-input100"></span>
                                <span class="symbol-input100">
                                    <i class="fa fa-user" aria-hidden="true"></i>
                                </span>
                            </div>
                            <div class="wrap-input100 validate-input" data-validate="Valid Email is required: example@example.com">
                                <input class="input100" type="text" name="email" id="email" placeholder="Email" >
                                <span class="focus-input100"></span>
                                <span class="symbol-input100">
                                    <i class="fa fa-envelope" aria-hidden="true"></i>
                                </span>
                            </div>
                            <div class="wrap-input100 validate-input" data-validate="Valid Phone Number is required: 555 5555">
                                <input class="input100" type="text" name="phoneNumber" id="phoneNumber" placeholder="phone Number" >
                                <span class="focus-input100"></span>
                                <span class="symbol-input100">
                                    <i class="fa fa-phone" aria-hidden="true"></i>
                                </span>
                            </div>
                            <div class="wrap-input100 validate-input" data-validate="Password is required">
                                <input class="input100" type="password" name="password" id="password" placeholder="Password" >
                                <span class="focus-input100"></span>
                                <span class="symbol-input100">
                                    <i class="fa fa-lock" aria-hidden="true"></i>
                                </span>
                            </div>
                            <div class="wrap-input100 validate-input" data-validate="Password is required">
                                <input class="input100" type="password" name="confrimPassword" id="confrimPassword" placeholder="Confirm Password" >
                                <span class="focus-input100"></span>
                                <span class="symbol-input100">
                                    <i class="fa fa-lock" aria-hidden="true"></i>
                                </span>
                            </div>
                            <label style="margin-vertical: 1em;color:#000;">Select Default Delivery Location</label>
                            <div class="row">
                                <div class="col-md-6 col-12 mb--20">                                 
                                    <select>
                                       <?php
                                       if($retvalue = mysqli_query($connect,$getDeliveryLocationList)){
                                            while($row=mysqli_fetch_array($retvalue)){
                                                ?> <option value="<?php echo $row["BuildingId"];?>"><?php echo $row["BuildingDescription"] ?></option><?php
                                            }
                                        }
                                        mysqli_close($connect);
                                       ?>
                                    </select>
                                </div>
                                <div class="wrap-input100 validate-input col-md-6 col-12 mb--20" data-validate="Valid Building Number is required.">
                                    <input style="border-radius: 20;" type="text" name="BuildingNo" id="BuildingNo" placeholder="Building No." >
                                    <span class="focus-input100"></span>
                                </div>
                            </div>
                            <label style="margin-bottom: 1em;color:#000;">Select Gender *</label>
                                <input style="margin-right: 1.5em;" type="radio" id="radioMale" name="demo01" value="M" checked="checked"/><label for="radioMale">Male</label>
                                <input style="margin-right: 1.5em;" type="radio" id="radio0Female" name="demo01" value="F" /><label for="radio0Female">Female</label>
                            <div class="single-input">
                                <button type="submit" class="food__btn"><span>Sign Up</span></button>
                            </div>
                        </form>
                    </div>
                    <span class="accountbox-close-button"><i class="zmdi zmdi-close"></i></span>
                </div>
            </div>
        </div><!-- //Login Form -->
    </div><!-- //Main wrapper -->

    <!-- JS Files -->
    <script src="js/vendor/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/active.js"></script>
    <script src="js/login.js"></script>
</body>
</html>
