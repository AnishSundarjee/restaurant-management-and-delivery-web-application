<?php
    session_start();

    //Setup database connectivity
    $host="127.0.0.1";
    $user="root";
    $password="password";
    $db="aphroditedb";
    $dbPort = "3306";

    $connect=mysqli_connect($host,$user,$password,$db,$dbPort);


    if(! $connect){
        die('Could not connect connect: ') ;
      }
        $user_id = $_SESSION["userName"];
        $getDeliveryLocationList = "SELECT * FROM building_info;";
        $getCustomerInfo = "CALL spGetCustomerInfo('$user_id');";

        $json_array = array();

        if($retvalue = mysqli_query($connect,$getCustomerInfo)){
          while($row=mysqli_fetch_array($retvalue)){
              $json_array=$row;
          }
        }  
        mysqli_free_result($retvalue); 
        mysqli_next_result($connect);  

        // echo '<pre>';
        // print_r($json_array);
        // echo '</pre>';

        if(isset($_SESSION["isLoggedIn"])){
            $isLogedin = $_SESSION["isLoggedIn"];
        }

    if(isset($_SESSION["cartItemCount"])){
        $cartCountValue=$_SESSION["cartItemCount"];
    }

    if(isset($_GET["res"])){
        if($_GET["res"]==2){
            echo "<script type='text/javascript'>alert('Incorrect credentials');</script>";
        }
        else {
            echo "<script type='text/javascript'>alert('Payment completed.');</script>";
        }
    }
?>
<!doctype html>
<html class="no-js" lang="zxx">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Afredieti || Setup Payment Method</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Favicons -->
	<link rel="shortcut icon" href="../images/favicon.ico">
	<link rel="apple-touch-icon" href="../images/icon.png">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/plugins.css">
    <link rel="stylesheet" href="../style.css">
    <!-- <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet" id="bootstrap-css"> -->

	<!-- Cusom css -->
 <link rel="stylesheet" href="../css/custom.css">

 <!-- Modernizer js -->
 <script src="../js/vendor/modernizr-3.5.0.min.js"></script>
</head>
<body>
	<!--[if lte IE 9]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
	<![endif]-->

	<!-- Add your site or application content here -->
	
	<!-- <div class="fakeloader"></div> -->

	<!-- Main wrapper -->
	<div class="wrapper" id="wrapper">
        <!-- Start Header Area -->
        <?php include '../reusableComponents/header.php'; ?>
        <!-- End Header Area -->
        <!-- Start Bradcaump area -->
        <div class="ht__bradcaump__area bg-image--27">
            <div class="ht__bradcaump__wrap d-flex align-items-center">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="bradcaump__inner text-center brad__white">
                                <h2 class="bradcaump-title">Customer Profile Setup</h2>
                                <nav class="bradcaump-inner">
                                  <a class="breadcrumb-item" href="index.php">Home</a>
                                  <span class="brd-separetor"><i class="zmdi zmdi-long-arrow-right"></i></span>
                                  <span class="breadcrumb-item active">Customer Profile</span>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Bradcaump area --> 
        <!-- Start Special Menu -->
        <!-- <section class="fd__special__menu__area bg-image--3 section-pt--lg"> -->
            <div class="container justify-content-center" >
                <h1>Profile - Setup Payroll here.</h1>
                <hr>
                <div class="row">
                <!-- edit form column -->
                <div class="col-md-9 personal-info">
                    
                    <h3>Personal info</h3>
                    
                    <form class="form-horizontal" role="form" method="POST">
                    <div class="">
                        <label class="col-lg-3 control-label">First name:</label>
                        <div class="col-lg-8">
                        <input class="form-control" type="text" value="<?php echo $json_array[1]; ?>" readonly>
                        </div>
                    </div>
                    <div class="">
                        <label class="col-lg-3 control-label">Last name:</label>
                        <div class="col-lg-8">
                        <input class="form-control" type="text" value="<?php echo $json_array[2]; ?>" readonly>
                        </div>
                    </div>
                    <div class="">
                        <label class="col-lg-3 control-label">Username:</label>
                        <div class="col-lg-8">
                        <input class="form-control" id="username" name="username" type="text" value="<?php echo $json_array[0]; ?>" readonly>
                        </div>
                    </div>
                    <div class="">
                        <label class="col-lg-3 control-label">Email:</label>
                        <div class="col-lg-8">
                        <input class="form-control" type="text" value="<?php echo $json_array[3]; ?>" readonly>
                        </div>
                    </div>
                    <div class="">
                        <label class="col-md-3 control-label">Payroll ID:</label>
                        <div class="col-md-8">
                        <input class="form-control" type="text" id="payrollId" name="payrollId" value="" placeholder="Enter Payroll ID" required>
                        </div>
                    </div>
                    <div class="">
                        <label class="col-md-3 control-label">password:</label>
                        <div class="col-md-8">
                        <input class="form-control" type="password" id="pin" name="pin" value="" placeholder="*****" required>
                        </div>
                    </div>
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-8">
                        <input type="button" class="btn btn-primary" onclick="_setUpPaymentMethod()" value="Save Changes">
                        <span></span>
                        <input type="reset" class="btn btn-default" value="Cancel">
                        </div>
                    </form>
                </div>
            </div>
            </div>
            <hr>
        <!-- </section> -->
        <!-- End Special Menu -->
        <!-- Start Footer Area -->
        <?php include '../reusableComponents/footer.php'; ?>
        <!-- End Footer Area -->
        <!-- Login Form -->
        <?php include '../reusableComponents/auth.php'; ?>
        <!-- //Login Form -->
    </div><!-- //Main wrapper -->

    <!-- JS Files -->
    <script src="../js/vendor/jquery-3.2.1.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/plugins.js"></script>
    <script src="../js/active.js"></script>
    <!-- <script src="js/login.js"></script> -->
    <script>
        function _setUpPaymentMethod() {  
            var pid = document.getElementById("payrollId").value;
            var pin = document.getElementById("pin").value;
            var userid = document.getElementById("username").value;
            if(pid != "" || pin!=""){
                if(!isNaN(pin) && pin.length == 4){
                    $.ajax({
                        url: '../controller/setUpPayroll.php',
                        type: 'POST',
                        data: {
                            user_id: userid,
                            payroll: pid,
                            payrollPIN: pin
                        },
                        success: function(msg) {
                            if(msg==0){
                                alert("User already has payment method setup. Please continue to transaction.");
                            }
                            else if(msg==1){
                                alert("This payroll is already used. Please contact admin for more information.");
                            }
                            else if(msg==2){
                                alert("Payment method setup completed. Continue shopping.");
                                window.location.href="index.php";
                            }
                            else if(msg==3){
                                alert('Credentials do not match, please try again.');
                            }
                            else alert('Something went wrong, Please contact admin for more information.')
                        }               
                    });
                }
                else{
                    alert('Pin does not meet criteria');
                }
                
            }
            else{
                alert('please complete from before submission');
            }
            //alert(user_id);
            
        }
    </script>

    <script>
        function validateEmail(email) {
            var re = /\S+@\S+\.\S+/;
            return re.test(email);
        }
        function _Login() {  
            var userID = document.getElementById("username").value;
            var Password = document.getElementById("userPassword").value;

            if(userID.length < 9){
                alert('Invalid User ID');
            }
            else {
                $.ajax({
                    url: '../controller/login.php',
                    type: 'POST',
                    data: {
                        username: userID,
                        userPassword: Password
                    },
                    success: function(msg) {
                        // if(msg==0){
                        //     alert("User does not exist.");
                        // }
                        // else if(msg==1){
                        //     alert("User Account is inactive.");
                        // }
                        // else if(msg==3){
                        //     alert("Incorrect password.");
                        // }
                        alert(msg);
                    }               
                });
            }
        }
        
        function _SignIn() {  
            var fistName = document.getElementById("fistName").value;
            var surName = document.getElementById("surName").value;
            var userID = document.getElementById("userID").value;
            var email = document.getElementById("email").value;
            var phoneNumber = document.getElementById("phoneNumber").value;
            var password = document.getElementById("password").value;
            var confrimPassword = document.getElementById("confrimPassword").value;
            var roomNo = document.getElementById("roomNo").value;
            var DeliverySelector = document.getElementById("DeliverySelector").value;
            var genderSRadio = document.querySelector("input[name=demo01]:checked");
            var genderSValue = genderSRadio ? genderSRadio.value : "";
            //console.log(' Value of gender : ',genderSValue)

            if(!(isNaN(fistName))){
                alert('Enter valid First Name.')
            }
            else if(!(isNaN(surName))){
                alert('Enter valid Sur Name.')
            }
            else if(userID.length < 9){
                alert('Invalid User ID');
            }
            else if(!(validateEmail(email))){
                alert("You have entered an invalid email address!")
            }
            else if((isNaN(phoneNumber)) || phoneNumber.length != 7){
                alert('Enter valid phoneNumber Name.')
            }
            else {
                $.ajax({
                    url: '../controller/signup.php',
                    type: 'POST',
                    data: {
                        fistName: fistName,
                        surName:surName,
                        userID:userID,
                        email:email,
                        phoneNumber:phoneNumber,
                        password:password,
                        confrimPassword: confrimPassword,
                        roomNo: roomNo,
                        DeliverySelector: DeliverySelector,
                        demo01: genderSValue
                    },
                    success: function(msg) {
                        if(msg==0){
                            return;
                        }
                        else if(msg==1){
                            alert("User account has been created, Please try to login.");
                        }
                        else if(msg==2){
                            alert("User account already exists, Please try to login.");
                        }
                        else if(msg==3){
                            alert("Email already exists, Please use another email ID.");
                        }
                        else if(msg==4){
                            alert("Password mismatch.");
                        }
                        else if(msg==5){
                            alert("Some values are missing.");
                        }
                        else alert("Something went wrong in your SignUp process, Please try again.");
                    }               
                });
            }
        }

    </script>

</body>
</html>