<?php
    session_start();

    //Setup database connectivity
    $host="127.0.0.1";
    $user="root";
    $password="password";
    $db="aphroditedb";
    $dbPort = "3306";

    $connect=mysqli_connect($host,$user,$password,$db,$dbPort);

    if(! $connect){
        die('Could not connect connect: ') ;
    }
    if(isset($_GET['el'])){
        $menuID = $_GET['el'];
        $_SESSION["itemID"]=$menuID;
        $getDeliveryLocationList = "SELECT * FROM building_info;";
        $getItemList = "CALL spFetchItemDetails($menuID);";
        $json_array = array();
        if($retvalue = mysqli_query($connect,$getItemList)){
        //print_r($retvalue);
            while($row=mysqli_fetch_array($retvalue)){
                $json_array=$row;
            }
           // echo $json_array[6];
        }  

        mysqli_free_result($retvalue); 
        mysqli_next_result($connect);  

        // echo '<pre>';
        // print_r($json_array[3]);
        // echo '</pre>';
        
        if(! $retvalue){
          die('Cannot connect to SQL: ');
        }
    }
    else {
        //echo "<script type='text/javascript'>alert('Something went wrong with the meal selection, Please try again.');</script>";
        header("location: menu-list.php?isSelect=1");
    }

    if(isset($_SESSION["isLoggedIn"])){
        $isLogedin = $_SESSION["isLoggedIn"];
    }
    
    if(isset($_SESSION["cartItemCount"])){
        $cartCountValue=$_SESSION["cartItemCount"];
    }
    $randNumber = rand(1,1000);
?>
<!doctype html>
<html class="no-js" lang="zxx">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Menu Details ||  Aahar Food Delivery Html5 Template</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Favicons -->
	<link rel="shortcut icon" href="../images/favicon.ico">
	<link rel="apple-touch-icon" href="../images/icon.png">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/plugins.css">
	<link rel="stylesheet" href="../style.css">

	<!-- Cusom css -->
   <link rel="stylesheet" href="../css/custom.css">

	<!-- Modernizer js -->
	<script src="../js/vendor/modernizr-3.5.0.min.js"></script>
</head>
<body>
	<!--[if lte IE 9]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
	<![endif]-->

	<!-- Add your site or application content here -->
	
	<!-- <div class="fakeloader"></div> -->

	<!-- Main wrapper -->
	<div class="wrapper" id="wrapper">
        <!-- Start Header Area -->
        <?php include '../reusableComponents/header.php'; ?>
        <!-- End Header Area -->
        <!-- Start Bradcaump area -->
        <div class="ht__bradcaump__area bg-image--27">
            <div class="ht__bradcaump__wrap d-flex align-items-center">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="bradcaump__inner text-center brad__white">
                                <h2 class="bradcaump-title">Menu Details</h2>
                                <nav class="bradcaump-inner">
                                  <a class="breadcrumb-item" href="index.php">Home</a>
                                  <span class="brd-separetor"><i class="zmdi zmdi-long-arrow-right"></i></span>
                                  <span class="breadcrumb-item active">Menu Details</span>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Bradcaump area --> 
        <!-- Start Blog List View Area -->
        <section class="blog__list__view section-padding--lg menudetails-right-sidebar bg--white">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-12 col-sm-12">
                        <div class="food__menu__container">
                                    
                            <div class="food__menu__inner d-flex flex-wrap flex-md-nowrap flex-lg-nowrap">
                                <div class="food__menu__thumb">
                                    <img src="../../../COS_Admin/images/menu_items/<?php echo $json_array[3]; ?>?<?php echo $randNumber; ?>" alt="product images" width="370" height="380">
                                </div>
                                <div class="food__menu__details">
                                    <div class="food__menu__content">
                                        <h2><?php echo $json_array[1];?></h2>
                                        <ul class="food__dtl__prize d-flex">
                                            <li class="old__prize"><?php echo $json_array[13]?></li>
                                            <li>$<?php echo $json_array[4]?></li>
                                        </ul>
                                        <p><?php echo $json_array[2]?></p>
                                        <div class="product-action-wrap">
                                            <div class="prodict-statas"><span>Food Type : <?php echo $json_array[12]?></span></div>
                                            <div class="product-quantity">
                                                <!-- <div id='myform' method='POST' action='controller/addToCart.php'> -->
                                                    <div class="product-quantity">
                                                        <div class="cart-plus-minus">
                                                            <div class="dec qtybutton">-</i></div>
                                                            <input class="cart-plus-minus-box" type="text" name="qtybutton" value="01" id="qtyItems">
                                                            <div class="inc qtybutton">+</div>
                                                            <div class="add__to__cart__btn">
                                                                <a  class="food__btn" onclick="_goToCart()" ><span>Add To Cart</span></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <!-- </form> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Start Product Descrive Area -->
                            <div class="menu__descrive__area">
                                <div class="menu__nav nav nav-tabs" role="tablist">
                                    <a class="active" id="nav-all-tab" data-toggle="tab" href="#nav-all" role="tab">Description</a>
                                </div>
                                <!-- Start Tab Content -->
                                <div class="menu__tab__content tab-content" id="nav-tabContent">
                                    <!-- Start Single Content -->
                                    <div class="single__dec__content fade show active" id="nav-all" role="tabpanel">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labor dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliqui comi modo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillumfugiat nu pariatur.Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,</p>
                                    </div>
                                    <!-- End Single Content -->
                                   
                                </div>
                                <!-- End Tab Content -->
                            </div>
                            <!-- End Product Descrive Area -->
                        </div>
                </div>
            </div>
        </section>
        <!-- End Blog List View Area -->
        <!-- Start Footer Area -->
        <?php include '../reusableComponents/footer.php'; ?>
        <!-- End Footer Area -->
        <!-- Login Form -->
        <?php include '../reusableComponents/auth.php'; ?>
        <!-- //Login Form -->>
	</div><!-- //Main wrapper -->

	<!-- JS Files -->
	<script src="../js/vendor/jquery-3.2.1.min.js"></script>
	<script src="../js/popper.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/plugins.js"></script>
    <script src="../js/active.js"></script>
    <!-- <script src="js/login.js"></script> -->
    <script>
        /*=============  Plus Minus Button ==============*/
        $(".qtybutton").on("click", function () {
            var $button = $(this);
            var oldValue = document.getElementById("qtyItems").value;
            if ($button.text() == "+") {  
                if (oldValue >= <?php echo $json_array[6]; ?>) {
                    var newVal = <?php echo $json_array[6]; ?>;
                }else{
                    var newVal = parseFloat(oldValue) + 1;
                }
            } else {
                // Don't allow decrementing below zero
                if (oldValue > 1) {
                    var newVal = parseFloat(oldValue) - 1;
                } else {
                    newVal = 1;
                }
            }
            document.getElementById("qtyItems").value = newVal;
            
        });
    </script>

    <script>
        function _goToCart () { 
            var cartValue = document.getElementById("qtyItems").value;
            var isLoggedIn = <?php if(!empty($isLogedin)){echo $isLogedin;} else echo '99'?>;
            if(isNaN(cartValue)){
                alert('Input only numbers');
            }
            else if(isLoggedIn != 2 || isLoggedIn == null ){
                alert('Please sign-in or sign-up to Add Meal item to cart.');
            }
            else{
                window.location.href="../controller/addToCart.php?el="+cartValue;
            }
        }
    </script>

    <script>
        function validateEmail(email) {
            var re = /\S+@\S+\.\S+/;
            return re.test(email);
        }
        function _Login() {  
            var userID = document.getElementById("username").value;
            var Password = document.getElementById("userPassword").value;

            if(userID.length < 9){
                alert('Invalid User ID');
            }
            else {
                $.ajax({
                    url: '../controller/login.php',
                    type: 'POST',
                    data: {
                        username: userID,
                        userPassword: Password
                    },
                    success: function(msg) {
                        // if(msg==0){
                        //     alert("User does not exist.");
                        // }
                        // else if(msg==1){
                        //     alert("User Account is inactive.");
                        // }
                        // else if(msg==3){
                        //     alert("Incorrect password.");
                        // }
                        alert(msg);
                    }               
                });
            }
        }
        
        function _SignIn() {  
            var fistName = document.getElementById("fistName").value;
            var surName = document.getElementById("surName").value;
            var userID = document.getElementById("userID").value;
            var email = document.getElementById("email").value;
            var phoneNumber = document.getElementById("phoneNumber").value;
            var password = document.getElementById("password").value;
            var confrimPassword = document.getElementById("confrimPassword").value;
            var roomNo = document.getElementById("roomNo").value;
            var DeliverySelector = document.getElementById("DeliverySelector").value;
            var genderSRadio = document.querySelector("input[name=demo01]:checked");
            var genderSValue = genderSRadio ? genderSRadio.value : "";
            //console.log(' Value of gender : ',genderSValue)

            if(!(isNaN(fistName))){
                alert('Enter valid First Name.')
            }
            else if(!(isNaN(surName))){
                alert('Enter valid Sur Name.')
            }
            else if(userID.length < 9){
                alert('Invalid User ID');
            }
            else if(!(validateEmail(email))){
                alert("You have entered an invalid email address!")
            }
            else if((isNaN(phoneNumber)) || phoneNumber.length != 7){
                alert('Enter valid phoneNumber Name.')
            }
            else {
                $.ajax({
                    url: '../controller/signup.php',
                    type: 'POST',
                    data: {
                        fistName: fistName,
                        surName:surName,
                        userID:userID,
                        email:email,
                        phoneNumber:phoneNumber,
                        password:password,
                        confrimPassword: confrimPassword,
                        roomNo: roomNo,
                        DeliverySelector: DeliverySelector,
                        demo01: genderSValue
                    },
                    success: function(msg) {
                        if(msg==0){
                            return;
                        }
                        else if(msg==1){
                            alert("User account has been created, Please try to login.");
                        }
                        else if(msg==2){
                            alert("User account already exists, Please try to login.");
                        }
                        else if(msg==3){
                            alert("Email already exists, Please use another email ID.");
                        }
                        else if(msg==4){
                            alert("Password mismatch.");
                        }
                        else if(msg==5){
                            alert("Some values are missing.");
                        }
                        else alert("Something went wrong in your SignUp process, Please try again.");
                    }               
                });
            }
        }

    </script>

</body>
</html>
