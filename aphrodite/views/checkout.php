<?php
    session_start();

    //Setup database connectivity
    $host="127.0.0.1";
    $user="root";
    $password="password";
    $db="aphroditedb";
    $dbPort = "3306";

    $connect=mysqli_connect($host,$user,$password,$db,$dbPort);

    function cleanData($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    if(! $connect){
        die('Could not connect connect: ') ;
      }

    if(isset($_SESSION["userName"])){
        $userID = cleanData($_SESSION["userName"]);
        $getCurrentAddress = "CALL spViewCurrentAddress('$userID');";
        $getDeliveryLocationList = "SELECT * FROM building_info;";
        $getDeliveryTimeSlotList = "SELECT * FROM timeslots;";
        $fetchOrderItems = "CALL spFetchCartItems('$userID');";
        $getCartSum =  "call spGetCartSum('$userID');";
        $json_array = array();
        if($retvalue = mysqli_query($connect,$getCurrentAddress)){
          while($row=mysqli_fetch_row($retvalue)){
              $DeliveryID = $row[0];
              $DeliveryDescription = $row[1];
              $Room = $row[2];
          }
        }

        mysqli_free_result($retvalue); 
        mysqli_next_result($connect); 

        if($retvalue = mysqli_query($connect,$fetchOrderItems)){
            while($row=mysqli_fetch_assoc($retvalue)){
                $json_array[]=$row;
            }
        }

        mysqli_free_result($retvalue); 
        mysqli_next_result($connect); 

        if($retvalue = mysqli_query($connect,$getCartSum)){
            while($row=mysqli_fetch_row($retvalue)){
                $cartSum=$row[0];
            }
        }

        mysqli_free_result($retvalue); 
        mysqli_next_result($connect); 

        if(! $retvalue){
            echo mysqli_error($connect);
        }
    }
    else{
        header("location: controller/logout.php");
    }

    if(isset($_SESSION["isLoggedIn"])){
        $isLogedin = $_SESSION["isLoggedIn"];
    }

    if(isset($_SESSION["cartItemCount"])){
        $cartCountValue=$_SESSION["cartItemCount"];
    }

    if(isset($_GET["orderId"])){
        $order = $_GET["orderId"];
        echo "<script type='text/javascript'>alert('Order complete. Please track order info from Order Tracking page order ID : $order');</script>";
    }

    if(isset($_GET["res"])){
        $res = $_GET["res"];
        if($res==1){
            echo "<script type='text/javascript'>alert('Please provide delivery time and date to place order');</script>";
        }
        else if($res==2){
            echo "<script type='text/javascript'>alert('Please set up payroll to use delivery option to place order.');</script>";
        }
        
    }
    
?>
<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Checkout ||  Aahar Food Delivery Html5 Template</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicons -->
    <link rel="shortcut icon" href="../images/favicon.ico">
    <link rel="apple-touch-icon" href="../images/icon.png">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/plugins.css">
    <link rel="stylesheet" href="../style.css">

    <!-- Cusom css -->
   <link rel="stylesheet" href="../css/custom.css">

    <!-- Modernizer js -->
    <script src="../js/vendor/modernizr-3.5.0.min.js"></script>
</head>
<body>

    <!--formden.js communicates with FormDen server to validate fields and submit via AJAX -->
    <script type="text/javascript" src="https://formden.com/static/cdn/formden.js"></script>

    <!-- Special version of Bootstrap that is isolated to content wrapped in .bootstrap-iso -->
    <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />

    <!--Font Awesome (added because you use icons in your prepend/append)-->
    <link rel="stylesheet" href="https://formden.com/static/cdn/font-awesome/4.4.0/css/font-awesome.min.css" />

    <!-- Inline CSS based on choices in "Settings" tab -->
    <style>.bootstrap-iso .formden_header h2, .bootstrap-iso .formden_header p, .bootstrap-iso form{font-family: Arial, Helvetica, sans-serif; color: black}.bootstrap-iso form button, .bootstrap-iso form button:hover{color: white !important;} .asteriskField{color: red;}</style>

    <!--[if lte IE 9]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
    <![endif]-->
    
    <!-- <div class="fakeloader"></div> -->

    <!-- Main wrapper -->
    <div class="wrapper" id="wrapper">
        <!-- Start Header Area -->
        <?php include '../reusableComponents/header.php'; ?>
        <!-- End Header Area -->
        <!-- Start Bradcaump area -->
        <div class="ht__bradcaump__area bg-image--27">
            <div class="ht__bradcaump__wrap d-flex align-items-center">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="bradcaump__inner text-center brad__white">
                                <h2 class="bradcaump-title">Checkout</h2>
                                <nav class="bradcaump-inner">
                                  <a class="breadcrumb-item" href="index.php">Home</a>
                                  <span class="brd-separetor"><i class="zmdi zmdi-long-arrow-right"></i></span>
                                  <span class="breadcrumb-item active">Checkout</span>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Bradcaump area --> 
         <section class="htc__checkout bg--white section-padding--lg">
            <!-- Checkout Section Start-->
            <div class="checkout-section">
                <div class="container">
                    <div class="row">
                       
                        <div class="col-lg-6 col-12 mb-30">
                               
                                <!-- Checkout Accordion Start -->
                                <div id="checkout-accordion">
                                    
                                    <!-- Shipping Method -->
                                    <div class="single-accordion">
                                        <a class="accordion-head collapsed" data-toggle="collapse" data-parent="#checkout-accordion" href="#shipping-method">1. Delivery location details</a>
                                        <div id="shipping-method" class="collapse show">
                                            <div class="accordion-body shipping-method fix">
                                               
                                                <h5>Delivery address</h5>
                                                <p><span>default address&nbsp;</span><?php echo $DeliveryDescription ?> Room: <?php echo $Room ?></p>
                                                <button class="shipping-form-toggle">Ship to a different address?</button>
                                                
                                                <form  id="DeliveryLocation" method="POST" class="shipping-form checkout-form">
                                                    <div class="row">
                                                        <div class="col-md-6 col-12 mb--20">                                 
                                                            <select id="selectAddress" name="selectAddress">
                                                            <?php
                                                                if($retvalue = mysqli_query($connect,$getDeliveryLocationList)){
                                                                    while($row=mysqli_fetch_array($retvalue)){
                                                                        ?> <option value="<?php echo $row["BuildingId"];?>"><?php echo $row["BuildingDescription"] ?></option><?php
                                                                    }
                                                                }
                                                                mysqli_free_result($retvalue); 
                                                                mysqli_next_result($connect);
                                                            ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-6 col-12 mb--20">                             
                                                            <input type="text" id="roomNo" name="roomNo" placeholder="Room Number." required maxlength=10>
                                                        </div>
                                                        <div class="col-12 mb--20">                              
                                                            <input type="text" placeholder="Additional Information.">
                                                        </div>
                                                        <div class="col-3 mb--8">                              
                                                            <button id="submitDeliveryLocationInfo" name="submitDeliveryLocationInfo" class="food__btn" onclick="SubmitFormData();" >Confirm</button>
                                                        </div>          
                                                    </div>
                                                </form>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <!-- Payment Method -->
                                    <div class="single-accordion">
                                        <a class="accordion-head collapsed" data-toggle="collapse" data-parent="#checkout-accordion" href="#payment-method">2. Payment method - Delivery Mode and Time</a>
                                        <div id="payment-method" class="collapse">
                                            <div class="accordion-body payment-method fix">
                                               
                                                <ul class="payment-method-list">
                                                    <li class="active" id="counter" value="apple">Pay at counter</li>
                                                    <li class="payment-form-toggle" id="payroll" >Payroll</li>
                                                </ul>
                                                <iframe name="votar" style="display:none;"></iframe>
                                                <form target="votar"  id="DeliveryTime" method="POST" action="../controller/addDeliveryTime.php" class="payment-form">
                                                    <div class="row">
                                                        
                                                        <div class="input-box col-12 mb--20">
                                                            <label>Delivery Time</label>
                                                            <select id="selectTime" name="selectTime">
                                                            <?php
                                                                if($retvalue = mysqli_query($connect,$getDeliveryTimeSlotList)){
                                                                    while($row=mysqli_fetch_array($retvalue)){
                                                                        ?> <option value="<?php echo $row["TimeslotId"];?>"><?php echo $row["TimeslotDescription"] ?></option><?php
                                                                    }
                                                                }
                                                                mysqli_free_result($retvalue); 
                                                                mysqli_next_result($connect);
                                                                mysqli_close($connect);
                                                            ?>
                                                            </select>
                                                        </div>
                                                       
                                                        <div class="input-box col-12">
                                                            <label class="control-label col-sm-2 requiredField" for="date">
                                                                Delivery Date
                                                                <span class="asteriskField">
                                                                    *
                                                                </span>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-calendar">
                                                                        </i>
                                                                    </div>
                                                                        <input class="form-control" id="date" name="date" placeholder="MM/DD/YYYY" type="text" required/>
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                        <div class="col-12">                              
                                                            
                                                        </div>
                                                        <div class="col-3 mb--8">                              
                                                            <button id="submitDeliveryTimeInfo" name="submitDeliveryTimeInfo" class="food__btn" >Confirm</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div><!-- Checkout Accordion Start -->
                            </div>
                            
                            <!-- Order Details -->
                            <div class="col-lg-6 col-12 mb-30">
                                                                
                                <div class="order-details-wrapper">
                                    <h2>your order</h2>
                                    <div class="order-details">
                                        <form action="../controller/placeOrder.php" method="POST" id="OrderForm">
                                            <ul>
                                                <li><p class="strong">Meal</p><p class="strong">total</p></li>
                                                <?php foreach ($json_array as $key => $value) {
                                                    ?>
                                                        <li><p><?php echo $value["ItemName"]; ?> x<?php echo $value["ItemQuantity"] ?></p><p>$<?php echo $value["TotalPrice"] ?></p></li>
                                                    <?php
                                                }?>
                                                <li><p class="strong">cart subtotal</p><p class="strong">$<?php echo $cartSum ?></p></li>
                                                <li><p class="strong">Delivery Method</p><p>
                                                    <input type="radio" name="order-shipping" id="free" value="0" checked="checked" /><label for="free">Pay at counter</label><br />
                                                    <input type="radio" name="order-shipping" id="flat" value="1" /><label for="flat">Flat Delivery Rate $5.00</label>
                                                </p></li>
                                                
                                                <li><button type="submit" class="food__btn"  >place order</button></li>
                                            </ul>
                                        </form>
                                    </div>
                                </div>
                                
                            </div>
                        
                    </div>
                </div>
            </div><!-- Checkout Section End-->             
        </section>   
        <!-- Start Footer Area -->
        <?php include '../reusableComponents/footer.php'; ?>
        <!-- End Footer Area -->
        <!-- Login Form -->
        <?php include '../reusableComponents/auth.php'; ?>
        <!-- //Login Form -->
    </div><!-- //Main wrapper -->

    <!-- JS Files -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/plugins.js"></script>
    <script src="../js/active.js"></script>
    <script src="../js/login.js"></script>
    <!-- Extra JavaScript/CSS added manually in "Settings" tab -->
    <!-- Include jQuery -->
    <!-- <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script> -->

    <!-- Include Date Range Picker -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

    <script>
        $(document).ready(function(){
            var today = new Date();
            var time = today.getHours();
            var restrictDate = null;
            if(time>=11){
                restrictDate = '+1d';
            }
            else{
                restrictDate = '0d';
            }
            var date_input=$('input[name="date"]'); //our date input has the name "date"
            var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
            date_input.datepicker({
                format: 'yyyy-mm-dd',
                container: container,
                daysOfWeekDisabled:[0],
                startDate: restrictDate,
                autoclose: true,
                forceParse:true,
                endDate:'2020-04-25',
            })
        })
    </script>
    <script>
    function SubmitFormData() { 
        var selectAddress = document.getElementById("selectAddress").value;
        var roomNo = document.getElementById("roomNo").value;
        $.ajax({
            url: '../controller/changeAddress.php',
            type: 'POST',
            data: {
                selectAddress: selectAddress,
                roomNo: roomNo
            },
            success: function(msg) {
                return;
            }               
        });
    }
    </script>
    <script>
        function validateEmail(email) {
            var re = /\S+@\S+\.\S+/;
            return re.test(email);
        }
        function _Login() {  
            var userID = document.getElementById("username").value;
            var Password = document.getElementById("userPassword").value;

            if(userID.length < 9){
                alert('Invalid User ID');
            }
            else {
                $.ajax({
                    url: '../controller/login.php',
                    type: 'POST',
                    data: {
                        username: userID,
                        userPassword: Password
                    },
                    success: function(msg) {
                        // if(msg==0){
                        //     alert("User does not exist.");
                        // }
                        // else if(msg==1){
                        //     alert("User Account is inactive.");
                        // }
                        // else if(msg==3){
                        //     alert("Incorrect password.");
                        // }
                        alert(msg);
                    }               
                });
            }
        }
        
        function _SignIn() {  
            var fistName = document.getElementById("fistName").value;
            var surName = document.getElementById("surName").value;
            var userID = document.getElementById("userID").value;
            var email = document.getElementById("email").value;
            var phoneNumber = document.getElementById("phoneNumber").value;
            var password = document.getElementById("password").value;
            var confrimPassword = document.getElementById("confrimPassword").value;
            var roomNo = document.getElementById("roomNo").value;
            var DeliverySelector = document.getElementById("DeliverySelector").value;
            var genderSRadio = document.querySelector("input[name=demo01]:checked");
            var genderSValue = genderSRadio ? genderSRadio.value : "";
            //console.log(' Value of gender : ',genderSValue)

            if(!(isNaN(fistName))){
                alert('Enter valid First Name.')
            }
            else if(!(isNaN(surName))){
                alert('Enter valid Sur Name.')
            }
            else if(userID.length < 9){
                alert('Invalid User ID');
            }
            else if(!(validateEmail(email))){
                alert("You have entered an invalid email address!")
            }
            else if((isNaN(phoneNumber)) || phoneNumber.length != 7){
                alert('Enter valid phoneNumber Name.')
            }
            else {
                $.ajax({
                    url: '../controller/signup.php',
                    type: 'POST',
                    data: {
                        fistName: fistName,
                        surName:surName,
                        userID:userID,
                        email:email,
                        phoneNumber:phoneNumber,
                        password:password,
                        confrimPassword: confrimPassword,
                        roomNo: roomNo,
                        DeliverySelector: DeliverySelector,
                        demo01: genderSValue
                    },
                    success: function(msg) {
                        if(msg==0){
                            return;
                        }
                        else if(msg==1){
                            alert("User account has been created, Please try to login.");
                        }
                        else if(msg==2){
                            alert("User account already exists, Please try to login.");
                        }
                        else if(msg==3){
                            alert("Email already exists, Please use another email ID.");
                        }
                        else if(msg==4){
                            alert("Password mismatch.");
                        }
                        else if(msg==5){
                            alert("Some values are missing.");
                        }
                        else alert("Something went wrong in your SignUp process, Please try again.");
                    }               
                });
            }
        }

    </script>
</body>
</html>
