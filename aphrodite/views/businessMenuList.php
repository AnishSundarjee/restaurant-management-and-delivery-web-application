<?php
    session_start();

    //Setup database connectivity
    $host="127.0.0.1";
    $user="root";
    $password="password";
    $db="aphroditedb";
    $dbPort = "3306";

    $connect=mysqli_connect($host,$user,$password,$db,$dbPort);


    if(! $connect){
        die('Could not connect connect: ') ;
      }
    function cleanData($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
    if(isset($_GET["el"])){
        $Bid = $_GET["el"];
        $getDeliveryLocationList = "SELECT * FROM building_info;";
        $getBusinessItemListSp = "CALL spFetchBusinessItemList('$Bid');";
        $json_array = array();
        if($retvalue = mysqli_query($connect,$getBusinessItemListSp)){
          //print_r($retvalue);
          while($row=mysqli_fetch_assoc($retvalue)){
              $json_array[]=$row;
          }
        }  
        // echo '<pre>';
        // print_r($json_array);
        // echo '</pre>';
        if(! $retvalue){
          die('Cannot connect to SQL: ');
        }
    }

    if(isset($_SESSION["isLoggedIn"])){
        $isLogedin = $_SESSION["isLoggedIn"];
    }

    if(isset($_SESSION["cartItemCount"])){
        $cartCountValue=$_SESSION["cartItemCount"];
    }
    $randNumber = rand(1,1000);
?>
<!doctype html>
<html class="no-js" lang="zxx">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Afredieti || Menu List</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Favicons -->
	<link rel="shortcut icon" href="../images/favicon.ico">
	<link rel="apple-touch-icon" href="../images/icon.png">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/plugins.css">
	<link rel="stylesheet" href="../style.css">

	<!-- Cusom css -->
   <link rel="stylesheet" href="../css/custom.css">

	<!-- Modernizer js -->
	<script src="../js/vendor/modernizr-3.5.0.min.js"></script>
</head>
<body>
	<!--[if lte IE 9]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
	<![endif]-->

	<!-- Add your site or application content here -->
	
	<!-- <div class="fakeloader"></div> -->

	<!-- Main wrapper -->
	<div class="wrapper" id="wrapper">
        <!-- Start Header Area -->
        <?php include '../reusableComponents/header.php'; ?>
        <!-- End Header Area -->
        <!-- Start Bradcaump area -->
        <div class="ht__bradcaump__area bg-image--27">
            <div class="ht__bradcaump__wrap d-flex align-items-center">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="bradcaump__inner text-center">
                                <h2 class="bradcaump-title">Menu List view</h2>
                                <nav class="bradcaump-inner">
                                  <a class="breadcrumb-item" href="index.php">Home</a>
                                  <span class="brd-separetor"><i class="zmdi zmdi-long-arrow-right"></i></span>
                                  <span class="breadcrumb-item active">Menu List</span>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Bradcaump area --> 
        <!-- Start Menu Grid Area -->
        <section class="food__menu__grid__area section-padding--lg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="food__nav nav nav-tabs" role="tablist">
                            <a class="active" id="nav-all-tab" data-toggle="tab" href="#nav-all" role="tab">All</a>
                            <a id="nav-breakfast-tab" data-toggle="tab" href="#nav-breakfast" role="tab">Breakfast</a>
                            <a id="nav-lunch-tab" data-toggle="tab" href="#nav-lunch" role="tab">Lunch</a>
                            <a id="nav-dinner-tab" data-toggle="tab" href="#nav-dinner" role="tab">Dinner</a>
                            <a id="nav-coffee-tab" data-toggle="tab" href="#nav-coffee" role="tab">Coffee</a>
                            <a id="nav-snacks-tab" data-toggle="tab" href="#nav-snacks" role="tab">Snacks</a>
                        </div>
                    </div>
                </div>
                <div class="row mt--30">
                    <div class="col-lg-12">
                        <div class="fd__tab__content tab-content" id="nav-tabContent">
                            <!-- Start Single Content -->
                            <div class="food__list__tab__content tab-pane fade show active" id="nav-all" role="tabpanel">
                                <?php
                                if(is_array($json_array)){
                                    foreach($json_array as $item => $value){
                                        $ItemQuantityOnHand = $value["ItemQuantityOnHand"];
                                        $itemid = $value["itemid"];
                                        $ItemName = $value["ItemName"];
                                        $BusinessName = $value["BusinessName"];
                                        $MealTypeDescription = $value["MealTypeDescription"];
                                        $ItemDescription = $value["ItemDescription"];
                                        $ItemPrice = $value["ItemPrice"];
                                        $ItemCategoryId = $value["ItemCategoryId"];
                                        $ItemImageThumbnail = $value["ItemImageThumbnail"];

                                        if($ItemQuantityOnHand>0){
                                        ?>
                                            <!-- Start Single Food -->
                                            <div class="single__food__list d-flex wow fadeInUp">
                                                <div class="food__list__thumb">
                                                    <a onclick="_goToMealDetail('<?php echo $itemid;?>')">
                                                        <img src="../../../COS_Admin/images/menu_items/<?php echo $ItemImageThumbnail; ?>?<?php echo $randNumber; ?>" alt="product images" width="469" height="253">
                                                    </a>
                                                </div>
                                                <div class="food__list__inner d-flex align-items-center justify-content-between">
                                                    <div class="food__list__details">
                                                        <h2><a onclick="_goToMealDetail('<?php echo $itemid;?>')"><?php echo $ItemName;?></a></h2>
                                                        <h4>Resturant Name: <?php echo $BusinessName;?></h4>
                                                        <h5>Food Type : <?php echo $MealTypeDescription;?></h5>
                                                        <p><?php echo $ItemDescription;?></p>
                                                        <div class="list__btn">
                                                            <a class="food__btn grey--btn theme--hover" onclick="_goToMealDetail('<?php echo $itemid;?>')">Order Now</a>
                                                        </div>
                                                    </div>
                                                    <div class="food__rating">
                                                        <div class="list__food__prize">
                                                            <span>$<?php echo $ItemPrice;?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- End Single Food -->
                                        <?php
                                        }
                                    }
                                }
                                ?>             
                            </div>
                            <!-- End Single Content -->
                            <!-- Start Single Content -->
                            <div class="food__list__tab__content tab-pane fade" id="nav-breakfast" role="tabpanel">
                                <?php
                                if(is_array($json_array)){
                                    foreach($json_array as $item => $value){
                                        $ItemQuantityOnHand = $value["ItemQuantityOnHand"];
                                        $itemid = $value["itemid"];
                                        $ItemName = $value["ItemName"];
                                        $BusinessName = $value["BusinessName"];
                                        $MealTypeDescription = $value["MealTypeDescription"];
                                        $ItemDescription = $value["ItemDescription"];
                                        $ItemPrice = $value["ItemPrice"];
                                        $ItemCategoryId = $value["ItemCategoryId"];
                                        $ItemImageThumbnail = $value["ItemImageThumbnail"];

                                        if($ItemQuantityOnHand>0 && $ItemCategoryId==1){
                                        ?>
                                            <!-- Start Single Food -->
                                            <div class="single__food__list d-flex wow fadeInUp">
                                                <div class="food__list__thumb">
                                                    <a onclick="_goToMealDetail('<?php echo $itemid;?>')">
                                                    <img src="../../../COS_Admin/images/menu_items/<?php echo $ItemImageThumbnail; ?>?<?php echo $randNumber; ?>" alt="product images" width="469" height="253">
                                                    </a>
                                                </div>
                                                <div class="food__list__inner d-flex align-items-center justify-content-between">
                                                    <div class="food__list__details">
                                                        <h2><a onclick="_goToMealDetail('<?php echo $itemid;?>')"><?php echo $ItemName;?></a></h2>
                                                        <h4>Resturant Name: <?php echo $BusinessName;?></h4>
                                                        <h5>Food Type : <?php echo $MealTypeDescription;?></h5>
                                                        <p><?php echo $ItemDescription;?></p>
                                                        <div class="list__btn">
                                                            <a class="food__btn grey--btn theme--hover" onclick="_goToMealDetail('<?php echo $itemid;?>')">Order Now</a>
                                                        </div>
                                                    </div>
                                                    <div class="food__rating">
                                                        <div class="list__food__prize">
                                                            <span>$<?php echo $ItemPrice;?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- End Single Food -->
                                        <?php
                                        }
                                    }
                                }
                                ?>          
                            </div>
                            <!-- End Single Content -->
                            <!-- Start Single Content -->
                            <div class="food__list__tab__content tab-pane fade" id="nav-lunch" role="tabpanel">
                                <?php
                                if(is_array($json_array)){
                                    foreach($json_array as $item => $value){
                                        $ItemQuantityOnHand = $value["ItemQuantityOnHand"];
                                        $itemid = $value["itemid"];
                                        $ItemName = $value["ItemName"];
                                        $BusinessName = $value["BusinessName"];
                                        $MealTypeDescription = $value["MealTypeDescription"];
                                        $ItemDescription = $value["ItemDescription"];
                                        $ItemPrice = $value["ItemPrice"];
                                        $ItemCategoryId = $value["ItemCategoryId"];
                                        $ItemImageThumbnail = $value["ItemImageThumbnail"];

                                        if($ItemQuantityOnHand>0 && $ItemCategoryId==2){
                                        ?>
                                            <!-- Start Single Food -->
                                            <div class="single__food__list d-flex wow fadeInUp">
                                                <div class="food__list__thumb">
                                                    <a onclick="_goToMealDetail('<?php echo $itemid;?>')">
                                                    <img src="../../../COS_Admin/images/menu_items/<?php echo $ItemImageThumbnail; ?>?<?php echo $randNumber; ?>" alt="product images" width="469" height="253">
                                                    </a>
                                                </div>
                                                <div class="food__list__inner d-flex align-items-center justify-content-between">
                                                    <div class="food__list__details">
                                                        <h2><a onclick="_goToMealDetail('<?php echo $itemid;?>')"><?php echo $ItemName;?></a></h2>
                                                        <h4>Resturant Name: <?php echo $BusinessName;?></h4>
                                                        <h5>Food Type : <?php echo $MealTypeDescription;?></h5>
                                                        <p><?php echo $ItemDescription;?></p>
                                                        <div class="list__btn">
                                                            <a class="food__btn grey--btn theme--hover" onclick="_goToMealDetail('<?php echo $itemid;?>')">Order Now</a>
                                                        </div>
                                                    </div>
                                                    <div class="food__rating">
                                                        <div class="list__food__prize">
                                                            <span>$<?php echo $ItemPrice;?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- End Single Food -->
                                        <?php
                                        }
                                    }
                                }
                                ?>          
                            </div>
                            <!-- End Single Content -->
                            <!-- Start Single Content -->
                            <div class="food__list__tab__content tab-pane fade" id="nav-dinner" role="tabpanel">
                                <?php
                                if(is_array($json_array)){
                                    foreach($json_array as $item => $value){
                                        $ItemQuantityOnHand = $value["ItemQuantityOnHand"];
                                        $itemid = $value["itemid"];
                                        $ItemName = $value["ItemName"];
                                        $BusinessName = $value["BusinessName"];
                                        $MealTypeDescription = $value["MealTypeDescription"];
                                        $ItemDescription = $value["ItemDescription"];
                                        $ItemPrice = $value["ItemPrice"];
                                        $ItemCategoryId = $value["ItemCategoryId"];
                                        $ItemImageThumbnail = $value["ItemImageThumbnail"];

                                        if($ItemQuantityOnHand>0 && $ItemCategoryId==3){
                                        ?>
                                            <!-- Start Single Food -->
                                            <div class="single__food__list d-flex wow fadeInUp">
                                                <div class="food__list__thumb">
                                                    <a onclick="_goToMealDetail('<?php echo $itemid;?>')">
                                                    <img src="../../../COS_Admin/images/menu_items/<?php echo $ItemImageThumbnail; ?>?<?php echo $randNumber; ?>" alt="product images" width="469" height="253">
                                                    </a>
                                                </div>
                                                <div class="food__list__inner d-flex align-items-center justify-content-between">
                                                    <div class="food__list__details">
                                                        <h2><a onclick="_goToMealDetail('<?php echo $itemid;?>')"><?php echo $ItemName;?></a></h2>
                                                        <h4>Resturant Name: <?php echo $BusinessName;?></h4>
                                                        <h5>Food Type : <?php echo $MealTypeDescription;?></h5>
                                                        <p><?php echo $ItemDescription;?></p>
                                                        <div class="list__btn">
                                                            <a class="food__btn grey--btn theme--hover" onclick="_goToMealDetail('<?php echo $itemid;?>')">Order Now</a>
                                                        </div>
                                                    </div>
                                                    <div class="food__rating">
                                                        <div class="list__food__prize">
                                                            <span>$<?php echo $ItemPrice;?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- End Single Food -->
                                        <?php
                                        }
                                    }
                                }
                                ?>          
                            </div>
                            <!-- End Single Content -->
                            <!-- Start Single Content -->
                            <div class="food__list__tab__content tab-pane fade" id="nav-coffee" role="tabpanel">
                                <?php
                                if(is_array($json_array)){
                                    foreach($json_array as $item => $value){
                                        $ItemQuantityOnHand = $value["ItemQuantityOnHand"];
                                        $itemid = $value["itemid"];
                                        $ItemName = $value["ItemName"];
                                        $BusinessName = $value["BusinessName"];
                                        $MealTypeDescription = $value["MealTypeDescription"];
                                        $ItemDescription = $value["ItemDescription"];
                                        $ItemPrice = $value["ItemPrice"];
                                        $ItemCategoryId = $value["ItemCategoryId"];
                                        $ItemImageThumbnail = $value["ItemImageThumbnail"];

                                        if($ItemQuantityOnHand>0 && $ItemCategoryId==4){
                                        ?>
                                            <!-- Start Single Food -->
                                            <div class="single__food__list d-flex wow fadeInUp">
                                                <div class="food__list__thumb">
                                                    <a onclick="_goToMealDetail('<?php echo $itemid;?>')">
                                                    <img src="../../../COS_Admin/images/menu_items/<?php echo $ItemImageThumbnail; ?>?<?php echo $randNumber; ?>" alt="product images" width="469" height="253">
                                                    </a>
                                                </div>
                                                <div class="food__list__inner d-flex align-items-center justify-content-between">
                                                    <div class="food__list__details">
                                                        <h2><a onclick="_goToMealDetail('<?php echo $itemid;?>')"><?php echo $ItemName;?></a></h2>
                                                        <h4>Resturant Name: <?php echo $BusinessName;?></h4>
                                                        <h5>Food Type : <?php echo $MealTypeDescription;?></h5>
                                                        <p><?php echo $ItemDescription;?></p>
                                                        <div class="list__btn">
                                                            <a class="food__btn grey--btn theme--hover" onclick="_goToMealDetail('<?php echo $itemid;?>')">Order Now</a>
                                                        </div>
                                                    </div>
                                                    <div class="food__rating">
                                                        <div class="list__food__prize">
                                                            <span>$<?php echo $ItemPrice;?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- End Single Food -->
                                        <?php
                                        }
                                    }
                                }
                                ?>          
                            </div>
                            <!-- End Single Content -->
                            <!-- Start Single Content -->
                            <div class="food__list__tab__content tab-pane fade" id="nav-snacks" role="tabpanel">
                                <?php
                                if(is_array($json_array)){
                                    foreach($json_array as $item => $value){
                                        $ItemQuantityOnHand = $value["ItemQuantityOnHand"];
                                        $itemid = $value["itemid"];
                                        $ItemName = $value["ItemName"];
                                        $BusinessName = $value["BusinessName"];
                                        $MealTypeDescription = $value["MealTypeDescription"];
                                        $ItemDescription = $value["ItemDescription"];
                                        $ItemPrice = $value["ItemPrice"];
                                        $ItemCategoryId = $value["ItemCategoryId"];
                                        $ItemImageThumbnail = $value["ItemImageThumbnail"];

                                        if($ItemQuantityOnHand>0 && $ItemCategoryId==5){
                                        ?>
                                            <!-- Start Single Food -->
                                            <div class="single__food__list d-flex wow fadeInUp">
                                                <div class="food__list__thumb">
                                                    <a onclick="_goToMealDetail('<?php echo $itemid;?>')">
                                                    <img src="../../../COS_Admin/images/menu_items/<?php echo $ItemImageThumbnail; ?>?<?php echo $randNumber; ?>" alt="product images" width="469" height="253">
                                                    </a>
                                                </div>
                                                <div class="food__list__inner d-flex align-items-center justify-content-between">
                                                    <div class="food__list__details">
                                                        <h2><a onclick="_goToMealDetail('<?php echo $itemid;?>')"><?php echo $ItemName;?></a></h2>
                                                        <h4>Resturant Name: <?php echo $BusinessName;?></h4>
                                                        <h5>Food Type : <?php echo $MealTypeDescription;?></h5>
                                                        <p><?php echo $ItemDescription;?></p>
                                                        <div class="list__btn">
                                                            <a class="food__btn grey--btn theme--hover" onclick="_goToMealDetail('<?php echo $itemid;?>')">Order Now</a>
                                                        </div>
                                                    </div>
                                                    <div class="food__rating">
                                                        <div class="list__food__prize">
                                                            <span>$<?php echo $ItemPrice;?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- End Single Food -->
                                        <?php
                                        }
                                    }
                                }
                                ?>          
                            </div>
                            <!-- End Single Content -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Menu Grid Area -->
        <!-- Start Footer Area -->
        <?php include '../reusableComponents/footer.php'; ?>
        <!-- End Footer Area -->
        <!-- Login Form -->
        <?php include '../reusableComponents/auth.php'; ?>
        <!-- //Login Form -->
	</div><!-- //Main wrapper -->

	<!-- JS Files -->
	<script src="../js/vendor/jquery-3.2.1.min.js"></script>
	<script src="../js/popper.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/plugins.js"></script>
    <script src="../js/active.js"></script>
    <script src="../js/login.js"></script>

    <script>
        function _goToMealDetail(el){
            //alert(el);
           window.location.href="menu-details.php?el="+el;
        }
    </script>

    <script>
        function validateEmail(email) {
            var re = /\S+@\S+\.\S+/;
            return re.test(email);
        }
        function _Login() {  
            var userID = document.getElementById("username").value;
            var Password = document.getElementById("userPassword").value;

            if(userID.length < 9){
                alert('Invalid User ID');
            }
            else {
                $.ajax({
                    url: '../controller/login.php',
                    type: 'POST',
                    data: {
                        username: userID,
                        userPassword: Password
                    },
                    success: function(msg) {
                        // if(msg==0){
                        //     alert("User does not exist.");
                        // }
                        // else if(msg==1){
                        //     alert("User Account is inactive.");
                        // }
                        // else if(msg==3){
                        //     alert("Incorrect password.");
                        // }
                        alert(msg);
                    }               
                });
            }
        }
        
        function _SignIn() {  
            var fistName = document.getElementById("fistName").value;
            var surName = document.getElementById("surName").value;
            var userID = document.getElementById("userID").value;
            var email = document.getElementById("email").value;
            var phoneNumber = document.getElementById("phoneNumber").value;
            var password = document.getElementById("password").value;
            var confrimPassword = document.getElementById("confrimPassword").value;
            var roomNo = document.getElementById("roomNo").value;
            var DeliverySelector = document.getElementById("DeliverySelector").value;
            var genderSRadio = document.querySelector("input[name=demo01]:checked");
            var genderSValue = genderSRadio ? genderSRadio.value : "";
            //console.log(' Value of gender : ',genderSValue)

            if(!(isNaN(fistName))){
                alert('Enter valid First Name.')
            }
            else if(!(isNaN(surName))){
                alert('Enter valid Sur Name.')
            }
            else if(userID.length < 9){
                alert('Invalid User ID');
            }
            else if(!(validateEmail(email))){
                alert("You have entered an invalid email address!")
            }
            else if((isNaN(phoneNumber)) || phoneNumber.length != 7){
                alert('Enter valid phoneNumber Name.')
            }
            else {
                $.ajax({
                    url: '../controller/signup.php',
                    type: 'POST',
                    data: {
                        fistName: fistName,
                        surName:surName,
                        userID:userID,
                        email:email,
                        phoneNumber:phoneNumber,
                        password:password,
                        confrimPassword: confrimPassword,
                        roomNo: roomNo,
                        DeliverySelector: DeliverySelector,
                        demo01: genderSValue
                    },
                    success: function(msg) {
                        if(msg==0){
                            return;
                        }
                        else if(msg==1){
                            alert("User account has been created, Please try to login.");
                        }
                        else if(msg==2){
                            alert("User account already exists, Please try to login.");
                        }
                        else if(msg==3){
                            alert("Email already exists, Please use another email ID.");
                        }
                        else if(msg==4){
                            alert("Password mismatch.");
                        }
                        else if(msg==5){
                            alert("Some values are missing.");
                        }
                        else alert("Something went wrong in your SignUp process, Please try again.");
                    }               
                });
            }
        }

    </script>
</body>
</html>
