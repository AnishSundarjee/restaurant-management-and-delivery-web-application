<?php
    session_start();

    //Setup database connectivity
    $host="127.0.0.1";
    $user="root";
    $password="password";
    $db="aphroditedb";
    $dbPort = "3306";

    $connect=mysqli_connect($host,$user,$password,$db,$dbPort);

    if(! $connect){
        die('Could not connect connect: ') ;
    }
    $getDeliveryLocationList = "SELECT * FROM building_info;";

    if(isset($_SESSION["isLoggedIn"])){
        $isLogedin = $_SESSION["isLoggedIn"];
    }

?>
<!doctype html>
<html class="no-js" lang="zxx">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>About-Us || Afredieti</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Favicons -->
	<link rel="shortcut icon" href="../images/favicon.ico">
	<link rel="apple-touch-icon" href="../images/icon.png">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/plugins.css">
	<link rel="stylesheet" href="../style.css">
	<!-- Cusom css -->
   <link rel="stylesheet" href="../css/custom.css">

	<!-- Modernizer js -->
	<script src="../js/vendor/modernizr-3.5.0.min.js"></script>
</head>
<body>
	<!--[if lte IE 9]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
	<![endif]-->

	<!-- Add your site or application content here -->
	
	<!-- <div class="fakeloader"></div> -->

	<!-- Main wrapper -->
	<div class="wrapper" id="wrapper">
        <!-- Start Header Area -->
        <?php include '../reusableComponents/header.php'; ?>
        <!-- End Header Area -->
        <!-- Start Bradcaump area -->
        <div class="ht__bradcaump__area bg-image--28">
            <div class="ht__bradcaump__wrap d-flex align-items-center">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="bradcaump__inner text-center brad__white">
                                <h2 class="bradcaump-title">about us</h2>
                                <nav class="bradcaump-inner">
                                  <a class="breadcrumb-item" href="index.php">Home</a>
                                  <span class="brd-separetor"><i class="zmdi zmdi-long-arrow-right"></i></span>
                                  <span class="breadcrumb-item active">about us</span>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Bradcaump area -->
        <section class="food__service bg--white section-padding--lg">
            <div class="container service__container">
                <div class="row">
                    <!-- Start Single Service -->
                    <div class="col-lg-6 col-md-12 col-sm-12">
                        <div class="service--2">
                            <div class="service__inner">
                                <div class="service__content">
                                    <h2><a href="about-us.php">fast delivery</a></h2>
                                    <div class="ser__icon">
                                        <img src="../images/shape/service/1.png" alt="icon images">
                                    </div>
                                </div>
                                <div class="service__hover__action d-flex align-items-center">
                                    <div class="service__hover__inner">
                                        <h4><a href="about-us.php">fast delivery</a></h4>
                                        <p>If you're too busy to head down to the resturant to pick your meal,
                                            do not hesitate to to use our delivery service where your meal will
                                            be delivered to you right at your location.
                                            
                                            We guarantee: </p>
                                        <ul>
                                            <li><a href="#">1.On time delivery within allocated time</a></li>
                                            <li><a href="#">2.Low costs for delivery</a></li>
                                            
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Start Single Service -->
                    <!-- Start Single Service -->
                    <div class="col-lg-6 col-md-12 col-sm-12">
                        <div class="service--2">
                            <div class="service__inner">
                                <div class="service__content">
                                    <h2><a href="about-us.php">quality food</a></h2>
                                    <div class="ser__icon">
                                        <img src="../images/shape/service/3.png" alt="icon images">
                                    </div>
                                </div>
                                <div class="service__hover__action d-flex align-items-center">
                                    <div class="service__hover__inner">
                                        <h4><a href="about-us.php">quality food</a></h4>
                                        <p>Our variety of quality food dishes will help you through your day,
                                        whether it be at the beginning of the day- Breakfast, Midday- Lunch
                                        or in the evening- Dinner.
                                        As the saying goes
		                                            "First we eat, then we do everything else"
                                            Do not hesitate. Place your orders NOW !! </p>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Start Single Service -->
                    <!-- Start Single Service -->
                    <div class="col-lg-6 col-md-12 col-sm-12">
                        <div class="service--2">
                            <div class="service__inner">
                                <div class="service__content">
                                    <h2><a href="about-us.php">various menu</a></h2>
                                    <div class="ser__icon">
                                        <img src="../images/shape/service/4.png" alt="icon images">
                                    </div>
                                </div>
                                <div class="service__hover__action d-flex align-items-center">
                                    <div class="service__hover__inner">
                                        <h4><a href="about-us.php">various menu</a></h4>
                                        <p>Our variety of menus will leave you mesmerized as you have 
                                        a wide range of meals to choose from. With our Combo deals, Weekly
                                        Specials and Meal Subscriptions, you wouldn't have to order anywhere else. </p>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Start Single Service -->
                    <!-- Start Single Service -->
                    <div class="col-lg-6 col-md-12 col-sm-12">
                        <div class="service--2">
                            <div class="service__inner">
                                <div class="service__content">
                                    <h2><a href="about-us.php">well service</a></h2>
                                    <div class="ser__icon">
                                        <img src="../images/shape/service/4.png" alt="icon images">
                                    </div>
                                </div>
                                <div class="service__hover__action d-flex align-items-center">
                                    <div class="service__hover__inner">
                                        <h4><a href="about-us.php">well service</a></h4>
                                        <p>Staffed with the best employees, our team will ensure that your meals are
                                        prepared with love and appreciation. To ensure that you get the best meal, at
                                        the right time with the warmest greetings from the restaurant staff and from
                                        our delivery team. </p>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Start Single Service -->
                </div>
            </div>
        </section>
        <!-- End Service Area -->

        <!-- Start Our Team Area -->
        <section class="food__team__area team--2 bg--white section-padding--lg bg-image--21">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="section__title title__style--2 service__align--center section__bg__black">
                            <h2 class="title__line">Meet Our Team</h2>
                       
                        </div>
                    </div>
                </div>
                <div class="row mt--40">
                    <!-- Start Single Team -->
                    <div class="col-lg-3 col-md-3">
                        <div class="team text-center foo">
                            <div class="team__thumb">
                                <a href="#">
                                    <img src="../images/team/team-list/2.jpg" alt="team images">
                                </a>
                            </div>
                            <div class="team__content">
                                <div class="team__info">
                                    <h4><a href="#">Anish Sundarjee</a></h4>
                                    <h6>Founder</h6>
                                </div>
                                <p>Anish is a founding Partner at our company.He assumes the roles of Project Leader 
                                and is the lead developer.</p>
                                <ul class="team__social__net">
                                    <li><a href="#"><i class="zmdi zmdi-google"></i></a></li>
                                    <li><a href="#"><i class="zmdi zmdi-rss"></i></a></li>
                                    <li><a href="#"><i class="zmdi zmdi-tumblr"></i></a></li>
                                    <li><a href="#"><i class="zmdi zmdi-instagram"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- End Single Team -->
                    <!-- Start Single Team -->
                    <div class="col-lg-3 col-md-3">
                        <div class="team text-center foo">
                            <div class="team__thumb">
                                <a href="#">
                                    <img src="../images/team/team-list/3.jpg" alt="team images">
                                </a>
                            </div>
                            <div class="team__content">
                                <div class="team__info">
                                    <h4><a href="#">Savneel Prasad</a></h4>
                                    <h6>Co-Founder</h6>
                                </div>
                                <p>Savneel is a co-founder of our company and is responsible for
                                 database development and management.</p>
                                <ul class="team__social__net">
                                    <li><a href="#"><i class="zmdi zmdi-google"></i></a></li>
                                    <li><a href="#"><i class="zmdi zmdi-rss"></i></a></li>
                                    <li><a href="#"><i class="zmdi zmdi-tumblr"></i></a></li>
                                    <li><a href="#"><i class="zmdi zmdi-instagram"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- End Single Team -->
                    <!-- Start Single Team -->
                    <div class="col-lg-3 col-md-3">
                        <div class="team text-center foo">
                            <div class="team__thumb">
                                <a href="#">
                                    <img src="../images/team/team-list/4.jpg" alt="team images">
                                </a>
                            </div>
                            <div class="team__content">
                                <div class="team__info">
                                    <h4><a href="#">Sera Laliqavoka</a></h4>
                                    <h6>Quality Assurance/Tester</h6>
                                </div>
                                <p>Sera's role is to ensure software developed is of quality and is what our
                                customer needs.</p>
                                <ul class="team__social__net">
                                    <li><a href="#"><i class="zmdi zmdi-google"></i></a></li>
                                    <li><a href="#"><i class="zmdi zmdi-rss"></i></a></li>
                                    <li><a href="#"><i class="zmdi zmdi-tumblr"></i></a></li>
                                    <li><a href="#"><i class="zmdi zmdi-instagram"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- End Single Team -->
                    <!-- Start Single Team -->
                    <div class="col-lg-3 col-md-3">
                        <div class="team text-center foo">
                            <div class="team__thumb">
                                <a href="#">
                                    <img src="../images/team/team-list/4.jpg" alt="team images">
                                </a>
                            </div>
                            <div class="team__content">
                                <div class="team__info">
                                    <h4><a href="#">Lusiana Tabalala</a></h4>
                                    <h6>Quality Assurance/Tester</h6>
                                </div>
                                <p>Lusiana's role is to ensure software developed is of quality and is what our
                                customer needs.</p>
                                <ul class="team__social__net">
                                    <li><a href="#"><i class="zmdi zmdi-google"></i></a></li>
                                    <li><a href="#"><i class="zmdi zmdi-rss"></i></a></li>
                                    <li><a href="#"><i class="zmdi zmdi-tumblr"></i></a></li>
                                    <li><a href="#"><i class="zmdi zmdi-instagram"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- End Single Team -->
                </div>
            </div>
        </section>
        <!-- End Our Team Area -->
        <!-- Start Accordion Area -->
        <section class="food__acconrdion__area bg--white section-padding--lg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="section__title title__style--2 service__align--center">
                            <h2 class="title__line">Frequently Asked Questions</h2>
                          
                        </div>
                    </div>
                </div>
                <div class="row mt--80 pb--60">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div id="accordion" class="food_accordion" role="tablist">
                            <div class="card">
                                <div class="acc-header" role="tab" id="headingOne">
                                  <h5>
                                        <a data-toggle="collapse" href="#collapseOne" role="button" aria-expanded="true" aria-controls="collapseOne"><span>1.</span>Are the prices the same when ordered online & at the restaurant?</a>
                                  </h5>
                                </div>

                                <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="card-body">The prices of the meals are the same, the only extra charge will be for the delivery service if you choose for your meals to be delivered to you.
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="acc-header" role="tab" id="headingTwo">
                                  <h5>
                                    <a class="collapsed" data-toggle="collapse" href="#collapseTwo" role="button" aria-expanded="false" aria-controls="collapseTwo">
                                        <span>2.</span>How can I pay for my order? 
                                    </a>
                                  </h5>
                                </div>
                                <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
                                  <div class="card-body">We accept Visa, MasterCard and debit cards for your convenience,we also allow Staff members to register for Payroll payment where deductions for orders will be made.
                                  </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="acc-header" role="tab" id="headingThree">
                                  <h5>
                                    <a class="collapsed" data-toggle="collapse" href="#collapseThree" role="button" aria-expanded="false" aria-controls="collapseThree">
                                        <span>3.</span> Can I place Meal Subscriptions?  
                                    </a>
                                  </h5>
                                </div>
                                <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">Yes, we accept meal subscriptions. You can choose from our menus and subscribe to a meal plan of your choice.
                                    Payments for subscriptions can be made in advance or as the days go for the subscription.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Accordion Area -->
        <!-- Start Footer Area -->
        <?php include '../reusableComponents/footer.php'; ?>
        <!-- End Footer Area -->
        <!-- Login Form -->
        <?php include '../reusableComponents/auth.php'; ?>
        <!-- //Login Form -->
	</div><!-- //Main wrapper -->

	<!-- JS Files -->
	<script src="../js/vendor/jquery-3.2.1.min.js"></script>
	<script src="../js/popper.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/plugins.js"></script>
    <script src="../js/active.js"></script>
    <!-- <script src="js/login.js"></script> -->

    <script>
        function validateEmail(email) {
            var re = /\S+@\S+\.\S+/;
            return re.test(email);
        }
        function _Login() {  
            var userID = document.getElementById("username").value;
            var Password = document.getElementById("userPassword").value;

            if(userID.length < 9){
                alert('Invalid User ID');
            }
            else {
                $.ajax({
                    url: '../controller/login.php',
                    type: 'POST',
                    data: {
                        username: userID,
                        userPassword: Password
                    },
                    success: function(msg) {
                        // if(msg==0){
                        //     alert("User does not exist.");
                        // }
                        // else if(msg==1){
                        //     alert("User Account is inactive.");
                        // }
                        // else if(msg==3){
                        //     alert("Incorrect password.");
                        // }
                        alert(msg);
                    }               
                });
            }
        }
        
        function _SignIn() {  
            var fistName = document.getElementById("fistName").value;
            var surName = document.getElementById("surName").value;
            var userID = document.getElementById("userID").value;
            var email = document.getElementById("email").value;
            var phoneNumber = document.getElementById("phoneNumber").value;
            var password = document.getElementById("password").value;
            var confrimPassword = document.getElementById("confrimPassword").value;
            var roomNo = document.getElementById("roomNo").value;
            var DeliverySelector = document.getElementById("DeliverySelector").value;
            var genderSRadio = document.querySelector("input[name=demo01]:checked");
            var genderSValue = genderSRadio ? genderSRadio.value : "";
            //console.log(' Value of gender : ',genderSValue)

            if(!(isNaN(fistName))){
                alert('Enter valid First Name.')
            }
            else if(!(isNaN(surName))){
                alert('Enter valid Sur Name.')
            }
            else if(userID.length < 9){
                alert('Invalid User ID');
            }
            else if(!(validateEmail(email))){
                alert("You have entered an invalid email address!")
            }
            else if((isNaN(phoneNumber)) || phoneNumber.length != 7){
                alert('Enter valid phoneNumber Name.')
            }
            else {
                $.ajax({
                    url: '../controller/signup.php',
                    type: 'POST',
                    data: {
                        fistName: fistName,
                        surName:surName,
                        userID:userID,
                        email:email,
                        phoneNumber:phoneNumber,
                        password:password,
                        confrimPassword: confrimPassword,
                        roomNo: roomNo,
                        DeliverySelector: DeliverySelector,
                        demo01: genderSValue
                    },
                    success: function(msg) {
                        if(msg==0){
                            return;
                        }
                        else if(msg==1){
                            alert("User account has been created, Please try to login.");
                        }
                        else if(msg==2){
                            alert("User account already exists, Please try to login.");
                        }
                        else if(msg==3){
                            alert("Email already exists, Please use another email ID.");
                        }
                        else if(msg==4){
                            alert("Password mismatch.");
                        }
                        else if(msg==5){
                            alert("Some values are missing.");
                        }
                        else alert("Something went wrong in your SignUp process, Please try again.");
                    }               
                });
            }
        }

    </script>
</body>
</html>
