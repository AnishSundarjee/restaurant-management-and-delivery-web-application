<?php
    session_start();
    //Setup database connectivity
    $host="127.0.0.1";
    $user="root";
    $password="password";
    $db="aphroditedb";
    $dbPort = "3306";

    $connect=mysqli_connect($host,$user,$password,$db,$dbPort);
    $output = array();

    function cleanData($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
    }
    if(! $connect){
        die('Could not connect connect: ') ;
    }

    if(isset($_SESSION["userName"])){
        $userId = cleanData($_SESSION["userName"]);
        $orderId = cleanData($_GET["orderId"]);

        $getPaymentAmount = "SELECT sum(OrderTotalPrice) FROM `orders`where CustomerId = '$userId' and OrderId = '$orderId';";
        if($retvalue = mysqli_query($connect,$getPaymentAmount)){
            //print_r($retvalue);
            while($row=mysqli_fetch_row($retvalue)){
               
            }
        }

        if(! $retvalue){
            echo mysqli_error($connect);
        }
    }
    else{
        header("location: ../controller/logout.php");
    }
      
    if(isset($_SESSION["isLoggedIn"])){
        $isLogedin = $_SESSION["isLoggedIn"];
        if($isLogedin == '0'){
            echo "<script type='text/javascript'>alert('User does not exist. $isLogedin');</script>";
        }
        else if($isLogedin == '1'){
            echo "<script type='text/javascript'>alert('User Account is inactive. $isLogedin');</script>";
        }
        else if($isLogedin == '2'){
           // echo "<script type='text/javascript'>alert('Successfull login. $isLogedin');</script>";
        }
        else if($isLogedin == '3'){
            echo "<script type='text/javascript'>alert('Incorrect password. $isLogedin');</script>";
        }
    }

    if(isset($_GET["res"])){
        if($_GET["res"]==2){
            echo "<script type='text/javascript'>alert('Incorrect credentials');</script>";
        }
        else {
            echo "<script type='text/javascript'>alert('Payment completed.');</script>";
        }
    }
    
?>
<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Payment ||  Aahar Food Delivery Html5 Template</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicons -->
    <link rel="shortcut icon" href="../images/favicon.ico">
    <link rel="apple-touch-icon" href="../images/icon.png">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/plugins.css">
    <link rel="stylesheet" href="../style.css">

    <!-- Cusom css -->
   <link rel="stylesheet" href="../css/custom.css">

    <!-- Modernizer js -->
    <script src="../js/vendor/modernizr-3.5.0.min.js"></script>
</head>
<body style="margin-top:20px;">

    <!-- Main wrapper -->
    <div class="wrapper" id="wrapper">
        <header class="htc__header bg--dark">
            <!-- Start Mainmenu Area -->
            <div id="sticky-header-with-topbar" class="mainmenu__wrap sticky__header">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-2 col-sm-4 col-md-6 order-1 order-lg-1">
                            <div class="logo">
                                <a href="index.php">
                                    <img src="../images/logo/foody.png" alt="logo images">
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- Mobile Menu -->
                    <div class="mobile-menu d-block d-lg-none"></div>
                    <!-- Mobile Menu -->
                </div>
            </div>
            <!-- End Mainmenu Area -->
        </header>
         <section class="htc__checkout bg--white section-padding--lg">
             
            <!-- Checkout Section Start-->
            <div class="checkout-section">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-xs-12 col-md-4">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title" style="display: inline;font-weight: bold;">
                                        Payment Details
                                    </h3>
                                   
                                </div>
                                <div class="panel-body">
                                    <form  id="DeliveryLocation" method="POST"action="../controller/processPayment.php">
                                        <div class="row">
                                                <div class="input-group">
                                                    <input type="radio" name="orderId" id="free" value="<?php echo $orderId ?>" checked="checked" /><?php echo $orderId ?>
                                                </div>

                                                <label for="payrollId">
                                                    PAYROLL ID</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="payrollId" name="payrollId" placeholder="Valid Payroll ID"
                                                        required autofocus />
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                                                </div>
                
                                                <label for="Pin">
                                                    PIN</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" id="Pin" name = "Pin" placeholder="Valid PIN"
                                                        required />
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                                                </div>
                                            <div class="col-6 mb--8">                              
                                                <button id="submitDeliveryLocationInfo" name="submitDeliveryLocationInfo" class="food__btn" >Confirm</button>
                                            </div>          
                                        </div>
                                    </form>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>   
            </div><!-- Checkout Section End-->             
         </section>   
        <!-- Start Footer Area -->
        <footer class="footer__area footer--1">
            <div class="footer__wrapper bg__cat--1 section-padding--lg">
                <div class="container">
                    <div class="row">
                        <!-- Start Single Footer -->
                        <div class="col-md-6 col-lg-4 col-sm-12">
                            <div class="footer">
                                <h2 class="ftr__title">About Afredieti</h2>
                                <div class="footer__inner">
                                    <div class="ftr__details">
                                        <p>The true plessures in life begin with a single food delivery,</p>
                                        <div class="ftr__address__inner">
                                            <div class="ftr__address">
                                                <div class="ftr__address__icon">
                                                    <i class="zmdi zmdi-home"></i>
                                                </div>
                                                <div class="frt__address__details">
                                                    <p>USP. ICT building TL3 Suva, Fiji</p>
                                                </div>
                                            </div>
                                            <div class="ftr__address">
                                                <div class="ftr__address__icon">
                                                    <i class="zmdi zmdi-phone"></i>
                                                </div>
                                                <div class="frt__address__details">
                                                    <p><a href="#">+679 555-5555</a></p>
                                                    <p><a href="#">+679 555-5555</a></p>
                                                </div>
                                            </div>
                                            <div class="ftr__address">
                                                <div class="ftr__address__icon">
                                                    <i class="zmdi zmdi-email"></i>
                                                </div>
                                                <div class="frt__address__details">
                                                    <p><a href="#">bigboyvamps540@gmail.com</a></p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <ul class="social__icon">
                                            <li><a href="#"><i class="zmdi zmdi-facebook"></i></a></li>
                                            <li><a href="#"><i class="zmdi zmdi-google"></i></a></li>
                                            <li><a href="#"><i class="zmdi zmdi-instagram"></i></a></li>
                                            <li><a href="#"><i class="zmdi zmdi-rss"></i></a></li>
                                        </ul> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Footer -->
                        <!-- Start Single Footer -->
                        <div class="col-md-6 col-lg-4 col-sm-12 sm--mt--40">
                            <div class="footer gallery">
                                <h2 class="ftr__title">Our Gallery</h2>
                                <div class="footer__inner">
                                    <ul class="sm__gallery__list">
                                        <li><a href="#"><img src="../images/gallery/sm-img/7.jpg" alt="gallery images"></a></li>
                                        <li><a href="#"><img src="../images/gallery/sm-img/8.jpg" alt="gallery images"></a></li>
                                        <li><a href="#"><img src="../images/gallery/sm-img/9.jpg" alt="gallery images"></a></li>
                                        <li><a href="#"><img src="../images/gallery/sm-img/10.png" alt="gallery images"></a></li>
                                        <li><a href="#"><img src="../images/gallery/sm-img/11.jpg" alt="gallery images"></a></li>
                                        <li><a href="#"><img src="../images/gallery/sm-img/12.jpg" alt="gallery images"></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Footer -->
                        <!-- Start Single Footer -->
                        <div class="col-md-6 col-lg-4 col-sm-12 md--mt--40 sm--mt--40">
                            <div class="footer">
                                <h2 class="ftr__title">Opening Time</h2>
                                <div class="footer__inner">
                                    <ul class="opening__time__list">
                                        <li>Monday<span>.......</span>9am to 11pm</li>
                                        <li>Tuesday<span>.......</span>9am to 11pm</li>
                                        <li>Wednesday<span>.......</span>9am to 11pm</li>
                                        <li>Thursday<span>.......</span>9am to 11pm</li>
                                        <li>Friday<span>.......</span>9am to 11pm</li>
                                        <li>Saturday<span>.......</span>9am to 11pm</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Footer -->
                       
                    </div>
                </div>
            </div>
            <div class="copyright bg--theme">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="copyright__inner">
                                <div class="cpy__right--left">
                                    <p>@All Right Reserved.<a href="https://freethemescloud.com/">Free themes Cloud</a></p>
                                </div>
                                <!-- <div class="cpy__right--right">
                                    <a href="#">
                                        <img src="images/icon/shape/2.png" alt="payment images">
                                    </a>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- End Footer Area -->
    </div><!-- //Main wrapper -->

    <!-- JS Files -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/plugins.js"></script>
    <script src="../js/active.js"></script>
    <script src="../js/login.js"></script>

    <!-- <script>
        function _processpay () { 
            var Pin1 = document.getElementById("Pin").value;
            var payId = document.getElementById("payrollId").value;
            var ordernum = document.getElementById("orderId").value;
         }
    </script> -->
</body>
</html>
