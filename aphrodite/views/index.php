<?php
    session_start();

    //Setup database connectivity
    $host="127.0.0.1";
    $user="root";
    $password="password";
    $db="aphroditedb";
    $dbPort = "3306";

    $connect=mysqli_connect($host,$user,$password,$db,$dbPort);


    if(! $connect){
        die('Could not connect connect: ') ;
      }
        $getDeliveryLocationList = "SELECT * FROM building_info;";
        $getItemList = "CALL spFetchItemList();";

        $json_array = array();

        if($retvalue = mysqli_query($connect,$getItemList)){
          while($row=mysqli_fetch_assoc($retvalue)){
              $json_array[]=$row;
          }
        }  
        mysqli_free_result($retvalue); 
        mysqli_next_result($connect);  

        // echo '<pre>';
        // print_r($json_array);
        // echo '</pre>';

    if(isset($_SESSION["isLoggedIn"])){
        $isLogedin = $_SESSION["isLoggedIn"];
    }
    
    if(isset($_SESSION["cartItemCount"])){
        $cartCountValue=$_SESSION["cartItemCount"];
    }

    if(isset($_GET["res"])){
        if($_GET["res"]==2){
            echo "<script type='text/javascript'>alert('Incorrect credentials');</script>";
        }
        else {
            echo "<script type='text/javascript'>alert('Payment completed.');</script>";
        }
    }

    $randNumber = rand(1,1000);
    
?>
<!doctype html>
<html class="no-js" lang="zxx">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Afredieti || Food Delivery</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Favicons -->
	<link rel="shortcut icon" href="../images/favicon.ico">
	<link rel="apple-touch-icon" href="../images/icon.png">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/plugins.css">
    <link rel="stylesheet" href="../style.css">

	<!-- Cusom css -->
 <link rel="stylesheet" href="../css/custom.css">

 <!-- Modernizer js -->
 <script src="../js/vendor/modernizr-3.5.0.min.js"></script>
</head>
<body>
	<!--[if lte IE 9]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
	<![endif]-->

	<!-- Add your site or application content here -->
	
	<!-- <div class="fakeloader"></div> -->

	<!-- Main wrapper -->
	<div class="wrapper" id="wrapper">
        <!-- Start Header Area -->
        <?php include '../reusableComponents/header.php'; ?>
        <!-- End Header Area -->
        <!-- Start Slider Area -->
        <div class="slider__area slider--one">
            <div class="slider__activation--1">
                <!-- Start Single Slide -->
                <div class="slide fullscreen bg-image--26">
                
                    <div class="container">
                    
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="slider__content">
                                
                                    <div class="slider__inner">
                                        <h2>Afredieti</h2>
                                        <h1>food delivery & service</h1>
                                        <div class="slider__input">
                                           
                                            <div class="containerSearchBox">
                                               <input class="res__search search-input" type="text" placeholder="What would you like to eat.." id="autocomplete" required/>
                                                <div class="suggestions" onclick="_searchTextbox()"></div>
                                            </div>
                                            <div class="src__btn">
                                                <a href="#">Search</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Single Slide -->
            </div>
        </div>
        <!-- End Slider Area -->
        <!-- Start Service Area -->
        <section class="fd__service__area bg-image--2 section-padding--xlg">
            <div class="container">
                <div class="service__wrapper bg--white">
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <div class="section__title service__align--left">
                                
                                <h2 class="title__line">How it works</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row mt--30">
                        <!-- Start Single Service -->
                        <div class="col-sm-12 col-md-6 col-lg-4">
                            <div class="service">
                                <div class="service__title">
                                    <div class="ser__icon">
                                        <img src="../images/icon/color-icon/1.png" alt="icon image">
                                    </div>
                                    <h2><a href="resturantView.php">Choose restaurant</a></h2>
                                </div>
                                <div class="service__details">
                                    <p>Select a restaurant to place your order.</p>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Service -->
                        <!-- Start Single Service -->
                        <div class="col-sm-12 col-md-6 col-lg-4">
                            <div class="service">
                                <div class="service__title">
                                    <div class="ser__icon">
                                        <img src="../images/icon/color-icon/2.png" alt="icon image">
                                    </div>
                                    <h2><a href="menu-list.php">Select what you love to eat</a></h2>
                                </div>
                                <div class="service__details">
                                    <p>Order from the avaialable menus your meal for the day.</p>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Service -->
                        <!-- Start Single Service -->
                        <div class="col-sm-12 col-md-6 col-lg-4">
                            <div class="service">
                                <div class="service__title">
                                    <div class="ser__icon">
                                        <img src="../images/icon/color-icon/3.png" alt="icon image">
                                    </div>
                                    <h2><a href="cart.php">Pickup or delivery</a></h2>
                                </div>
                                <div class="service__details">
                                    <p>Choose to either pick up from the restaurant or have it delivered to your doorstep.</p>
                                </div>
                            </div>
                        </div>
                        <!-- End Single Service -->
                    </div>
                </div>
            </div>
        </section>
        <!-- End Service Area -->
        <!-- Start Special Menu -->
        <section class="fd__special__menu__area bg-image--3 section-pt--lg">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="section__title service__align--left">
                       
                            <h2 class="title__line">Restaurant Menu</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="special__food__menu mt--80">
                <div class="food__menu__prl bg-image--4">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="food__nav nav nav-tabs" role="tablist">
                                    <a class="active" id="nav-all-tab" data-toggle="tab" href="#nav-all" role="tab">All</a>
                                    <a id="nav-breakfast-tab" data-toggle="tab" href="#nav-breakfast" role="tab">Breakfast</a>
                                    <a id="nav-lunch-tab" data-toggle="tab" href="#nav-lunch" role="tab">Lunch</a>
                                    <a id="nav-dinner-tab" data-toggle="tab" href="#nav-dinner" role="tab">Dinner</a>
                                    <a id="nav-coffee-tab" data-toggle="tab" href="#nav-coffee" role="tab">Coffee</a>
                                    <a id="nav-snacks-tab" data-toggle="tab" href="#nav-snacks" role="tab">Snacks</a>
                                </div>
                                <div class="fd__tab__content tab-content" id="nav-tabContent">
                                    <!-- Start Single tab -->
                                    <div class="single__tab__panel tab-pane fade show active" id="nav-all" role="tabpanel">
                                        <div class="tab__content__wrap">
                                            <!-- Start Single Tab Content -->
                                            <div class="single__tab__content">
                                            <?php
                                            if(is_array($json_array)){
                                                foreach($json_array as $item => $value){
                                                    $ItemQuantityOnHand = $value["ItemQuantityOnHand"];
                                                    $itemid = $value["itemid"];
                                                    $ItemName = $value["ItemName"];
                                                    $ItemPrice = $value["ItemPrice"];
                                                    $MealTypeDescription = $value["MealTypeDescription"];
                                                    $BusinessName = $value["BusinessName"];
                                                    $ItemImageThumbnail = $value["ItemImageThumbnail"];

                                                    if($ItemQuantityOnHand>0){
                                                    ?>
                                                            <!-- Start Single Food -->
                                                            <div class="food__menu">
                                                                <div class="food__menu__thumb">
                                                                    <a onclick="_goToMealDetail('<?php echo $itemid;?>')">
                                                                    <img src="../../../COS_Admin/images/menu_items/<?php echo $ItemImageThumbnail; ?>?<?php echo $randNumber; ?>" alt="product images" width="105" height="109">
                                                                    </a>
                                                                </div>
                                                                <div class="food__menu__details">
                                                                    <div class="fd__menu__title__prize">
                                                                        <h4><a onclick="_goToMealDetail('<?php echo $itemid;?>')"><?php echo $ItemName;?></a></h4>
                                                                        <span class="menu__prize">$<?php echo $ItemPrice;?></span>
                                                                    </div>
                                                                    <div class="fd__menu__details">
                                                                        <p>Food Type : <?php echo $MealTypeDescription;?></p>
                                                                        <p>Resturant Name : <?php echo $BusinessName;?></p>
                                                                        
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- End Single Food -->
                                                    <?php
                                                    }
                                                }
                                            }
                                            ?>      
                                            </div>
                                            <!-- End Single Tab Content -->
                                        </div>
                                    </div>
                                    <!-- End Single tab -->
                                    <!-- Start Single tab -->
                                    <div class="single__tab__panel tab-pane fade" id="nav-breakfast" role="tabpanel">
                                        <div class="tab__content__wrap">
                                            <!-- Start Single Tab Content -->
                                            <div class="single__tab__content">
                                            <?php
                                            if(is_array($json_array)){
                                                foreach($json_array as $item => $value){
                                                    $ItemQuantityOnHand = $value["ItemQuantityOnHand"];
                                                    $itemid = $value["itemid"];
                                                    $ItemName = $value["ItemName"];
                                                    $ItemPrice = $value["ItemPrice"];
                                                    $MealTypeDescription = $value["MealTypeDescription"];
                                                    $BusinessName = $value["BusinessName"];
                                                    $ItemCategoryId = $value["ItemCategoryId"];
                                                    $ItemImageThumbnail = $value["ItemImageThumbnail"];

                                                    if($ItemQuantityOnHand>0 && $ItemCategoryId==1){
                                                    ?>
                                                        <!-- Start Single Food -->
                                                        <div class="food__menu">
                                                            <div class="food__menu__thumb">
                                                                <a onclick="_goToMealDetail('<?php echo $itemid;?>')">
                                                                <img src="../../../COS_Admin/images/menu_items/<?php echo $ItemImageThumbnail; ?>?<?php echo $randNumber; ?>" alt="product images" width="105" height="109">
                                                                </a>
                                                            </div>
                                                            <div class="food__menu__details">
                                                                <div class="fd__menu__title__prize">
                                                                    <h4><a onclick="_goToMealDetail('<?php echo $itemid;?>')"><?php echo $ItemName;?></a></h4>
                                                                    <span class="menu__prize">$<?php echo $ItemPrice;?></span>
                                                                </div>
                                                                <div class="fd__menu__details">
                                                                    <p>Food Type : <?php echo $MealTypeDescription;?></p>
                                                                    <p>Resturant Name : <?php echo $BusinessName;?></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End Single Food -->
                                                    <?php
                                                    }
                                                }
                                            }
                                            ?>             
                                            </div>
                                            <!-- End Single Tab Content -->
                                        </div>
                                    </div>
                                    <!-- End Single tab -->
                                    <!-- Start Single tab -->
                                    <div class="single__tab__panel tab-pane fade" id="nav-lunch" role="tabpanel">
                                        <div class="tab__content__wrap">
                                            <!-- Start Single Tab Content -->
                                            <div class="single__tab__content">
                                            <?php
                                            if(is_array($json_array)){
                                                foreach($json_array as $item => $value){
                                                    $ItemQuantityOnHand = $value["ItemQuantityOnHand"];
                                                    $itemid = $value["itemid"];
                                                    $ItemName = $value["ItemName"];
                                                    $ItemPrice = $value["ItemPrice"];
                                                    $MealTypeDescription = $value["MealTypeDescription"];
                                                    $BusinessName = $value["BusinessName"];
                                                    $ItemCategoryId = $value["ItemCategoryId"];
                                                    $ItemImageThumbnail = $value["ItemImageThumbnail"];

                                                    if($ItemQuantityOnHand>0 && $ItemCategoryId==2){
                                                    ?>
                                                        <!-- Start Single Food -->
                                                        <div class="food__menu">
                                                            <div class="food__menu__thumb">
                                                                <a onclick="_goToMealDetail('<?php echo $itemid;?>')">
                                                                <img src="../../../COS_Admin/images/menu_items/<?php echo $ItemImageThumbnail; ?>?<?php echo $randNumber; ?>" alt="product images" width="105" height="109">
                                                                </a>
                                                            </div>
                                                            <div class="food__menu__details">
                                                                <div class="fd__menu__title__prize">
                                                                    <h4><a onclick="_goToMealDetail('<?php echo $itemid;?>')"><?php echo $ItemName;?></a></h4>
                                                                    <span class="menu__prize">$<?php echo $ItemPrice;?></span>
                                                                </div>
                                                                <div class="fd__menu__details">
                                                                    <p>Food Type : <?php echo $MealTypeDescription;?></p>
                                                                    <p>Resturant Name : <?php echo $BusinessName;?></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End Single Food -->
                                                    <?php
                                                    }
                                                }
                                            }
                                            ?>             
                                            </div>
                                            <!-- End Single Tab Content -->
                                        </div>
                                    </div>
                                    <!-- End Single tab -->
                                    <!-- Start Single tab -->
                                    <div class="single__tab__panel tab-pane fade" id="nav-dinner" role="tabpanel">
                                        <div class="tab__content__wrap">
                                            <!-- Start Single Tab Content -->
                                            <div class="single__tab__content">
                                            <?php
                                            if(is_array($json_array)){
                                                foreach($json_array as $item => $value){
                                                    $ItemQuantityOnHand = $value["ItemQuantityOnHand"];
                                                    $itemid = $value["itemid"];
                                                    $ItemName = $value["ItemName"];
                                                    $ItemPrice = $value["ItemPrice"];
                                                    $MealTypeDescription = $value["MealTypeDescription"];
                                                    $BusinessName = $value["BusinessName"];
                                                    $ItemCategoryId = $value["ItemCategoryId"];
                                                    $ItemImageThumbnail = $value["ItemImageThumbnail"];

                                                    if($ItemQuantityOnHand>0 && $ItemCategoryId==3){
                                                    ?>
                                                        <!-- Start Single Food -->
                                                        <div class="food__menu">
                                                            <div class="food__menu__thumb">
                                                                <a onclick="_goToMealDetail('<?php echo $itemid;?>')">
                                                                <img src="../../../COS_Admin/images/menu_items/<?php echo $ItemImageThumbnail; ?>?<?php echo $randNumber; ?>" alt="product images" width="105" height="109">
                                                                </a>
                                                            </div>
                                                            <div class="food__menu__details">
                                                                <div class="fd__menu__title__prize">
                                                                    <h4><a onclick="_goToMealDetail('<?php echo $itemid;?>')"><?php echo $ItemName;?></a></h4>
                                                                    <span class="menu__prize">$<?php echo $ItemPrice;?></span>
                                                                </div>
                                                                <div class="fd__menu__details">
                                                                    <p>Food Type : <?php echo $MealTypeDescription;?></p>
                                                                    <p>Resturant Name : <?php echo $BusinessName;?></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End Single Food -->
                                                    <?php
                                                    }
                                                }
                                            }
                                            ?>              
                                            </div>
                                            <!-- End Single Tab Content -->
                                        </div>
                                    </div>
                                    <!-- End Single tab -->
                                    <!-- Start Single tab -->
                                    <div class="single__tab__panel tab-pane fade" id="nav-coffee" role="tabpanel">
                                        <div class="tab__content__wrap">
                                            <!-- Start Single Tab Content -->
                                            <div class="single__tab__content">
                                            <?php
                                            if(is_array($json_array)){
                                                foreach($json_array as $item => $value){
                                                    $ItemQuantityOnHand = $value["ItemQuantityOnHand"];
                                                    $itemid = $value["itemid"];
                                                    $ItemName = $value["ItemName"];
                                                    $ItemPrice = $value["ItemPrice"];
                                                    $MealTypeDescription = $value["MealTypeDescription"];
                                                    $BusinessName = $value["BusinessName"];
                                                    $ItemCategoryId = $value["ItemCategoryId"];
                                                    $ItemImageThumbnail = $value["ItemImageThumbnail"];

                                                    if($ItemQuantityOnHand>0 && $ItemCategoryId==4){
                                                    ?>
                                                        <!-- Start Single Food -->
                                                        <div class="food__menu">
                                                            <div class="food__menu__thumb">
                                                                <a onclick="_goToMealDetail('<?php echo $itemid;?>')">
                                                                <img src="../../../COS_Admin/images/menu_items/<?php echo $ItemImageThumbnail; ?>?<?php echo $randNumber; ?>" alt="product images" width="105" height="109">
                                                                </a>
                                                            </div>
                                                            <div class="food__menu__details">
                                                                <div class="fd__menu__title__prize">
                                                                    <h4><a onclick="_goToMealDetail('<?php echo $itemid;?>')"><?php echo $ItemName;?></a></h4>
                                                                    <span class="menu__prize">$<?php echo $ItemPrice;?></span>
                                                                </div>
                                                                <div class="fd__menu__details">
                                                                    <p>Food Type : <?php echo $MealTypeDescription;?></p>
                                                                    <p>Resturant Name : <?php echo $BusinessName;?></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End Single Food -->
                                                    <?php
                                                    }
                                                }
                                            }
                                            ?>               
                                            </div>
                                            <!-- End Single Tab Content -->
                                        </div>
                                    </div>
                                    <!-- End Single tab -->
                                    <!-- Start Single tab -->
                                    <div class="single__tab__panel tab-pane fade" id="nav-snacks" role="tabpanel">
                                        <div class="tab__content__wrap">
                                            <!-- Start Single Tab Content -->
                                            <div class="single__tab__content">
                                            <?php
                                            if(is_array($json_array)){
                                                foreach($json_array as $item => $value){
                                                    $ItemQuantityOnHand = $value["ItemQuantityOnHand"];
                                                    $itemid = $value["itemid"];
                                                    $ItemName = $value["ItemName"];
                                                    $ItemPrice = $value["ItemPrice"];
                                                    $MealTypeDescription = $value["MealTypeDescription"];
                                                    $BusinessName = $value["BusinessName"];
                                                    $ItemCategoryId = $value["ItemCategoryId"];

                                                    if($ItemQuantityOnHand>0 && $ItemCategoryId==5){
                                                    ?>
                                                        <!-- Start Single Food -->
                                                        <div class="food__menu">
                                                            <div class="food__menu__thumb">
                                                                <a onclick="_goToMealDetail('<?php echo $itemid;?>')">
                                                                <img src="../../../COS_Admin/images/menu_items/<?php echo $ItemImageThumbnail; ?>?<?php echo $randNumber; ?>" alt="product images" width="105" height="109">
                                                                </a>
                                                            </div>
                                                            <div class="food__menu__details">
                                                                <div class="fd__menu__title__prize">
                                                                    <h4><a onclick="_goToMealDetail('<?php echo $itemid;?>')"><?php echo $ItemName;?></a></h4>
                                                                    <span class="menu__prize">$<?php echo $ItemPrice;?></span>
                                                                </div>
                                                                <div class="fd__menu__details">
                                                                    <p>Food Type : <?php echo $MealTypeDescription;?></p>
                                                                    <p>Resturant Name : <?php echo $BusinessName;?></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End Single Food -->
                                                    <?php
                                                    }
                                                }
                                            }
                                            ?>               
                                            </div>
                                            <!-- End Single Tab Content -->
                                        </div>
                                    </div>
                                    <!-- End Single tab -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>      
        <!-- End Special Menu -->
        <!-- Start Footer Area -->
        <?php include '../reusableComponents/footer.php'; ?>
        <!-- End Footer Area -->
        <!-- Login Form -->
        <?php include '../reusableComponents/auth.php'; ?>
        <!-- //Login Form -->
    </div><!-- //Main wrapper -->

    <!-- JS Files -->
    <script src="../js/vendor/jquery-3.2.1.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/plugins.js"></script>
    <script src="../js/active.js"></script>
    <!-- <script src="js/login.js"></script> -->

 
    <script>
        const meal = $.ajax({
            url: '../controller/getSearchList.php',
            type: 'GET',
            success: function(msg) {
                return msg;
            }              
        });
        const meals = [
            {name: 'Mexican'},
            {name: 'Indian'},
            {name: 'Lovo'},
            {name: 'Bread'},
            {name: 'Beef'}
        ];                              
        const searchInput = document.querySelector('.search-input');
        const suggestionsPanel = document.querySelector('.suggestions');

        searchInput.addEventListener('keyup', function() {
            const input = searchInput.value;
            suggestionsPanel.innerHTML = '';
            const suggestions = meals.filter(function(country) {
                return country.name.toLowerCase().startsWith(input);
            });
            suggestions.forEach(function(suggested) {
                const div = document.createElement('div');
                div.setAttribute("id", "suggestionli");
                div.innerHTML = suggested.name;
                suggestionsPanel.appendChild(div);
            });
            if (input === '') {
                suggestionsPanel.innerHTML = '';  
            }
        })
    </script>

    <script>
        function _searchTextbox(){
            const suggestionsPanel = document.getElementById('suggestionli').innerText;
            document.getElementById('autocomplete').value=suggestionsPanel;
        }
    </script>

    <script>
        function _goToMealDetail(el){
           //alert(el);
           //redirect to desired page eg url.com?el=el
           window.location.href="menu-details.php?el="+el;
        }
    </script>

    <script>
        function validateEmail(email) {
            var re = /\S+@\S+\.\S+/;
            return re.test(email);
        }
        function _Login() {  
            var userID = document.getElementById("username").value;
            var Password = document.getElementById("userPassword").value;

            if(userID.length < 9){
                alert('Invalid User ID');
            }
            else {
                $.ajax({
                    url: '../controller/login.php',
                    type: 'POST',
                    data: {
                        username: userID,
                        userPassword: Password
                    },
                    success: function(msg) {
                        // if(msg==0){
                        //     alert("User does not exist.");
                        // }
                        // else if(msg==1){
                        //     alert("User Account is inactive.");
                        // }
                        // else if(msg==3){
                        //     alert("Incorrect password.");
                        // }
                        alert(msg);
                    }               
                });
            }
        }
        
        function _SignIn() {  
            var fistName = document.getElementById("fistName").value;
            var surName = document.getElementById("surName").value;
            var userID = document.getElementById("userID").value;
            var email = document.getElementById("email").value;
            var phoneNumber = document.getElementById("phoneNumber").value;
            var password = document.getElementById("password").value;
            var confrimPassword = document.getElementById("confrimPassword").value;
            var roomNo = document.getElementById("roomNo").value;
            var DeliverySelector = document.getElementById("DeliverySelector").value;
            var genderSRadio = document.querySelector("input[name=demo01]:checked");
            var genderSValue = genderSRadio ? genderSRadio.value : "";
            //console.log(' Value of gender : ',genderSValue)

            if(!(isNaN(fistName))){
                alert('Enter valid First Name.')
            }
            else if(!(isNaN(surName))){
                alert('Enter valid Sur Name.')
            }
            else if(userID.length < 9){
                alert('Invalid User ID');
            }
            else if(!(validateEmail(email))){
                alert("You have entered an invalid email address!")
            }
            else if((isNaN(phoneNumber)) || phoneNumber.length != 7){
                alert('Enter valid phoneNumber Name.')
            }
            else {
                $.ajax({
                    url: '../controller/signup.php',
                    type: 'POST',
                    data: {
                        fistName: fistName,
                        surName:surName,
                        userID:userID,
                        email:email,
                        phoneNumber:phoneNumber,
                        password:password,
                        confrimPassword: confrimPassword,
                        roomNo: roomNo,
                        DeliverySelector: DeliverySelector,
                        demo01: genderSValue
                    },
                    success: function(msg) {
                        if(msg==0){
                            return;
                        }
                        else if(msg==1){
                            alert("User account has been created, Please try to login.");
                        }
                        else if(msg==2){
                            alert("User account already exists, Please try to login.");
                        }
                        else if(msg==3){
                            alert("Email already exists, Please use another email ID.");
                        }
                        else if(msg==4){
                            alert("Password mismatch.");
                        }
                        else if(msg==5){
                            alert("Some values are missing.");
                        }
                        else alert("Something went wrong in your SignUp process, Please try again.");
                    }               
                });
            }
        }

    </script>

</body>
</html>