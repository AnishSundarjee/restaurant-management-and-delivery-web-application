<?php
    session_start();

    //Setup database connectivity
    $host="127.0.0.1";
    $user="root";
    $password="password";
    $db="aphroditedb";
    $dbPort = "3306";

    $connect=mysqli_connect($host,$user,$password,$db,$dbPort);

    function cleanData($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    if(! $connect){
        die('Could not connect connect: ') ;
    }
    $output=array();

    if(isset($_SESSION["userName"])){
        $username=cleanData($_SESSION["userName"]);
        $getDeliveryLocationList = "SELECT * FROM building_info;";
        $viewCartSP = "CALL spViewCart('$username');";
        $viewSubTotalSP = "CALL spViewSubTotal('$username');";

        if($retvalue = mysqli_query($connect,$viewSubTotalSP)){
            while($row=mysqli_fetch_row($retvalue)){
                $CartTotal= $row[0];
                $Tax= $row[1];
                $SubTotal= $row[2];
            }
        }
        mysqli_free_result($retvalue);
        mysqli_next_result($connect);
       
        if($retvalue = mysqli_query($connect,$viewCartSP)){
            while($row=mysqli_fetch_assoc($retvalue)){
                $output[]=$row;
                
            }
        }
    
        if(! $retvalue){
            echo mysqli_error($connect);

        }
        mysqli_free_result($retvalue); 
        mysqli_next_result($connect);  
        
    }
    else{
        header("location: ../controller/logout.php");
    }

    if(isset($_SESSION["isLoggedIn"])){
        $isLogedin = $_SESSION["isLoggedIn"];
    }

    if(isset($_SESSION["cartItemCount"])){
        $cartCountValue=$_SESSION["cartItemCount"];
    }

    if(isset($_GET["isAdded"])){
        $addToCartResponse = $_GET["isAdded"];
        if($addToCartResponse == 1){
            //echo "<script type='text/javascript'>alert('New item added. $addToCartResponse');</script>";
        }
        else if($addToCartResponse == 2){
            //echo "<script type='text/javascript'>alert('Item Quantity Updated. $addToCartResponse');</script>";
        }
        else if($addToCartResponse == 0){
            if(isset($_GET["qty"])){
                $maxQty = $_GET["qty"];
                echo "<script type='text/javascript'>alert('Inventory limit exceeded max orders. Available is $maxQty');</script>";
            }
        }
        else {
            echo "<script type='text/javascript'>alert('Well Shit. $addToCartResponse');</script>";
        }
    }

    if(isset($_GET["isDeleted"])){
        $deleteFromCartResponse = $_GET["isDeleted"];
        if($deleteFromCartResponse == 1){
            echo "<script type='text/javascript'>alert('Delete successful');</script>";
        }
        else if($deleteFromCartResponse == 0){
            echo "<script type='text/javascript'>alert('Item not in cart');</script>";
        }
        else {
            echo "<script type='text/javascript'>alert('Well Shit. $deleteFromCartResponse');</script>";
        }
    }
    $randNumber = rand(1,1000);
?>
<!doctype html>
<html class="no-js" lang="zxx">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Cart ||  Aahar Food Delivery Html5 Template</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Favicons -->
	<link rel="shortcut icon" href="../images/favicon.ico">
	<link rel="apple-touch-icon" href="../images/icon.png">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/plugins.css">
	<link rel="stylesheet" href="../style.css">

	<!-- Cusom css -->
   <link rel="stylesheet" href="../css/custom.css">

	<!-- Modernizer js -->
	<script src="../js/vendor/modernizr-3.5.0.min.js"></script>
</head>
<body>
	<!--[if lte IE 9]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
	<![endif]-->

	<!-- Add your site or application content here -->
	
	<!-- <div class="fakeloader"></div> -->

	<!-- Main wrapper -->
	<div class="wrapper" id="wrapper">
        <!-- Start Header Area -->
        <?php include '../reusableComponents/header.php'; ?>
        <!-- End Header Area -->
        <!-- Start Bradcaump area -->
        <div class="ht__bradcaump__area bg-image--27">
            <div class="ht__bradcaump__wrap d-flex align-items-center">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="bradcaump__inner text-center brad__white">
                                <h2 class="bradcaump-title">cart page</h2>
                                <nav class="bradcaump-inner">
                                  <a class="breadcrumb-item" href="index.php">Home</a>
                                  <span class="brd-separetor"><i class="zmdi zmdi-long-arrow-right"></i></span>
                                  <span class="breadcrumb-item active">cart page</span>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Bradcaump area --> 
        <!-- cart-main-area start -->
        <div class="cart-main-area section-padding--lg bg-image--25">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 ol-lg-12">
                        <form action="#">               
                            <div class="table-content table-responsive">
                                <?php
                                    echo "<table>";
                                    ?>
                                    <thead>
                                        <tr class="title-top">
                                            <th class="product-thumbnail">Image</th>
                                            <th class="product-name">Product</th>
                                            <th class="product-price">Price</th>
                                            <th class="product-quantity">Quantity</th>
                                            <th class="product-resturant">Resturant</th>
                                            <th class="product-subtotal">Total</th>
                                            <th class="product-update">Update</th>
                                            <th class="product-remove">Remove</th>
                                        </tr>
                                    </thead>
                                    <?php
                                    if(!empty($output)){
                                        if(is_array($output)){
                                            foreach ($output as $key => $value) {
                                                $ItemImageThumbnail = $value["ItemImageThumbnail"];
                                                $ItemName = $value["ItemName"];
                                                $ItemPrice = $value["ItemPrice"];
                                                $ItemQuantity = $value["ItemQuantity"];
                                                $BusinessName = $value["BusinessName"];
                                                $TotalPrice = $value["TotalPrice"];
                                                $ItemId = $value["ItemId"];
                                                $BusinessId = $value["BusinessId"];
                                                $ItemStatus = $value["ItemStatus"];

                                                if($ItemStatus == 1){
                                                    ?> 
                                                    <tr>
                                                        <td class="product-thumbnail"><a onclick="_updateCart(<?php echo $ItemId?>)">
                                                            <img src="../../../COS_Admin/images/menu_items/<?php echo $ItemImageThumbnail; ?>?<?php echo $randNumber; ?>" alt="product images" width="101" height="79">
                                                        </a></td>
                                                        <td class="product-name"><h6><b><?php echo $ItemName?><b></h6></td>
                                                        <td class="product-price"><span class="amount"><?php echo '$'.$ItemPrice?></span></td>
                                                        <td class="product-quantity"><b><?php echo $ItemQuantity?><b></td>
                                                        <td class="product-resturant"><b><?php echo $BusinessName?><b></td>
                                                        <td class="product-subtotal"><?php echo '$'.$TotalPrice?></td>
                                                        <td class="product-remove"><h6><u><a onclick="_updateCart(<?php echo $ItemId?>)">Update</a></u></h6></td>
                                                        <td class="product-remove"><a onclick="_removeFromCart(<?php echo $ItemId?>)">X</a></td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                        }                                       
                                    }
                                    echo "</table>";
                                ?>
                            </div>
                        </form> 
                        <div class="cartbox__btn">
                            <ul class="cart__btn__list d-flex flex-wrap flex-md-nowrap flex-lg-nowrap justify-content-between">
                                <li><a href="menu-list.php">Continue Shopping</a></li>
                                <?php if(!empty($output)){
                                    ?>
                                    <li><a href="checkout.php">Check Out</a></li>
                                    <?php
                                } ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 offset-lg-6">
                        <div class="cartbox__total__area">
                            <div class="cartbox-total d-flex justify-content-between">
                                <ul class="cart__total__list">
                                    <li>Cart total</li>
                                    <li>Service Tax</li>
                                </ul>
                                <ul class="cart__total__tk">
                                    <li>$<?php if(!empty($CartTotal)) {echo $CartTotal;} else echo "0.00";?></li>
                                    <li>$<?php echo $Tax?></li>
                                </ul>
                            </div>
                            <div class="cart__total__amount">
                                <span>Sub Total</span>
                                <span>$<?php if(!empty($SubTotal)) {echo $SubTotal;} else echo "0.00";?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
        <!-- cart-main-area end -->
        <!-- Start Footer Area -->
        <?php include '../reusableComponents/footer.php'; ?>
        <!-- End Footer Area -->
        <!-- Login Form -->
        <?php include '../reusableComponents/auth.php'; ?>
        <!-- //Login Form -->
	</div><!-- //Main wrapper -->

	<!-- JS Files -->
	<script src="../js/vendor/jquery-3.2.1.min.js"></script>
	<script src="../js/popper.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/plugins.js"></script>
    <script src="../js/active.js"></script>
    <!-- <script src="js/login.js"></script> -->

    <script>
        function _updateCart(el) {  
            window.location.href="menu-details.php?el="+el;
        }
    </script>

    <script>
        function _removeFromCart (el) { 
            window.location.href="../controller/deleteFromCart.php?el="+el;
         }
    </script>

    <script>
        function validateEmail(email) {
            var re = /\S+@\S+\.\S+/;
            return re.test(email);
        }
        function _Login() {  
            var userID = document.getElementById("username").value;
            var Password = document.getElementById("userPassword").value;

            if(userID.length < 9){
                alert('Invalid User ID');
            }
            else {
                $.ajax({
                    url: '../controller/login.php',
                    type: 'POST',
                    data: {
                        username: userID,
                        userPassword: Password
                    },
                    success: function(msg) {
                        // if(msg==0){
                        //     alert("User does not exist.");
                        // }
                        // else if(msg==1){
                        //     alert("User Account is inactive.");
                        // }
                        // else if(msg==3){
                        //     alert("Incorrect password.");
                        // }
                        alert(msg);
                    }               
                });
            }
        }
        
        function _SignIn() {  
            var fistName = document.getElementById("fistName").value;
            var surName = document.getElementById("surName").value;
            var userID = document.getElementById("userID").value;
            var email = document.getElementById("email").value;
            var phoneNumber = document.getElementById("phoneNumber").value;
            var password = document.getElementById("password").value;
            var confrimPassword = document.getElementById("confrimPassword").value;
            var roomNo = document.getElementById("roomNo").value;
            var DeliverySelector = document.getElementById("DeliverySelector").value;
            var genderSRadio = document.querySelector("input[name=demo01]:checked");
            var genderSValue = genderSRadio ? genderSRadio.value : "";
            //console.log(' Value of gender : ',genderSValue)

            if(!(isNaN(fistName))){
                alert('Enter valid First Name.')
            }
            else if(!(isNaN(surName))){
                alert('Enter valid Sur Name.')
            }
            else if(userID.length < 9){
                alert('Invalid User ID');
            }
            else if(!(validateEmail(email))){
                alert("You have entered an invalid email address!")
            }
            else if((isNaN(phoneNumber)) || phoneNumber.length != 7){
                alert('Enter valid phoneNumber Name.')
            }
            else {
                $.ajax({
                    url: '../controller/signup.php',
                    type: 'POST',
                    data: {
                        fistName: fistName,
                        surName:surName,
                        userID:userID,
                        email:email,
                        phoneNumber:phoneNumber,
                        password:password,
                        confrimPassword: confrimPassword,
                        roomNo: roomNo,
                        DeliverySelector: DeliverySelector,
                        demo01: genderSValue
                    },
                    success: function(msg) {
                        if(msg==0){
                            return;
                        }
                        else if(msg==1){
                            alert("User account has been created, Please try to login.");
                        }
                        else if(msg==2){
                            alert("User account already exists, Please try to login.");
                        }
                        else if(msg==3){
                            alert("Email already exists, Please use another email ID.");
                        }
                        else if(msg==4){
                            alert("Password mismatch.");
                        }
                        else if(msg==5){
                            alert("Some values are missing.");
                        }
                        else alert("Something went wrong in your SignUp process, Please try again.");
                    }               
                });
            }
        }

    </script>
</body>
</html>
