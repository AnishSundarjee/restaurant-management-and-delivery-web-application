<?php
session_start();
$date = date(DATE_ATOM);

//Setup database connectivity
$host="127.0.0.1";
$user="root";
$password="password";
$db="aphroditedb";
$dbPort = "3306";

$connect=mysqli_connect($host,$user,$password,$db,$dbPort);

function cleanData($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
if(! $connect){
    die('Could not connect connect: ') ;
}

$json_array = array();
if(isset($_SESSION["userName"])){
    $userId = cleanData($_SESSION["userName"]);
    $GetOrderDetailsSp = "CALL spGetOrderDetails('$userId');"; 
    $GetOrderDetailsPayAtCounterSp = "CALL spGetOrderDetailsPayAtCounter('$userId');"; 
    if($retvalue = mysqli_query($connect,$GetOrderDetailsSp)){
        // print_r($retvalue);
        while($row=mysqli_fetch_assoc($retvalue)){
            $json_array[]=$row;
        }
    }
    if(! $retvalue){
        echo mysqli_error($connect);
    }

    mysqli_free_result($retvalue); 
    mysqli_next_result($connect); 

    if($retvalue = mysqli_query($connect,$GetOrderDetailsPayAtCounterSp)){
        // print_r($retvalue);
        while($row=mysqli_fetch_assoc($retvalue)){
            $json_array_payatcounter[]=$row;
        }
    }
    if(! $retvalue){
        echo mysqli_error($connect);
    }

    $jsonResponse = json_encode($json_array);
    $jsonResponsePayAtCounter = json_encode($json_array_payatcounter);
}
else {header("location: ../controller/logout.php");}

if(isset($_SESSION["isLoggedIn"])){
    $isLogedin = $_SESSION["isLoggedIn"];
}

    // echo '<pre>';
    // print_r($json_array_payatcounter);
    // echo '</pre>';

?>

<!doctype html>
<html class="no-js" lang="zxx">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Order Tracking || Afredieti</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Favicons -->
	<link rel="shortcut icon" href="../images/favicon.ico">
	<link rel="apple-touch-icon" href="../images/icon.png">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/plugins.css">
	<link rel="stylesheet" href="../style.css">
	<!-- Cusom css -->
   <link rel="stylesheet" href="../css/custom.css">
   <script src="../js/vendor/jquery-3.2.1.min.js"></script>
   <!-- Data tables CDNs -->
   <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.20/datatables.min.css"/>
   <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.20/datatables.min.js"></script>


	<!-- Modernizer js -->
	<script src="../js/vendor/modernizr-3.5.0.min.js"></script>
</head>
<body>
<!-- Main wrapper -->
<div class="wrapper" id="wrapper">
    <!-- Start Header Area -->
    <?php include '../reusableComponents/header.php'; ?>
    <!-- End Header Area -->
    <!-- Start Bradcaump area -->
    <div class="ht__bradcaump__area bg-image--28">
    <h5 style="color:#e60000;"><?php echo $errorReceived; ?></h5>
    <h5 style="color:green;"><?php echo $SignupSuccessReceived; ?></h5>
        <div class="ht__bradcaump__wrap d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="bradcaump__inner text-center brad__white">
                            <h2 class="bradcaump-title">Order Tracking</h2>
                            <nav class="bradcaump-inner">
                                <a class="breadcrumb-item" href="index.php">Home</a>
                                <span class="brd-separetor"><i class="zmdi zmdi-long-arrow-right"></i></span>
                                <span class="breadcrumb-item active">Order Tracking</span>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Bradcaump area -->
    <!-- Start Section area -->
    <section class="food__service bg--white section-padding--lg">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="food__nav nav nav-tabs" role="tablist">
                            <a class="active" id="nav-all-tab" data-toggle="tab" href="#nav-all" role="tab">Payroll Delivery</a>
                            <a id="nav-breakfast-tab" data-toggle="tab" href="#nav-breakfast" role="tab">Pay at counter</a>
                            <a id="nav-lunch-tab" data-toggle="tab" href="#nav-lunch" role="tab">Subscriptions</a>
                            <!-- <a id="nav-dinner-tab" data-toggle="tab" href="#nav-dinner" role="tab">Dinner</a>
                            <a id="nav-coffee-tab" data-toggle="tab" href="#nav-coffee" role="tab">Coffee</a>
                            <a id="nav-snacks-tab" data-toggle="tab" href="#nav-snacks" role="tab">Snacks</a> -->
                        </div>
                        <div class="fd__tab__content tab-content" id="nav-tabContent">
                            <div class="single__tab__panel tab-pane fade show active" id="nav-all" role="tabpanel">
                                <!-- Start Payroll Order Tracking area -->
                                <div class="wrap-input100 validate-input" data-validate="Valid Order ID is required: eg.1">
                                    <input class="input100" type="text" name="myInput" id="myInput" onkeyup="_searchByOrderId()" placeholder="Search for Order No..">
                                    <span class="focus-input100"></span>
                                    <span class="symbol-input100">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </span>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-condensed" id="mydata">
                                        <thead>
                                            <tr>
                                                <th>BatchOrderId</th>  
                                                <th>DeliveryMethod</th>  
                                                <th>Business Name</th>  
                                                <th>Business Address</th>  
                                                <th>Building Description</th>  
                                                <th>RoomNo</th>  
                                                <th>Delivery Time</th>  
                                                <th>Delivery Date</th>  
                                                <th>Meal Name</th>  
                                                <th>Quantity</th>  
                                                <th>Total Price</th>  
                                                <th>Order Status</th>  
                                                <th>Action</th>  
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <!-- End Payroll Order Tracking area -->
                            </div>
                            <div class="single__tab__panel tab-pane fade" id="nav-breakfast" role="tabpanel">
                                <!-- Start PayAtCounter Order Tracking area -->
                                <div class="wrap-input100 validate-input" data-validate="Valid Order ID is required: eg.1">
                                    <input class="input100" type="text" name="myInputPayAtCounter" id="myInputPayAtCounter" onkeyup="_searchByOrderIdPayAtCounter()" placeholder="Search for Order No..">
                                    <span class="focus-input100"></span>
                                    <span class="symbol-input100">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </span>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-condensed" id="mydataPayAtCounter">
                                        <thead>
                                            <tr>
                                                <th>BatchOrderId</th>  
                                                <th>DeliveryMethod</th>  
                                                <th>Resturant Name</th>  
                                                <th>PickUp Point</th>  
                                                <th>Meal Name</th>  
                                                <th>Quantity</th>  
                                                <th>Total Price</th>  
                                                <th>Order Status</th>  
                                                <th>Action</th>  
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <!-- End PayAtCounter Order Tracking area -->
                            </div>
                            <div class="single__tab__panel tab-pane fade" id="nav-lunch" role="tabpanel">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <!-- End Section area -->
    <!-- Start Footer Area -->
    <?php include '../reusableComponents/footer.php'; ?>
    <!-- End Footer Area -->
    <!-- Login Form -->
    <?php include '../reusableComponents/auth.php'; ?>
    <!-- //Login Form -->
</div>

    <!-- JS Files -->
	<script src="../js/popper.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/plugins.js"></script>
    <script src="../js/active.js"></script>
    <script src="../js/login.js"></script>

    <script>  
        $(document).ready(function(){
            var jsonResponse = <?php echo $jsonResponse ?>;
            //declare array parse php into it
            var table=$('#mydata').dataTable( {
                searching: false,
                data: jsonResponse,
                order: [[ 0, "desc" ]],
                keys: {
                    clipboard: true
                },
                columns     :     [  
                    {     data     :     "OrderId" },  
                    {     data     :     "DeliveryMethod", render: function ( data, type, row ) {
                            return 'Home Delivery';
                        } 
                    },  
                    {     data     :     "BusinessName" },  
                    {     data     :     "BusinessAddress" },  
                    {     data     :     "BuildingDescription" },  
                    {     data     :     "RoomNo" },  
                    {     data     :     "TimeslotDescription" },  
                    {     data     :     "DeliveryDate" },  
                    {     data     :     "ItemName" },  
                    {     data     :     "OrderItemQuantity" },  
                    {     data     :     "OrderTotalPrice", render: $.fn.dataTable.render.number(',', '.', 2, '')},  
                    {     data     :     "OrderStatusDescription" }, 
                    {     data     :     "MainOrderId" ,render: function(data) {
                    return '<a class="btn btn-sm  btn-danger btnCopy" id="OrderId" style="margin-left:10px;font-size:10px;" onclick="_disputeOrder(\''+data+'\')"> Dispute</a>'
                        }
                    },  
                ],
            });
        });  
    </script> 

<script>  
        $(document).ready(function(){
            var jsonResponse = <?php echo $jsonResponsePayAtCounter ?>;
            //declare array parse php into it
            var table=$('#mydataPayAtCounter').dataTable( {
                searching: false,
                data: jsonResponse,
                order: [[ 0, "desc" ]],
                keys: {
                    clipboard: true
                },
                columns     :     [  
                    {     data     :     "OrderId" },  
                    {     data     :     "DeliveryMethod", render: function ( data, type, row ) {
                            return 'Pickup';
                        } 
                    },  
                    {     data     :     "BusinessName" },  
                    {     data     :     "BusinessAddress" },  
                    {     data     :     "ItemName" },  
                    {     data     :     "OrderItemQuantity" },  
                    {     data     :     "OrderTotalPrice", render: $.fn.dataTable.render.number(',', '.', 2, '')},  
                    {     data     :     "OrderStatusDescription" }, 
                    {     data     :     "MainOrderId" ,render: function(data) {
                    return '<a class="btn btn-sm  btn-danger btnCopy" id="OrderId" style="margin-left:10px;font-size:10px;" onclick="_disputeOrderPayAtCounter(\''+data+'\')"> Dispute</a>'
                        }
                    },  
                ],
            });
        });  
    </script>

    <script>
        function _searchByOrderId() {
            // Declare variables
            var input, filter, table, tr, td, i, txtValue;
            input = document.getElementById("myInput");
            filter = input.value.toUpperCase();
            table = document.getElementById("mydata");
            tr = table.getElementsByTagName("tr");

            // Loop through all table rows, and hide those who don't match the search query
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[0];
                if (td) {
                    txtValue = td.textContent || td.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }
        
    </script>

    <script>
        function _searchByOrderIdPayAtCounter() {
            // Declare variables
            var input, filter, table, tr, td, i, txtValue;
            input = document.getElementById("myInputPayAtCounter");
            filter = input.value.toUpperCase();
            table = document.getElementById("mydataPayAtCounter");
            tr = table.getElementsByTagName("tr");

            // Loop through all table rows, and hide those who don't match the search query
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[0];
                if (td) {
                    txtValue = td.textContent || td.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }
        
    </script>

    <script>
        function _disputeOrder (el) { 
            $.ajax({
            url: '../controller/cancelOrder.php',
            type: 'POST',
            data: {
                delType: 1,
                orderNum: el
            },
            success: function(msg) {
                if(msg==1){
                    alert('Selected Order has been succesfully cancelled FREE OF CHARGE'); 
                }
                else if(msg==2){
                    alert('Selected order has been cancelled, With no refund.');
                }
                else {
                    alert(msg);
                }
                window.location.href="orderTracking.php";
            }               
        });
        }
    </script>

    <script>
        function _disputeOrderPayAtCounter (el) { 
            $.ajax({
            url: '../controller/cancelOrder.php',
            type: 'POST',
            data: {
                delType: 0,
                orderNum: el
            },
            success: function(msg) {
                if(msg==1){
                    alert('Selected Order has been succesfully cancelled'); 
                }
                else if(msg==2){
                    alert('Selected Order cannot be cancelled, Order is already ready.'); 
                }
                else {
                    alert(msg);
                }
                window.location.href="orderTracking.php";
            }               
        });
        }
    </script>

    <script>
        function validateEmail(email) {
            var re = /\S+@\S+\.\S+/;
            return re.test(email);
        }
        function _Login() {  
            var userID = document.getElementById("username").value;
            var Password = document.getElementById("userPassword").value;

            if(userID.length < 9){
                alert('Invalid User ID');
            }
            else {
                $.ajax({
                    url: '../controller/login.php',
                    type: 'POST',
                    data: {
                        username: userID,
                        userPassword: Password
                    },
                    success: function(msg) {
                        // if(msg==0){
                        //     alert("User does not exist.");
                        // }
                        // else if(msg==1){
                        //     alert("User Account is inactive.");
                        // }
                        // else if(msg==3){
                        //     alert("Incorrect password.");
                        // }
                        alert(msg);
                    }               
                });
            }
        }
        
        function _SignIn() {  
            var fistName = document.getElementById("fistName").value;
            var surName = document.getElementById("surName").value;
            var userID = document.getElementById("userID").value;
            var email = document.getElementById("email").value;
            var phoneNumber = document.getElementById("phoneNumber").value;
            var password = document.getElementById("password").value;
            var confrimPassword = document.getElementById("confrimPassword").value;
            var roomNo = document.getElementById("roomNo").value;
            var DeliverySelector = document.getElementById("DeliverySelector").value;
            var genderSRadio = document.querySelector("input[name=demo01]:checked");
            var genderSValue = genderSRadio ? genderSRadio.value : "";
            //console.log(' Value of gender : ',genderSValue)

            if(!(isNaN(fistName))){
                alert('Enter valid First Name.')
            }
            else if(!(isNaN(surName))){
                alert('Enter valid Sur Name.')
            }
            else if(userID.length < 9){
                alert('Invalid User ID');
            }
            else if(!(validateEmail(email))){
                alert("You have entered an invalid email address!")
            }
            else if((isNaN(phoneNumber)) || phoneNumber.length != 7){
                alert('Enter valid phoneNumber Name.')
            }
            else {
                $.ajax({
                    url: '../controller/signup.php',
                    type: 'POST',
                    data: {
                        fistName: fistName,
                        surName:surName,
                        userID:userID,
                        email:email,
                        phoneNumber:phoneNumber,
                        password:password,
                        confrimPassword: confrimPassword,
                        roomNo: roomNo,
                        DeliverySelector: DeliverySelector,
                        demo01: genderSValue
                    },
                    success: function(msg) {
                        if(msg==0){
                            return;
                        }
                        else if(msg==1){
                            alert("User account has been created, Please try to login.");
                        }
                        else if(msg==2){
                            alert("User account already exists, Please try to login.");
                        }
                        else if(msg==3){
                            alert("Email already exists, Please use another email ID.");
                        }
                        else if(msg==4){
                            alert("Password mismatch.");
                        }
                        else if(msg==5){
                            alert("Some values are missing.");
                        }
                        else alert("Something went wrong in your SignUp process, Please try again.");
                    }               
                });
            }
        }

    </script>
</body>
</html>
