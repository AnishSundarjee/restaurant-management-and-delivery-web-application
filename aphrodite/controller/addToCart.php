<?php
    session_start();

    //Setup database connectivity
    $host="127.0.0.1";
    $user="root";
    $password="password";
    $db="aphroditedb";
    $dbPort = "3306";

    $connect=mysqli_connect($host,$user,$password,$db,$dbPort);
    $output = array();

    function cleanData($data) {
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      return $data;
  }
    if(! $connect){
        die('Could not connect connect: ') ;
      }
      
    if(isset($_SESSION["userName"])){
        //echo $_SESSION["userName"];
        if(isset($_SESSION["itemID"])){
            if(isset($_GET["el"])){
                $username=cleanData($_SESSION["userName"]);
                $itemID=cleanData($_SESSION["itemID"]);
                $itemQty=cleanData($_GET["el"]);
                $addToCartSP = "CALL spAddToCart('$username',$itemID,$itemQty);";
                if($retvalue = mysqli_query($connect,$addToCartSP)){
                    while($row=mysqli_fetch_row($retvalue)){
                      $response = $row[0];
                      $maxQty = $row[1];
                    }
                }
                header("location: ../views/cart.php?isAdded=".$response."&qty=".$maxQty);
                if(! $retvalue){
                    die('Cannot connect to SQL: ');
                }
            }
        }
    }
    else{
        header("location: logout.php");
    }
      
      mysqli_close($connect);
?>
