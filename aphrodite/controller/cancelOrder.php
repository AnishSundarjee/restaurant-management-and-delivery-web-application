<?php
session_start();

//Setup database connectivity
$host="127.0.0.1";
$user="root";
$password="password";
$db="aphroditedb";
$dbPort = "3306";

$connect=mysqli_connect($host,$user,$password,$db,$dbPort);

function cleanData($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
if(! $connect){
    die('Could not connect connect: ') ;
}

if(isset($_SESSION["userName"])){
    if(isset($_POST["orderNum"])){
        $orderNum = cleanData($_POST["orderNum"]);
        if(isset($_POST["delType"])){
            $delType = cleanData($_POST["delType"]);
            if($delType == 1){
                $cancelOrderSP = "CALL spCancelOrder($orderNum);";
                if($retvalue = mysqli_query($connect,$cancelOrderSP)){
                    //print_r($retvalue);
                    while($row=mysqli_fetch_row($retvalue)){
                        echo $row[0];
                    }
                }
                if(! $retvalue){
                    echo mysqli_error($connect);
                }
            }
            else if($delType == 0){
                $cancelOrderPayAtCounterSP = "CALL spCancelPayAtCounterOrder($orderNum);";
                if($retvalue = mysqli_query($connect,$cancelOrderPayAtCounterSP)){
                    //print_r($retvalue);
                    while($row=mysqli_fetch_row($retvalue)){
                        echo $row[0];
                    }
                }
                if(! $retvalue){
                    echo mysqli_error($connect);
                }
            }
        }  
        else{
            {header("location: logout.php");}
        } 
    }
    else{
        {header("location: logout.php");}
    }
}
else{
    {header("location: logout.php");}
}

mysqli_close($connect);
?>