<?php
session_start();

//Setup database connectivity
$host="127.0.0.1";
$user="root";
$password="password";
$db="aphroditedb";
$dbPort = "3306";

$connect=mysqli_connect($host,$user,$password,$db,$dbPort);
$output = array();

function cleanData($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
if(! $connect){
    die('Could not connect connect: ') ;
}
  $json_array = array();
if(isset($_SESSION['userName'])){
    $username=cleanData($_SESSION['userName']);
    $deliveryType=cleanData($_POST['order-shipping']);

    $getOrderId = "SELECT MAX(OrderId) orderid from orders;";
    $getOrderItemPayrollSP = "CALL spGetOrderValuesPayroll('$username',$deliveryType);";
    $getOrderItemsFreeSp = "CALL spGetPayOnDeliveryItemList('$username',$deliveryType);";
    $isUserPayroll = "CALL spCheckPayrollStatus('$username')";
    //$deleteFromOrders = "Delete from cart where CustomerId = '$username');";
    if($retvalue = mysqli_query($connect,$getOrderId)){
        //print_r($retvalue);
        while($row=mysqli_fetch_row($retvalue)){
            $oldOrder=$row[0];
        }
    }
    $orderId = $oldOrder + 1;

    mysqli_free_result($retvalue); 
    mysqli_next_result($connect); 
    if($deliveryType==1){
        if($retvalue = mysqli_query($connect,$isUserPayroll)){
            //print_r($retvalue);
            while($row=mysqli_fetch_row($retvalue)){
                $res=$row[0];
            }
        }
        mysqli_free_result($retvalue); 
        mysqli_next_result($connect); 
        if($res==1){
            if($retvalue = mysqli_query($connect,$getOrderItemPayrollSP)){
                //print_r($retvalue);
                while($row=mysqli_fetch_assoc($retvalue)){
                    $json_array[]=$row;
                }
            }
            
            mysqli_free_result($retvalue); 
            mysqli_next_result($connect); 

            if(!empty($json_array)){
                foreach ($json_array as $key => $value) {
                    $Bid =  cleanData($value["BusinessId"]);
                    $DelMethod =  cleanData($value["DeliveryMethod"]);
                    $BuildingId =  cleanData($value["BuildingId"]);
                    $roomNo =  cleanData($value["roomNo"]);
                    $DeliveryTimeSlot =  cleanData($value["DeliveryTimeSlot"]);
                    $Price =  cleanData($value["TotalPrice"]);
                    $status =  cleanData($value["OrderStatus"]);
                    $qty =  cleanData($value["ItemQuantity"]);
                    $item_id = cleanData($value["ItemId"]);
                    $DeliveryDate = cleanData($value["DeliveryDate"]);
                    
                    $placeOrderPayrollSP = "call spPlaceOrder('$username',$deliveryType,$orderId,'$Bid','$DelMethod','$BuildingId','$roomNo',$DeliveryTimeSlot,'$Price',$status,$qty,$item_id,'$DeliveryDate');";
                     
                    if($retvalue = mysqli_query($connect,$placeOrderPayrollSP)){
                        while($row=mysqli_fetch_row($retvalue)){ 
                                $res= $row[0];
                        }
                    }
        
                    if(! $retvalue){
                        echo mysqli_error($connect);
                    }
    
                   mysqli_free_result($retvalue); 
                   mysqli_next_result($connect); 
                   
                }
                if(! $retvalue){
                    echo mysqli_error($connect);
                }
                header("location: sendMailForConfirmation.php?orderId=".$orderId);
            }
            else header("location: ../views/checkout.php?res=1");
        }
        else header("location: ../views/checkout.php?res=2");
        
    }
    else{
        if($retvalue = mysqli_query($connect,$getOrderItemsFreeSp)){
            //print_r($retvalue);
            while($row=mysqli_fetch_assoc($retvalue)){
                $json_array[]=$row;
            }
        }
        mysqli_free_result($retvalue); 
        mysqli_next_result($connect); 
        if(!empty($json_array)){
            foreach ($json_array as $key => $value) {
                $Bid =  cleanData($value["BusinessId"]);
                $DelMethod =  cleanData($value["DeliveryMethod"]);
                $Price =  cleanData($value["TotalPrice"]);
                $status =  cleanData($value["OrderStatus"]);
                $qty =  cleanData($value["ItemQuantity"]);
                $item_id = cleanData($value["ItemId"]);
                
                $placeOrderSP = "call spPlaceOrder('$username',$deliveryType,$orderId,'$Bid','$DelMethod',1,1,1,'$Price',$status,$qty,$item_id,'1990-01-01');";
                if($retvalue = mysqli_query($connect,$placeOrderSP)){
                    //print_r($retvalue);
                    while($row=mysqli_fetch_row($retvalue)){
                        if($row[0]==2){
                            $res = $row[0];
                        }
                        else return;
                    }
                }
                mysqli_next_result($connect); 
            }
    
            if(! $retvalue){
                echo mysqli_error($connect);
            }
            header("location: sendMailForConfirmation.php?orderId=".$orderId);

        } else header("location: ../views/checkout.php?res=1");
       //header("location: ../views/checkout.php?orderId=".$orderId);
    }

    // echo '<pre>';
    // print_r($json_array);
    // echo '</pre>';
    
}
  
  mysqli_close($connect);
?>