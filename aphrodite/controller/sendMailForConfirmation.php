<?php
session_start();

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

// Load Composer's autoloader
require '../vendor/autoload.php';

$date = date(DATE_ATOM);

//Setup database connectivity
$host="127.0.0.1";
$user="root";
$password="password";
$db="aphroditedb";
$dbPort = "3306";

$connect=mysqli_connect($host,$user,$password,$db,$dbPort);
$output = array();

function cleanData($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
if(! $connect){
    die('Could not connect connect: ') ;
}
$output = array();

if(isset($_GET["orderId"])){
    $orderId = $_GET["orderId"];
    if(isset($_SESSION["userName"])){
        $user_id = $_SESSION["userName"];
        $getEmailInfoSp = "CALL spGetEmailInfo('$orderId');";
        if($retvalue = mysqli_query($connect,$getEmailInfoSp)){
            while($row=mysqli_fetch_array($retvalue)){
                $output = $row;
            }
        }
        if(! $retvalue){
            echo mysqli_error($connect);
        }
    }
    else header("location: logout.php");
}
else header("location: ../views/index.php");

//Instantiation and passing `true` enables exceptions
$mail = new PHPMailer(true);

try {
    //Recipients
    $mail->setFrom('bigboyvamps540@gmail.com', 'Afrodite');
    $mail->addAddress($output[0]);     // Add a recipient

    // Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Meal order confirmation from Afrodite';
    $mail->Body    = 'Hi '.$output[1]. ' your order has been placed. Track your order with id : <b>'.$orderId.'</b> from our order tracking page.';
    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    $mail->send();
    echo header("location: ../views/emailConfirmation.php?orderId=".$orderId);
} catch (Exception $e) {
    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
}

?>