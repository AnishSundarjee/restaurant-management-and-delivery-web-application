<?php

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

// Load Composer's autoloader
require '../vendor/autoload.php';

$mail = new PHPMailer(true);

    function cleanData($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }       
    
    if(isset($_POST['name']) && isset($_POST['email']) && isset($_POST['subject']) && isset($_POST['message'])){
        //Instantiation and passing `true` enables exceptions
        
        try {
                                    
            $name = cleanData($_POST['name']);
            $email = cleanData($_POST['email']);
            $subject = cleanData($_POST['subject']);
            $message = cleanData($_POST['message']);
            $companyEmail = 'bigboyvamps540@gmail.com';
            //echo $email;
            //Recipients
            $mail->setFrom($email, $name);
            $mail->addAddress($companyEmail, 'Afrodite');     // Add a recipient
            $mail->addReplyTo($email, $name);
        
            // Attachments
            $mail->addAttachment('../images/logo/foody.png', 'CompanyLogo.jpg');    // Optional name
        
            // Content
            $mail->isHTML(true);   // Set email format to HTML
            $mail->Subject = $subject;
            $mail->Body    = '<h3>'.$message.'</h3>';
            $mail->AltBody = $message;
        
            $mail->send();
            echo '1';
        } catch (Exception $e) {
            echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        }
    }
    else {
        echo 'something went wrong, Please try again.';
    }

?>