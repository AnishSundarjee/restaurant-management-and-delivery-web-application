<?php
    session_start();

    //Setup database connectivity
    $host="127.0.0.1";
    $user="root";
    $password="password";
    $db="aphroditedb";
    $dbPort = "3306";

    $connect=mysqli_connect($host,$user,$password,$db,$dbPort);


    if(! $connect){
        die('Could not connect connect: ') ;
    }

    $json_array = array();
    $GetSearchListSp = "CALL spFetchSearchItems();"; 
    if($retvalue = mysqli_query($connect,$GetSearchListSp)){
        // print_r($retvalue);
        while($row=mysqli_fetch_assoc($retvalue)){
            $json_array[]=$row;
        }
    }
    if(! $retvalue){
        echo mysqli_error($connect);
    }

    $jsonResponse = json_encode($json_array);

    echo $jsonResponse;

    mysqli_close($connect);
?>