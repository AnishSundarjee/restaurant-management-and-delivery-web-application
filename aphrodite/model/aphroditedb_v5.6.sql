-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 18, 2020 at 11:26 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aphroditedb`
--

DELIMITER $$
--
-- Procedures
--
DROP PROCEDURE IF EXISTS `spAddNewBusiness`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spAddNewBusiness` (IN `business_id` VARCHAR(15), IN `business_name` VARCHAR(50), IN `business_email` VARCHAR(50), IN `business_phone` VARCHAR(10), IN `business_address` VARCHAR(200), IN `user_name` VARCHAR(50), IN `user_role` INT, IN `fname` VARCHAR(50), IN `lname` VARCHAR(50), IN `email` VARCHAR(50), IN `password` VARCHAR(200))  BEGIN
    INSERT INTO business_details (BusinessId, BusinessName, BusinessEmail, BusinessPhone, BusinessAddress) VALUES (business_id, business_name, business_email, business_phone, business_address);
    INSERT INTO users (UserName, RoleId, UserFirstName, UserLastName, UserEmail, UserPassword, BusinessId) VALUES (user_name, user_role, fname, lname, email, password, business_id);

END$$

DROP PROCEDURE IF EXISTS `spAddNewCafeteriaStaff`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spAddNewCafeteriaStaff` (IN `user_name` VARCHAR(50), IN `user_role` INT, IN `fname` VARCHAR(50), IN `lname` VARCHAR(50), IN `email` VARCHAR(50), IN `phone` VARCHAR(10), IN `address` VARCHAR(200), IN `user_password` VARCHAR(200), IN `business_id` VARCHAR(10))  BEGIN
    INSERT INTO users (UserName, RoleId, UserFirstName, UserLastName, UserEmail, UserPhone, UserAddress, UserPassword, BusinessID) VALUES (user_name, user_role, fname, lname, email, phone, address, user_password, business_id);
END$$

DROP PROCEDURE IF EXISTS `spAddNewDeliveryStaff`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spAddNewDeliveryStaff` (IN `user_name` VARCHAR(50), IN `user_role` INT, IN `fname` VARCHAR(50), IN `lname` VARCHAR(50), IN `email` VARCHAR(50), IN `phone` VARCHAR(10), IN `address` VARCHAR(200), IN `password` VARCHAR(200))  BEGIN
    INSERT INTO users (UserName, RoleId, UserFirstName, UserLastName, UserEmail, UserPhone, UserAddress, UserPassword) VALUES (user_name, user_role, fname, lname, email, phone, address, password);
END$$

DROP PROCEDURE IF EXISTS `spAddNewMealCategory`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spAddNewMealCategory` (IN `meal_category` VARCHAR(50))  BEGIN
		INSERT INTO item_category (ItemCategoryDescription) Values (meal_category);
END$$

DROP PROCEDURE IF EXISTS `spAddNewMealType`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spAddNewMealType` (IN `meal_type` VARCHAR(50))  BEGIN
		INSERT INTO meal_type (MealTypeDescription) Values (meal_type);
END$$

DROP PROCEDURE IF EXISTS `spAddNewMenuItem`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spAddNewMenuItem` (IN `item_id` INT, IN `item_name` VARCHAR(50), IN `item_desc` VARCHAR(1000), IN `item_image` MEDIUMTEXT, IN `item_price` DECIMAL(10,2), IN `item_quantity` INT, IN `business_id` VARCHAR(15), IN `meal_category_id` INT, IN `meal_type_id` INT)  BEGIN
    INSERT INTO `item_details`(`ItemId`, `ItemName`, `ItemDescription`, `ItemImageThumbnail`, `ItemPrice`,  `ItemQuantityOnHand`, `BusinessId`, `ItemCategoryId`, `MealTypeId`) VALUES (item_id,item_name,item_desc,item_image,item_price,item_quantity,business_id,meal_category_id,meal_type_id);
END$$

DROP PROCEDURE IF EXISTS `spAddToCart`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spAddToCart` (IN `user_id` VARCHAR(9), IN `item_id` INT, IN `quantity` INT)  BEGIN
	DECLARE existingPrice, newPrice, item_price, total_price DECIMAL(10,2) default 0.0;
    DECLARE newItemQuantityOnHand,maxQty,existingQty, newQuantity, response int DEFAULT 0;

    SELECT ItemPrice INTO item_price FROM item_details WHERE ItemId = item_id;
    SELECT ItemQuantity INTO existingQty FROM cart where CustomerId=user_id AND ItemId = item_id;
    SELECT TotalPrice INTO existingPrice FROM cart where CustomerId=user_id AND ItemId = item_id;
    SELECT ItemQuantityOnHand INTO maxQty FROM item_details where ItemId = item_id;

    Set total_price = item_price * quantity;
    set newQuantity = existingQty + quantity;
    set newPrice = existingPrice + total_price;
    SET newItemQuantityOnHand = maxQty - quantity;

    IF (select count(*) from cart where CustomerId=user_id AND ItemId = item_id) > 0 THEN
        
        IF(maxQty>=newQuantity) THEN
            START TRANSACTION;
            UPDATE item_details SET ItemQuantityOnHand=newItemQuantityOnHand where ItemId = item_id;
            UPDATE cart SET ItemQuantity= newQuantity, TotalPrice = newPrice WHERE CustomerId=user_id AND ItemId = item_id;

            SET response = 2;
            COMMIT;

        ELSE SET response = 0;
        END IF;
    ELSE
        START TRANSACTION;
        
        UPDATE item_details SET ItemQuantityOnHand=newItemQuantityOnHand where ItemId = item_id;
        INSERT INTO cart (CustomerId, ItemId, ItemQuantity, TotalPrice) VALUES (user_id, item_id, quantity, total_price);

        SET response = 1;
        COMMIT;
    END IF;
        SELECT response,maxQty;
END$$

DROP PROCEDURE IF EXISTS `spCancelOrder`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spCancelOrder` (IN `OrderNum` INT)  BEGIN
    DECLARE orderStatusCode, response int;
    DECLARE amountToDeduct, ExistingBal, NewBal decimal(10,2); 
    DECLARE bid varchar(10);
    select OrderStatusId into orderStatusCode from orders where MainOrderId = OrderNum;
    select OrderTotalPrice into amountToDeduct from orders where MainOrderId = OrderNum;
    select BusinessId into bid  from orders where MainOrderId = OrderNum;
    select BusinessBalance into ExistingBal from business_details where BusinessId = bid;

    set NewBal = ExistingBal-amountToDeduct;
    if(orderStatusCode < 2) THEN
        update business_details set BusinessBalance=NewBal where BusinessId = bid;
        update orders set OrderStatusId = 6 where MainOrderId = OrderNum;
        set response = 1;
    ELSE
        update orders set OrderStatusId = 6 where MainOrderId = OrderNum;
        set response = 2;
    END IF;

    select response;
END$$

DROP PROCEDURE IF EXISTS `spCancelPayAtCounterOrder`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spCancelPayAtCounterOrder` (IN `orderNum` INT)  BEGIN
    DECLARE orderStatusCode, response int;
    select OrderStatusId into orderStatusCode from orders where MainOrderId = OrderNum;

    if(orderStatusCode < 2) THEN
        update orders set OrderStatusId = 6 where MainOrderId = OrderNum;
        set response = 1;
    ELSE
        set response = 2;
    END IF;
    select response;
END$$

DROP PROCEDURE IF EXISTS `spCheckPayrollStatus`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spCheckPayrollStatus` (IN `user_id` VARCHAR(9))  BEGIN
	DECLARE response int default 99;
	IF(SELECT count(*) FROM payment_info WHERE CustomerId=user_id)>0 THEN
    set response = 1;
    ELSE SET response = 0;
    end if;
    SELECT response;
END$$

DROP PROCEDURE IF EXISTS `spDeleteBusiness`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spDeleteBusiness` (IN `business_id` VARCHAR(15))  BEGIN
	DELETE FROM users WHERE BusinessId = business_id;
    
    DELETE FROM item_details WHERE BusinessId = business_id;
    
	DELETE FROM business_details WHERE BusinessId = business_id;
END$$

DROP PROCEDURE IF EXISTS `spDeleteCafeteriaStaff`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spDeleteCafeteriaStaff` (IN `cafeteriaStaff_id` INT)  BEGIN
	DELETE FROM users WHERE UserId = cafeteriaStaff_id;
END$$

DROP PROCEDURE IF EXISTS `spDeleteDeliveryStaff`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spDeleteDeliveryStaff` (IN `deliveryStaff_id` INT)  BEGIN
	DELETE FROM users WHERE UserId = deliveryStaff_id;
END$$

DROP PROCEDURE IF EXISTS `spDeleteItemFromCart`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spDeleteItemFromCart` (IN `user_id` VARCHAR(9), IN `item_id` INT)  BEGIN
	DECLARE existingQty,qty,newQty,response int default 99;
    
    IF(select count(*) from cart where CustomerId = user_id AND ItemId = item_id)>0 THEN
        select ItemQuantityOnHand into existingQty from item_details WHERE ItemId=item_id;
        select ItemQuantity into qty from cart where CustomerId = user_id AND ItemId = item_id;

        set newQty = existingQty+qty;
        Start Transaction;
        UPDATE item_details set ItemQuantityOnHand = newQty WHERE ItemId=item_id;
        DELETE FROM cart WHERE CustomerId = user_id AND ItemId = item_id;
        set response = 1;
        Commit;
        
    ELSE set response = 0;
    END IF;
    
    SELECT response;
END$$

DROP PROCEDURE IF EXISTS `spDeleteMealCategory`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spDeleteMealCategory` (IN `mealCategory_id` INT)  BEGIN
	DELETE FROM item_category WHERE ItemCategoryId = mealCategory_id;
END$$

DROP PROCEDURE IF EXISTS `spDeleteMealType`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spDeleteMealType` (IN `mealType_id` INT)  BEGIN
	DELETE FROM meal_type WHERE MealTypeId = mealType_id;
END$$

DROP PROCEDURE IF EXISTS `spDeleteMenuItem`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spDeleteMenuItem` (IN `item_id` INT)  BEGIN
	DELETE FROM item_details WHERE ItemId = item_id;
END$$

DROP PROCEDURE IF EXISTS `spFetchBusinessDetails`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spFetchBusinessDetails` (IN `business_id` VARCHAR(6))  BEGIN
	SELECT BusinessName, BusinessImage, BusinessEmail, BusinessPhone, BusinessAddress, BusinessStatus FROM business_details WHERE BusinessId = business_id;
END$$

DROP PROCEDURE IF EXISTS `spFetchBusinessItemList`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spFetchBusinessItemList` (IN `BId` VARCHAR(5))  BEGIN
    SELECT a.itemid,a.ItemName,a.ItemDescription,a.ItemImageThumbnail,a.ItemPrice,a.ItemDeliveryPrice,
    a.ItemQuantityOnHand,a.ItemRating,a.ItemStatus,a.BusinessId,a.ItemCategoryId,a.MealTypeId,
    b.MealTypeDescription,c.BusinessName
    FROM `item_details` a
    INNER JOIN meal_type b
    on a.MealTypeId=b.MealTypeId
    INNER JOIN business_details c
    on a.businessId=c.BusinessId
    WHERE a.ItemStatus = 1 and a.BusinessId=BId and a.ItemQuantityOnHand>0; 
END$$

DROP PROCEDURE IF EXISTS `spFetchBusinessList`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spFetchBusinessList` ()  BEGIN
    SELECT  BusinessImage, BusinessName, BusinessEmail, BusinessPhone, BusinessAddress, BusinessId FROM business_details
    WHERE BusinessStatus = 1;
END$$

DROP PROCEDURE IF EXISTS `spFetchBusinessList2`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spFetchBusinessList2` ()  BEGIN
	SELECT  BusinessId, BusinessName, BusinessEmail, BusinessPhone, BusinessAddress, BusinessStatus FROM business_details;
END$$

DROP PROCEDURE IF EXISTS `spFetchCafeteriaStaffDetails`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spFetchCafeteriaStaffDetails` (IN `cafeteriaStaff_id` VARCHAR(6))  BEGIN
	SELECT UserName, UserFirstName, UserLastName, UserEmail, UserPhone, UserPassword, UserStatus FROM users WHERE UserId = cafeteriaStaff_id;
END$$

DROP PROCEDURE IF EXISTS `spFetchCafeteriaStaffList`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spFetchCafeteriaStaffList` (IN `business_id` VARCHAR(10))  BEGIN
	SELECT UserId, UserName, UserFirstName, UserLastName, UserEmail, UserPhone, UserAddress, UserStatus FROM users WHERE RoleId = 3 AND BusinessID = business_id;
END$$

DROP PROCEDURE IF EXISTS `spFetchCartItems`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spFetchCartItems` (IN `user_id` VARCHAR(9))  BEGIN
    SELECT b.ItemName,a.ItemQuantity, a.TotalPrice
    FROM cart a
    INNER JOIN item_details b
    on a.ItemId=b.ItemId
    WHERE CustomerId = user_id and b.ItemStatus=1;
END$$

DROP PROCEDURE IF EXISTS `spFetchDeliveryOrderDetails`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spFetchDeliveryOrderDetails` (IN `order_id` INT)  BEGIN
SELECT a.OrderId, a.CustomerId, concat(e.CustomerFirstName, " ", e.CustomerLastName) AS CustomerName, concat(a.DeliveryDate, " | ", c.TimeslotDescription) AS DeliveryTime, concat(d.BuildingDescription," | ", a.RoomNo) AS DeliveryLocation, a.OrderStatusId, b.OrderStatusDescription
    FROM orders a
    INNER JOIN order_status b
    ON a.OrderStatusID = b.OrderStatusID
    INNER JOIN timeslots c
    ON a.DeliveryTimeslotId = c.TimeslotId
    INNER JOIN building_info d
    ON a.BuildingId = d.BuildingId
    INNER JOIN customer_details e
	ON a.CustomerId = e.CustomerId
    WHERE a.OrderId = order_id AND a.DeliveryMethod = 1 AND a.OrderStatusId = 4
    GROUP BY a.OrderId;
END$$

DROP PROCEDURE IF EXISTS `spFetchDeliveryOrderList`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spFetchDeliveryOrderList` ()  BEGIN
SELECT a.OrderId, a.CustomerId, concat(a.DeliveryDate, " | ", c.TimeslotDescription) AS DeliveryTime, concat(d.BuildingDescription," | ", a.RoomNo) AS DeliveryLocation, b.OrderStatusDescription
    FROM orders a
    INNER JOIN order_status b
    ON a.OrderStatusID = b.OrderStatusID
    INNER JOIN timeslots c
    ON a.DeliveryTimeslotId = c.TimeslotId
    INNER JOIN building_info d
    ON a.BuildingId = d.BuildingId
    WHERE a.DeliveryMethod = 1 AND a.OrderStatusId = 4
    GROUP BY a.OrderId;
END$$

DROP PROCEDURE IF EXISTS `spFetchDeliveryStaffDetails`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spFetchDeliveryStaffDetails` (IN `deliveryStaff_id` VARCHAR(6))  BEGIN
	SELECT UserName, UserFirstName, UserLastName, UserEmail, UserPhone, UserPassword, UserStatus FROM users WHERE UserId = deliveryStaff_id;
END$$

DROP PROCEDURE IF EXISTS `spFetchDeliveryStaffList`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spFetchDeliveryStaffList` ()  BEGIN
	SELECT UserId, UserName, UserFirstName, UserLastName, UserEmail, UserPhone, UserAddress, UserStatus FROM users WHERE RoleId = 4;
END$$

DROP PROCEDURE IF EXISTS `spFetchItemDetails`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spFetchItemDetails` (IN `item_id` INT)  BEGIN
SELECT a.itemid,a.ItemName,a.ItemDescription,a.ItemImageThumbnail,a.ItemPrice,a.ItemDeliveryPrice,
   a.ItemQuantityOnHand,a.ItemRating,a.ItemStatus,a.BusinessId,a.ItemCategoryId,a.MealTypeId,
    b.MealTypeDescription,c.BusinessName
    FROM `item_details` a
    INNER JOIN meal_type b
    on a.MealTypeId=b.MealTypeId
    INNER JOIN business_details c
    on a.businessId=c.BusinessId
    WHERE ItemStatus = 1 AND ItemId = item_id;
END$$

DROP PROCEDURE IF EXISTS `spFetchItemDetails2`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spFetchItemDetails2` (IN `item_id` INT)  BEGIN
	SELECT `ItemName`, `ItemDescription`, `ItemImageThumbnail`, `ItemPrice`, `ItemQuantityOnHand`, `ItemStatus`, `ItemCategoryId`, `MealTypeId` 
    FROM `item_details` 
    WHERE ItemId = item_id;
END$$

DROP PROCEDURE IF EXISTS `spFetchItemList`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spFetchItemList` ()  BEGIN
SELECT a.itemid,a.ItemName,a.ItemDescription,a.ItemImageThumbnail,a.ItemPrice,a.ItemDeliveryPrice,
   a.ItemQuantityOnHand,a.ItemRating,a.ItemStatus,a.BusinessId,a.ItemCategoryId,a.MealTypeId,
    b.MealTypeDescription,c.BusinessName
    FROM `item_details` a
    INNER JOIN meal_type b
    on a.MealTypeId=b.MealTypeId
    INNER JOIN business_details c
    on a.businessId=c.BusinessId
    WHERE ItemStatus = 1;
END$$

DROP PROCEDURE IF EXISTS `spFetchItemList2`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spFetchItemList2` (IN `business_id` VARCHAR(6))  BEGIN
	SELECT a.ItemId,a.ItemName,a.ItemDescription,a.ItemImageThumbnail,a.ItemPrice,a.ItemQuantityOnHand,b.ItemCategoryDescription,c.MealTypeDescription,a.ItemStatus 
    FROM item_details a 
    INNER JOIN item_category b on a.ItemCategoryId=b.ItemCategoryId 
    INNER JOIN meal_type c on a.MealTypeId=c.MealTypeId
    WHERE BusinessId = business_id;
END$$

DROP PROCEDURE IF EXISTS `spFetchMealCategories`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spFetchMealCategories` ()  BEGIN
	SELECT 	* FROM item_category;
END$$

DROP PROCEDURE IF EXISTS `spFetchMealCategoryDetails`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spFetchMealCategoryDetails` (IN `mealCategory_id` INT)  BEGIN
	SELECT ItemCategoryDescription FROM item_category WHERE ItemCategoryId = mealCategory_id;
END$$

DROP PROCEDURE IF EXISTS `spFetchMealTypeDetails`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spFetchMealTypeDetails` (IN `mealType_id` INT)  BEGIN
	SELECT MealTypeDescription FROM meal_type WHERE MealTypeId = mealType_id;
END$$

DROP PROCEDURE IF EXISTS `spFetchMealTypes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spFetchMealTypes` ()  BEGIN
	SELECT * FROM meal_type;
END$$

DROP PROCEDURE IF EXISTS `spFetchMenuManagerDetails`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spFetchMenuManagerDetails` (IN `business_id` VARCHAR(6))  BEGIN
	SELECT UserName, UserFirstName, UserLastName, UserEmail, UserStatus, UserPassword FROM users where BusinessId = business_id AND RoleId = 2;
END$$

DROP PROCEDURE IF EXISTS `spFetchOrderDetails`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spFetchOrderDetails` (IN `order_id` INT)  BEGIN
	SELECT a.CustomerId, concat(b.CustomerFirstName, " ", b.CustomerLastName) AS CustomerName, a.DeliveryMethod, c.ItemName, c.ItemPrice, a.OrderItemQuantity, a.OrderTotalPrice, a.OrderStatusId, d.OrderStatusDescription
	FROM orders a
	INNER JOIN customer_details b
	ON a.CustomerId = b.CustomerId
    INNER JOIN item_details c
    ON a.itemId = c.ItemId
    INNER JOIN order_status d
    ON a.OrderStatusID = d.OrderStatusID
    WHERE a.OrderId = order_id;
END$$

DROP PROCEDURE IF EXISTS `spFetchOrderList`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spFetchOrderList` (IN `business_id` VARCHAR(15))  BEGIN
SELECT a.OrderId, a.CustomerId, a.DeliveryMethod, sum(a.OrderTotalPrice) AS OrderTotalPrice, b.OrderStatusDescription
    FROM orders a
    INNER JOIN order_status b
    ON a.OrderStatusID = b.OrderStatusID
    WHERE a.BusinessId = business_id
    GROUP BY a.OrderId;
END$$

DROP PROCEDURE IF EXISTS `spFetchPendingDeliveryOrderList`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spFetchPendingDeliveryOrderList` (IN `business_id` VARCHAR(15))  BEGIN
SELECT a.OrderId, a.CustomerId, concat(a.DeliveryDate, " | ", c.TimeslotDescription) AS DeliveryTime, sum(a.OrderTotalPrice) AS OrderTotalPrice, b.OrderStatusDescription
    FROM orders a
    INNER JOIN order_status b
    ON a.OrderStatusID = b.OrderStatusID
    INNER JOIN timeslots c
    ON a.DeliveryTimeslotId = c.TimeslotId
    WHERE a.BusinessId = business_id AND a.DeliveryMethod = 1 AND a.OrderStatusId = 1 OR a.OrderStatusId = 2 OR a.OrderStatusId = 3
    GROUP BY a.OrderId;
END$$

DROP PROCEDURE IF EXISTS `spFetchPendingPayAtCounterOrderList`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spFetchPendingPayAtCounterOrderList` (IN `business_id` VARCHAR(15))  BEGIN
SELECT a.OrderId, a.CustomerId, sum(a.OrderTotalPrice) AS OrderTotalPrice, b.OrderStatusDescription
    FROM orders a
    INNER JOIN order_status b
    ON a.OrderStatusID = b.OrderStatusID
    WHERE a.BusinessId = business_id AND a.DeliveryMethod = 0 AND a.OrderStatusId = 1 OR a.OrderStatusId = 2 OR a.OrderStatusId = 3
    GROUP BY a.OrderId;
END$$

DROP PROCEDURE IF EXISTS `spFetchSearchItems`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spFetchSearchItems` ()  BEGIN
SELECT a.ItemId,a.ItemName, b.BusinessName, c.MealTypeDescription
FROM item_details a
INNER JOIN business_details b on a.BusinessId=b.BusinessId
INNER JOIN meal_type c on a.MealTypeId=c.MealTypeId
ORDER BY a.ItemId DESC;
END$$

DROP PROCEDURE IF EXISTS `spGetCartSum`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spGetCartSum` (IN `user_id` VARCHAR(9))  BEGIN
select sum(TotalPrice) 
from cart 
where CustomerId = user_id;
END$$

DROP PROCEDURE IF EXISTS `spGetCustomerInfo`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spGetCustomerInfo` (IN `user_id` VARCHAR(9))  BEGIN
SELECT CustomerId, CustomerFirstName, CustomerLastName, CustomerEmail, CustomerPhoneNumber 
FROM customer_details where CustomerId = user_id;
END$$

DROP PROCEDURE IF EXISTS `spGetEmailInfo`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spGetEmailInfo` (IN `orderNum` VARCHAR(255))  NO SQL
BEGIN
select a.CustomerEmail, a.CustomerFirstName, b.OrderId
from customer_details a
inner JOIN orders b
on a.CustomerId = b.CustomerId
where b.OrderId = orderNum;
END$$

DROP PROCEDURE IF EXISTS `spGetMaxBusinessId`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spGetMaxBusinessId` ()  BEGIN
	SELECT MAX(Id) id from business_details;
END$$

DROP PROCEDURE IF EXISTS `spGetMaxMenuItemId`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spGetMaxMenuItemId` ()  BEGIN
	SELECT MAX(ItemId) id from item_details;
END$$

DROP PROCEDURE IF EXISTS `spGetMaxUserId`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spGetMaxUserId` ()  BEGIN
	SELECT MAX(UserId) id from users;
END$$

DROP PROCEDURE IF EXISTS `spGetOrderDetails`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spGetOrderDetails` (IN `user_id` VARCHAR(9))  BEGIN
    IF(select count(*) from orders where DeliveryMethod=1 and CustomerId = user_id) THEN 
        select a.OrderId, a.DeliveryMethod, b.BusinessName, b.BusinessAddress, c.BuildingDescription, a.RoomNo, d.TimeslotDescription, a.DeliveryDate, 
        e.ItemName, a.OrderItemQuantity, a.OrderTotalPrice,f.OrderStatusDescription, a.MainOrderId
        from orders a
        inner join business_details b
        on a.BusinessId = b.BusinessId
        inner join building_info c
        on a.BuildingId = c.BuildingId
        inner join timeslots d
        on a.DeliveryTimeslotId = d.TimeslotId
        inner join item_details e
        on a.itemId = e.itemId
        inner join order_status f
        on a.OrderStatusId = f.OrderStatusId
        where a.CustomerId = user_id and a.OrderStatusId <> 6 and a.DeliveryMethod=1
        order by a.DeliveryDate, a.OrderId DESC;
    END IF;
END$$

DROP PROCEDURE IF EXISTS `spGetOrderDetailsPayAtCounter`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spGetOrderDetailsPayAtCounter` (IN `user_id` VARCHAR(9))  BEGIN
    IF(select count(*) from orders where DeliveryMethod=0 and CustomerId = user_id) THEN 
        select a.OrderId, a.DeliveryMethod, b.BusinessName, b.BusinessAddress,
        e.ItemName, a.OrderItemQuantity, a.OrderTotalPrice,f.OrderStatusDescription, a.MainOrderId
        from orders a
        inner join business_details b
        on a.BusinessId = b.BusinessId
        inner join item_details e
        on a.itemId = e.itemId
        inner join order_status f
        on a.OrderStatusId = f.OrderStatusId
        where a.CustomerId = user_id and a.DeliveryMethod=0 and a.OrderStatusId <> 6
        order by a.OrderId DESC;
    END IF;
END$$

DROP PROCEDURE IF EXISTS `spGetOrderValuesPayroll`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spGetOrderValuesPayroll` (IN `user_id` VARCHAR(9), IN `deliveryType` INT)  BEGIN

DECLARE response int;

if(select count(*) from cart where CustomerId=user_id)>0 THEN
    if(deliveryType = 1) THEN
        if(select count(*) from tempdeliveryinfo where customerID = user_id)>0 then
            start transaction;
                select a.CustomerId ,c.BusinessId, 1 as 'DeliveryMethod' , e.BuildingId, e.roomNo, d.DeliveryTimeSlot, a.TotalPrice, 1 as 'OrderStatus', a.ItemQuantity,a.ItemId ,d.DeliveryDate
                from cart a
                inner join customer_details b
                on a.CustomerId = b.CustomerId
                inner join item_details c
                on a.ItemId=c.ItemId
                inner join tempdeliverytime d
                on a.CustomerId = d.CustomerId
                inner join tempdeliveryinfo e
                on a.customerId=e.customerID
                where a.CustomerId=user_id;

                set response=1;
            commit;
        else
            start transaction;
                select a.CustomerId ,c.BusinessId, 1 as 'DeliveryMethod' , b.BuildingId, b.roomNo, d.DeliveryTimeSlot, a.TotalPrice, 1 as 'OrderStatus', a.ItemQuantity,a.ItemId ,d.DeliveryDate
                from cart a
                inner join customer_details b
                on a.CustomerId = b.CustomerId
                inner join item_details c
                on a.ItemId=c.ItemId
                inner join tempdeliverytime d
                on a.CustomerId = d.CustomerId
                where a.CustomerId=user_id;

                set response= 2;
            commit;
        end if;
    end if;
end if;

END$$

DROP PROCEDURE IF EXISTS `spGetPayOnDeliveryItemList`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spGetPayOnDeliveryItemList` (IN `user_id` VARCHAR(9), IN `deliveryType` INT)  BEGIN

DECLARE response int;

if(select count(*) from cart where CustomerId=user_id)>0 THEN
    if(deliveryType = 0) THEN
        start transaction;
            select a.CustomerId ,c.BusinessId, 0 as 'DeliveryMethod' , a.TotalPrice, 1 as 'OrderStatus', a.ItemQuantity,a.ItemId
            from cart a
            inner join customer_details b
            on a.CustomerId = b.CustomerId
            inner join item_details c
            on a.ItemId=c.ItemId
            where a.CustomerId=user_id;

            set response= 1;
        commit;
    end if;
end if;

END$$

DROP PROCEDURE IF EXISTS `spLogin`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spLogin` (IN `userId` VARCHAR(9), IN `userPassword` VARCHAR(100))  BEGIN

set @response=99;
set @checkPaymentRegistration=99;
set @countOfCartItems=-99;

	IF (SELECT count(*) FROM customer_details WHERE CustomerId=userId) > 0 THEN -- if user exists
		IF (SELECT count(*) FROM customer_details WHERE CustomerId=userId AND CustomerStatus=0) > 0 THEN   -- if user account is inactive
				SET @response = 1;   -- user account is inactive
		ELSEIF (SELECT count(*) FROM customer_details WHERE CustomerId=userId AND CustomerPassword=userPassword) > 0 THEN   -- if user has entered correct password
			SET @response = 2;   -- user account password is correct
            SELECT count(*) into @countOfCartItems FROM cart WHERE CustomerId=userId;
			IF (SELECT count(*) FROM payment_info WHERE CustomerId=userId) > 0 THEN    -- if user has registered for payment
				SET @checkPaymentRegistration = 1;   -- user account is registered for payment
			ELSE
				SET @checkPaymentRegistration = 0;   -- user account is not registered for payment
			END IF;
		ELSE
			SET @response = 3;   -- user password is incorrect
		END IF;
        
	ELSE
		SET @response = 0;   -- user does not exist
	
    END IF;

    SELECT @response,@checkPaymentRegistration,@countOfCartItems;

	END$$

DROP PROCEDURE IF EXISTS `spLoginStaff`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spLoginStaff` (IN `userId` VARCHAR(6), IN `userPassword` VARCHAR(100))  BEGIN

    SET @response=99;
    SET @businessId = "";
    SET @fname = "";
    SET @lname = "";

    IF (SELECT count(*) FROM staff_details WHERE StaffId=userId) > 0 THEN -- if staff exists
        IF (SELECT count(*) FROM staff_details WHERE StaffId=userId AND StaffStatus=0) > 0 THEN   -- if staff account is inactive
                SET @response = 1;   -- user account is inactive
        ELSEIF (SELECT count(*) FROM staff_details WHERE StaffId=userId AND StaffPassword=userPassword) > 0 THEN   -- if staff has entered correct password
            SET @response = 2;   -- staff account password is correct
            SELECT BusinessId FROM staff_details WHERE StaffId=userId AND StaffPassword=userPassword INTO @businessId;
            SELECT StaffFirstName FROM staff_details WHERE StaffId=userId AND StaffPassword=userPassword INTO @fname;
            SELECT StaffLastName FROM staff_details WHERE StaffId=userId AND StaffPassword=userPassword INTO @lname;
        ELSE
            SET @response = 3;   -- staff password is incorrect
        END IF;
        
    ELSE
        SET @response = 0;   -- staff does not exist

    END IF;

    SELECT @response, @businessId, @fname, @lname;

END$$

DROP PROCEDURE IF EXISTS `spLoginUser`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spLoginUser` (IN `user_name` VARCHAR(50), IN `user_password` VARCHAR(100))  BEGIN

    SET @response=99;
    SET @businessId = "";
    SET @fname = "";
    SET @lname = "";
    SET @rodeId = 99;

    IF (SELECT count(*) FROM users WHERE UserName = user_name) > 0 THEN -- if user exists
        IF (SELECT count(*) FROM users WHERE UserName = user_name AND UserStatus=0) > 0 THEN   -- if user account is inactive
                SET @response = 1;   -- user account is inactive
        ELSEIF (SELECT count(*) FROM users WHERE UserName = user_name AND UserPassword = user_password) > 0 THEN   -- if user has entered correct password
            SET @response = 2;   -- user account password is correct
            SELECT RoleId FROM users WHERE UserName = user_name AND UserPassword = user_password INTO @roleId;
            SELECT BusinessId FROM users WHERE UserName = user_name AND UserPassword = user_password INTO @businessId;
            SELECT UserFirstName FROM users WHERE UserName = user_name AND UserPassword = user_password INTO @fname;
            SELECT UserLastName FROM users WHERE UserName = user_name AND UserPassword = user_password INTO @lname;

        ELSE
            SET @response = 3;   -- user password is incorrect
        END IF;
        
    ELSE
        SET @response = 0;   -- user does not exist

    END IF;

    SELECT @response, @roleId, @fname, @lname, @businessId;

END$$

DROP PROCEDURE IF EXISTS `spPlaceOrder`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spPlaceOrder` (IN `user_id` VARCHAR(9), IN `deliveryType` INT, IN `MealId` VARCHAR(255), IN `BId` VARCHAR(10), IN `DeliveryStyle` VARCHAR(10), IN `Building` INT, IN `Room` VARCHAR(10), IN `TimeSlot` INT, IN `OrderPrice` DECIMAL(10,2), IN `OrderStatus` INT, IN `OrderItemQty` INT, IN `Item_Id` INT, IN `Deldate` DATE)  BEGIN

DECLARE response int default 99;
DECLARE oldBusinessBal, newBusinessBal DECIMAL(10,2) default 0.0;

if(select count(*) from cart where CustomerId=user_id)>0 THEN
    if(deliveryType = 1) THEN
        if(select count(*) from tempdeliverytime where CustomerId=user_id)>0 THEN
            select BusinessBalance into oldBusinessBal from business_details where BusinessId=BId;
            set newBusinessBal = oldBusinessBal + OrderPrice;

            insert into orders(OrderId, CustomerId, BusinessId, DeliveryMethod, BuildingId, RoomNo, DeliveryTimeslotId, OrderTotalPrice, OrderStatusId, OrderItemQuantity, itemId, DeliveryDate) 
            values(MealId,user_id,BId,DeliveryStyle, Building, Room, TimeSlot, OrderPrice ,OrderStatus, OrderItemQty,Item_Id,Deldate);

            update business_details set BusinessBalance = newBusinessBal where BusinessId=BId;

            set response=1;
        else set response=4;
        end if;
    else
        insert into orders(OrderId, CustomerId,BusinessId, DeliveryMethod,OrderTotalPrice, OrderStatusId, OrderItemQuantity, itemId, DeliveryDate)
        values(MealId,user_id,BId,DeliveryStyle, OrderPrice, OrderStatus, OrderItemQty, Item_Id, Deldate);
        
        set response=2;
    end if;

    delete from cart where CustomerId=user_id and itemId=Item_Id;
else set response=3;
end if;

select response;

END$$

DROP PROCEDURE IF EXISTS `spProcessPayment`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spProcessPayment` (IN `user_id` VARCHAR(9), IN `payrollnumber` VARCHAR(20), IN `payrollPin` INT, IN `orderNumber` VARCHAR(255))  BEGIN
	DECLARE  newCustomerBal, oldCustomerBal, oldBusinessBal, newBusinessBal, total_price DECIMAL(10,2) default 0.0;
    DECLARE  i,countOfLoop,response int DEFAULT 0;
    DECLARE bid varchar(10);
    if(select count(*) from payroll_info where PayrollId = payrollnumber and Pin = payrollPin) > 0 THEN

        select count(*) into countOfLoop from orders where orderId=orderNumber and OrderStatusId=0;
        
            
        SET i = 0;
            
        loop_label:  LOOP
            IF  i = countOfLoop THEN 
                LEAVE  loop_label;
            END  IF;
                
            SET  i = i + 1;
            select BusinessId into bid from orders where orderId=orderNumber and OrderStatusId=0;
            select sum(OrderTotalPrice) into total_price from orders where orderId=orderNumber and OrderStatusId=0 and BusinessId = bid;
            select AvailableBalance into oldCustomerBal from payroll_info where PayrollId = payrollnumber;
            select BusinessBalance into oldBusinessBal from business_details where BusinessId=bid;

            set newCustomerBal = oldCustomerBal - total_price;
            set newBusinessBal = oldBusinessBal + total_price;
            
            update payroll_info set AvailableBalance = newCustomerBal where PayrollId = payrollnumber;
           update business_details set BusinessBalance = newBusinessBal where BusinessId=bid;
            update orders set OrderStatusId = 1 where orderId=orderNumber and OrderStatusId=0 and BusinessId = bid;
            
        END LOOP;
        set response =1;
    ELSE set response = 2;
    end if;

    select response;
END$$

DROP PROCEDURE IF EXISTS `spRegister`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spRegister` (IN `firstName` VARCHAR(50), IN `lastName` VARCHAR(50), IN `userId` VARCHAR(9), IN `email` VARCHAR(100), IN `gender` VARCHAR(1), IN `userPassword` VARCHAR(100), IN `userNumber` VARCHAR(7), IN `deliveryId` INT, IN `roomNum` VARCHAR(10))  BEGIN

set @response=99;
SET @checkUserDetails = 99;

    BEGIN
        BEGIN
			IF (SELECT count(*) FROM customer_details WHERE CustomerID=userId) > 0 THEN
				SET @checkUserDetails = 1;   -- user is already registered, suggest login
            
            ELSEIF (SELECT count(*) FROM customer_details WHERE CustomerEmail=email) > 0 THEN
                SET @checkUserDetails = 2;   -- email already exists
            
            ELSE
                SET @checkUserDetails = 0;   -- user is not registered
            END IF;
        END;

        BEGIN
            IF @checkUserDetails = 0 THEN
                START TRANSACTION;
                        INSERT INTO customer_details (CustomerId, CustomerFirstName, CustomerLastName, CustomerEmail, CustomerPhoneNumber ,CustomerGender, CustomerPassword,BuildingId,roomNo) 
                        VALUES (userId, firstName, lastName, email, userNumber,gender, userpassword,deliveryId,roomNum);

                        SET @response = 1;   -- user details have been entered in db      
                COMMIT;

            ELSEIF (@checkUserDetails = 1) THEN
                SET @response = 0;   -- user already exist

            ELSEIF (@checkUserDetails = 2) THEN
                SET @response = 2;   -- email already exist

            ELSE
                SET @response = 3;   -- something went wrong
            END IF;
        END;

        SELECT @response;
        
	END;
    
END$$

DROP PROCEDURE IF EXISTS `spSetupPayment`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spSetupPayment` (IN `user_id` VARCHAR(9), IN `payment_method` VARCHAR(20), IN `payment_id` VARCHAR(15), IN `delivery_location_id` INT)  BEGIN

set @response = 99;

    BEGIN
        IF (SELECT count(*) FROM payment_info WHERE CustomerId = user_id and PaymentId = payment_id) > 0 THEN  -- if user has already registered for payment
            SET @response = 0;   -- user is already registered for payment
        ELSE
            BEGIN
                IF (SELECT count(*) FROM payroll_info WHERE PayrollId = payment_id) > 0 THEN    -- if payroll account is already being used
                    SET @response = 1;   -- payroll account is already being used
                ELSE
                    START TRANSACTION;
                        INSERT INTO payment_info (PaymentMethod, PaymentId, CustomerId, DeliveryLocationId, Status) VALUES (payment_method, payment_id, user_id, delivery_location_id, 1);
                        
                        UPDATE payroll_info SET payroll_info.Status = 1;
                        
                        SET @response = 2;   -- user payment registration details have been entered in the DB and 'payroll_info' table has been updated (Status = 1)
                    COMMIT;
				END IF;
                
            END;
		END IF;
        SELECT @response;
    END;
END$$

DROP PROCEDURE IF EXISTS `spSetUpPayroll`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spSetUpPayroll` (IN `user_id` VARCHAR(9), IN `payroll` VARCHAR(20), IN `payrollPIN` INT)  BEGIN
    DECLARE response int default 99;
    if(select count(*) from payment_info where CustomerId=user_id) > 0 THEN
        set response = 0;
    elseif(select count(*) from payment_info where PaymentId = payroll) > 0 THEN
        set response = 1;
    else 
        if(select count(*) from payroll_info where PayrollId =payroll AND Pin = payrollPIN) > 0 THEN
            insert into payment_info (PaymentMethod, PaymentId, CustomerId, Status)
            values('Payroll',payroll,user_id,1);

            set response=2;
        else set response=3;
        end if;
    end if;

    select response;
END$$

DROP PROCEDURE IF EXISTS `spTempDeliveryAddress`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spTempDeliveryAddress` (IN `user_id` VARCHAR(9), IN `DeliveryId` INT, IN `room` VARCHAR(10))  BEGIN
	DECLARE response int default 99;
    
	if(select count(*) from TempDeliveryInfo where customerID=user_id)>0 THEN
    Update TempDeliveryInfo SET buildingID=DeliveryId, roomNo=room where customerID=user_id;
    set response=1;
    ELSE
    	INSERT INTO TempDeliveryInfo VALUES(user_id,DeliveryId,room);
        set response=2;
    END IF;

    select response;
END$$

DROP PROCEDURE IF EXISTS `spTempDeliveryTime`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spTempDeliveryTime` (IN `user_id` VARCHAR(9), IN `timeSlot` INT, IN `deliveryDate` DATE)  NO SQL
BEGIN
	Declare response int DEFAULT 99;
    
    delete from TempDeliveryTime where CustomerID=user_id;
    insert into TempDeliveryTime values(user_id, timeSlot, deliveryDate);
    set response=1;
    
    select response;
END$$

DROP PROCEDURE IF EXISTS `spUpdateBusinessDetails`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spUpdateBusinessDetails` (IN `business_id` VARCHAR(15), IN `business_name` VARCHAR(50), IN `business_email` VARCHAR(50), IN `business_phone` VARCHAR(10), IN `business_address` VARCHAR(200), IN `business_status` INT, IN `user_name` VARCHAR(50), IN `user_fname` VARCHAR(50), IN `user_lname` VARCHAR(50), IN `user_email` VARCHAR(50), IN `user_password` VARCHAR(200), IN `user_status` INT)  BEGIN

    UPDATE business_details SET BusinessName = business_name, BusinessEmail = business_email, BusinessPhone = business_phone, BusinessAddress = business_address, BusinessStatus = business_status WHERE BusinessId = business_id;

    UPDATE users SET UserFirstName = user_fname, UserLastName = user_lname, UserEmail = user_email, UserPassword = user_password, UserStatus = user_status WHERE UserName = user_name;

END$$

DROP PROCEDURE IF EXISTS `spUpdateBusinessProfile`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spUpdateBusinessProfile` (IN `business_id` VARCHAR(15), IN `business_name` VARCHAR(50), IN `business_image` VARCHAR(200), IN `business_email` VARCHAR(50), IN `business_phone` VARCHAR(10), IN `business_address` VARCHAR(200), IN `user_name` VARCHAR(50), IN `user_fname` VARCHAR(50), IN `user_lname` VARCHAR(50), IN `user_email` VARCHAR(50), IN `user_password` VARCHAR(200))  BEGIN

    UPDATE business_details SET BusinessName = business_name, BusinessImage = business_image, BusinessEmail = business_email, BusinessPhone = business_phone, BusinessAddress = business_address WHERE BusinessId = business_id;

    UPDATE users SET UserFirstName = user_fname, UserLastName = user_lname, UserEmail = user_email, UserPassword = user_password WHERE UserName = user_name;

END$$

DROP PROCEDURE IF EXISTS `spUpdateCafeteriaStaff`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spUpdateCafeteriaStaff` (IN `user_name` VARCHAR(50), IN `user_fname` VARCHAR(50), IN `user_lname` VARCHAR(50), IN `user_email` VARCHAR(50), IN `user_phone` VARCHAR(10), IN `user_address` VARCHAR(200), IN `user_password` VARCHAR(200), IN `user_status` INT)  BEGIN
    UPDATE users SET UserFirstName = user_fname, UserLastName = user_lname, UserEmail = user_email, UserPhone = user_phone, UserAddress = user_address, UserPassword = user_password, UserStatus = user_status WHERE UserName = user_name;
END$$

DROP PROCEDURE IF EXISTS `spUpdateCartItemQuantity`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spUpdateCartItemQuantity` (IN `user_id` VARCHAR(9), `item_id` INT, IN `quantity` INT)  BEGIN
	DECLARE item_price, total_price DECIMAL(10,2) default 0.0;
    SELECT ItemPrice INTO item_price FROM item_details WHERE ItemId = item_id;
    Set total_price = item_price * quantity;
    
    UPDATE cart SET ItemQuantity = quantity, TotalPrice = total_price WHERE CustomerId = user_id AND ItemId = item_id;
END$$

DROP PROCEDURE IF EXISTS `spUpdateDeliveryOrderStatus`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spUpdateDeliveryOrderStatus` (IN `order_id` INT, IN `order_status` INT)  BEGIN
	UPDATE orders SET OrderStatusId = order_status
    WHERE OrderId = order_id;
END$$

DROP PROCEDURE IF EXISTS `spUpdateDeliveryStaff`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spUpdateDeliveryStaff` (IN `user_name` VARCHAR(50), IN `user_fname` VARCHAR(50), IN `user_lname` VARCHAR(50), IN `user_email` VARCHAR(50), IN `user_phone` VARCHAR(10), IN `user_address` VARCHAR(200), IN `user_password` VARCHAR(200), IN `user_status` INT)  BEGIN
    UPDATE users SET UserFirstName = user_fname, UserLastName = user_lname, UserEmail = user_email, UserPhone = user_phone, UserAddress = user_address, UserPassword = user_password, UserStatus = user_status WHERE UserName = user_name;
END$$

DROP PROCEDURE IF EXISTS `spUpdateMealCategory`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spUpdateMealCategory` (IN `mealCategory_id` INT, IN `mealCategory_desc` VARCHAR(50))  BEGIN
	UPDATE item_category SET ItemCategoryDescription = mealCategory_desc WHERE ItemCategoryId = mealCategory_id;
END$$

DROP PROCEDURE IF EXISTS `spUpdateMealType`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spUpdateMealType` (IN `mealType_id` INT, IN `mealType_desc` VARCHAR(50))  BEGIN
	UPDATE meal_type SET MealTypeDescription = mealType_desc WHERE MealTypeId = mealType_id;
END$$

DROP PROCEDURE IF EXISTS `spUpdateMenuItem`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spUpdateMenuItem` (IN `item_id` INT, IN `item_name` VARCHAR(50), IN `item_desc` VARCHAR(1000), IN `item_image` VARCHAR(20), IN `item_price` DECIMAL(10,2), IN `item_quantity` INT, IN `item_status` INT, IN `meal_category_id` INT, IN `meal_type_id` INT)  BEGIN
    UPDATE `item_details` SET`ItemName`= item_name,`ItemDescription`= item_desc, `ItemImageThumbnail` = item_image, `ItemPrice`= item_price,`ItemQuantityOnHand`= item_quantity,`ItemStatus`= item_status,`ItemCategoryId`= meal_category_id,`MealTypeId`=meal_type_id WHERE `ItemId`= item_id;
END$$

DROP PROCEDURE IF EXISTS `spUpdateOrderStatus`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spUpdateOrderStatus` (IN `order_id` INT, IN `order_status` INT, IN `business_id` VARCHAR(15))  BEGIN
	UPDATE orders SET OrderStatusId = order_status
    WHERE OrderId = order_id AND BusinessId = business_id;
END$$

DROP PROCEDURE IF EXISTS `spViewCart`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spViewCart` (IN `user_id` VARCHAR(9))  BEGIN
	select b.ItemImageThumbnail,b.ItemName,b.ItemPrice,a.ItemQuantity,c.BusinessName,a.TotalPrice,b.ItemId,b.BusinessId,b.ItemStatus
from cart a
INNER JOIN item_details b
on a.ItemId=b.ItemId
INNER JOIN business_details c
on b.BusinessId=c.BusinessId
WHERE a.CustomerId = user_id;
END$$

DROP PROCEDURE IF EXISTS `spViewCurrentAddress`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spViewCurrentAddress` (IN `user_id` VARCHAR(9))  BEGIN
select b.BuildingId, b.BuildingDescription ,a.roomNo
from customer_details a
INNER JOIN building_info b
on a.BuildingId=b.BuildingId
where a.CustomerId=user_id;
END$$

DROP PROCEDURE IF EXISTS `spViewSubTotal`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `spViewSubTotal` (IN `user_id` VARCHAR(9))  BEGIN
Declare CartTotal,Tax,SubTotal DECIMAL(10,2) default 0.0;

select sum(TotalPrice) into CartTotal from cart where CustomerId = user_id;
set SubTotal = CartTotal+Tax;

select CartTotal,Tax,SubTotal;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `building_info`
--

DROP TABLE IF EXISTS `building_info`;
CREATE TABLE `building_info` (
  `BuildingId` int(11) NOT NULL,
  `BuildingDescription` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `building_info`
--

INSERT INTO `building_info` (`BuildingId`, `BuildingDescription`) VALUES
(1, 'Faculty of Business and Economics Building'),
(2, 'Faculty of Science, Technology & Environment Building'),
(3, 'Faculty of Arts, Law & Education Building'),
(4, 'Marine Campus Security Booth'),
(5, 'Statham Campus Reception Office'),
(6, 'Student Administrative Services Reception Office'),
(7, 'Engineering Block'),
(8, 'Land Management Building'),
(9, 'ICT Building A'),
(10, 'ICT Building B'),
(11, 'Library Building Front Desk');

-- --------------------------------------------------------

--
-- Table structure for table `business_details`
--

DROP TABLE IF EXISTS `business_details`;
CREATE TABLE `business_details` (
  `BusinessId` varchar(15) NOT NULL,
  `BusinessName` varchar(50) NOT NULL,
  `BusinessEmail` varchar(50) NOT NULL,
  `BusinessPhone` varchar(10) NOT NULL,
  `BusinessAddress` varchar(200) NOT NULL,
  `BusinessStatus` int(11) NOT NULL DEFAULT 1,
  `BusinessBalance` decimal(10,2) NOT NULL DEFAULT 0.00,
  `BusinessImage` varchar(200) DEFAULT NULL,
  `Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `business_details`
--

INSERT INTO `business_details` (`BusinessId`, `BusinessName`, `BusinessEmail`, `BusinessPhone`, `BusinessAddress`, `BusinessStatus`, `BusinessBalance`, `BusinessImage`, `Id`) VALUES
('B010', 'USP Cafeteria', 'uspcafe0120@gmail.com', '3385421', 'USP Cafeteria', 1, '154.00', 'B010.jpg', 10001),
('B020', 'Navintees Spicy Kuisine', 'navinteekuisine20@gmail.com', '3395462', 'USP Cafeteria', 1, '8.00', '', 10002),
('B030', 'Usp Southern Cross', 'uspsoutherncross13@yahoo.com', '3392555', 'USP Southern Cross', 1, '0.00', NULL, 10003),
('B10004', 'test business', 'test@gmail.com', '1231231', 'USP', 1, '0.00', NULL, 10012);

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
CREATE TABLE `cart` (
  `CustomerId` varchar(9) NOT NULL,
  `ItemId` int(11) NOT NULL,
  `ItemQuantity` int(11) NOT NULL,
  `TotalPrice` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`CustomerId`, `ItemId`, `ItemQuantity`, `TotalPrice`) VALUES
('s123456', 7, 1, '8.50'),
('s11123207', 9, 2, '17.00');

-- --------------------------------------------------------

--
-- Table structure for table `credit_card_info`
--

DROP TABLE IF EXISTS `credit_card_info`;
CREATE TABLE `credit_card_info` (
  `CreditCardNumber` int(11) NOT NULL,
  `CreditCardType` varchar(50) NOT NULL,
  `ExpirationMonth` int(11) NOT NULL,
  `ExpirationYear` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer_details`
--

DROP TABLE IF EXISTS `customer_details`;
CREATE TABLE `customer_details` (
  `CustomerId` varchar(9) NOT NULL,
  `CustomerFirstName` varchar(30) NOT NULL,
  `CustomerLastName` varchar(30) NOT NULL,
  `CustomerEmail` varchar(50) NOT NULL,
  `CustomerPhoneNumber` varchar(20) NOT NULL,
  `CustomerGender` varchar(1) NOT NULL,
  `CustomerPassword` varchar(100) NOT NULL,
  `CustomerStatus` int(11) NOT NULL DEFAULT 1,
  `CustomerCreateTime` timestamp NOT NULL DEFAULT current_timestamp(),
  `BuildingId` int(11) DEFAULT NULL,
  `roomNo` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer_details`
--

INSERT INTO `customer_details` (`CustomerId`, `CustomerFirstName`, `CustomerLastName`, `CustomerEmail`, `CustomerPhoneNumber`, `CustomerGender`, `CustomerPassword`, `CustomerStatus`, `CustomerCreateTime`, `BuildingId`, `roomNo`) VALUES
('s11123207', 'Anish', 'Sundarjee', 'anishsundarjee25@gmail.com', '9998105', 'M', 'Test', 1, '2020-03-05 08:02:18', 1, '1'),
('s123456', 'Test', 'test', 'test@test.com', '2038148', 'M', 'test', 1, '2020-03-08 12:29:57', 1, '5');

-- --------------------------------------------------------

--
-- Table structure for table `delivery_timeslot`
--

DROP TABLE IF EXISTS `delivery_timeslot`;
CREATE TABLE `delivery_timeslot` (
  `DeliveryTimeslotId` int(11) NOT NULL,
  `DeliveryDate` date NOT NULL,
  `TimeslotId` int(11) NOT NULL,
  `TimeslotsAvailable` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `item_category`
--

DROP TABLE IF EXISTS `item_category`;
CREATE TABLE `item_category` (
  `ItemCategoryId` int(11) NOT NULL,
  `ItemCategoryDescription` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `item_category`
--

INSERT INTO `item_category` (`ItemCategoryId`, `ItemCategoryDescription`) VALUES
(1, 'Breakfast'),
(2, 'Lunch'),
(3, 'Dinner');

-- --------------------------------------------------------

--
-- Table structure for table `item_details`
--

DROP TABLE IF EXISTS `item_details`;
CREATE TABLE `item_details` (
  `ItemId` int(11) NOT NULL,
  `ItemName` varchar(50) NOT NULL,
  `ItemDescription` varchar(1000) NOT NULL,
  `ItemImageThumbnail` varchar(200) DEFAULT NULL,
  `ItemPrice` decimal(10,2) NOT NULL,
  `ItemDeliveryPrice` decimal(10,2) DEFAULT NULL,
  `ItemQuantityOnHand` int(11) NOT NULL,
  `ItemRating` int(11) DEFAULT NULL,
  `ItemStatus` int(11) NOT NULL DEFAULT 1,
  `BusinessId` varchar(15) NOT NULL,
  `ItemCategoryId` int(11) DEFAULT NULL,
  `MealTypeId` int(11) DEFAULT NULL,
  `ItemTypeId` int(11) DEFAULT NULL,
  `ItemIngredients` varchar(1000) DEFAULT NULL,
  `ItemRecipe` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `item_details`
--

INSERT INTO `item_details` (`ItemId`, `ItemName`, `ItemDescription`, `ItemImageThumbnail`, `ItemPrice`, `ItemDeliveryPrice`, `ItemQuantityOnHand`, `ItemRating`, `ItemStatus`, `BusinessId`, `ItemCategoryId`, `MealTypeId`, `ItemTypeId`, `ItemIngredients`, `ItemRecipe`) VALUES
(1, 'French Fries', 'Delicious, Scrumptious fries.', '1.jpg', '5.00', '5.00', 0, 0, 1, 'B010', 2, 4, 2, NULL, NULL),
(2, 'Chicken Stirfry', 'Quick to make stir fried chicken with freshly cut vegetables that will be your easy go to dinner or lunch.', '2.jpg', '8.00', '5.00', 0, 0, 1, 'B010', 2, 1, 2, NULL, NULL),
(3, 'Soy Sauce Chicken &amp; Chips', 'Delicious tender chicken slices marinated in mesmerizing soy sauce.', '3.jpg', '8.00', '5.00', 0, 0, 1, 'B010', 2, 1, 2, NULL, NULL),
(4, 'Deep Fried Chicken &amp; Chips', 'Our classic deep fried scrumptious chicken and chips will leave you asking more.', '4.jpg', '8.00', '5.00', 0, 0, 1, 'B010', 2, 1, 2, NULL, NULL),
(5, 'Sausage &amp; Chips', 'Have a taste of our delicious dish at an affordable price.', '5.jpg', '6.00', '5.00', 0, 0, 1, 'B010', 2, 6, 2, NULL, NULL),
(6, 'Fish &amp; Chips', 'Consists of deep-fried beer-battered fish fillets served with French-fries.', '6.jpg', '5.00', '5.00', 0, 0, 1, 'B010', 2, 5, 2, NULL, NULL),
(7, 'Pork Stirfry', 'Richly marinated Pork chunks stir fried with crunchy mouth-watering vegetables, suitable for your lunch or dinner needs.', '7.jpg', '8.50', '5.00', 0, 0, 1, 'B010', 2, 2, 2, NULL, NULL),
(8, 'Red Pork', 'Tender pork cooked in sauce of spicy powders with an appealing red tinge added to the dish to mesmerize it.', '8.jpg', '8.50', '5.00', 0, 0, 1, 'B010', 2, 2, 2, NULL, NULL),
(9, 'Chilli Chicken(Bone)', 'Sweet, spicy and slightly sour tender chicken dish that will leave you asking for more.', '9.jpg', '8.50', '5.00', 2, 0, 1, 'B010', 2, 1, 2, NULL, NULL),
(10, 'Chicken in Blackbean(Bone)', 'Tastiest stir fried dish cooked with sesame oil, fresh garlic and fermented beans to add its mouth-watering flavor.', '10.jpg', '8.00', '5.00', 7, 0, 1, 'B010', 2, 1, 2, NULL, NULL),
(11, 'Chilli Lamb(Bone)', 'Succulent pieces of marinated lamb mixed with various spices and its main component chilli to give it a sophisticated taste.', '11.jpg', '8.00', '5.00', 9, 0, 1, 'B010', 2, 6, 2, NULL, NULL),
(12, 'Curry Chicken', 'Rich and aromatic curry dish consisting of tender chicken cooked in various tasty curry paste. With a dash of vegetables to balance out your meal.', '12.jpg', '8.00', '5.00', 9, 0, 1, 'B010', 2, 1, 2, NULL, NULL),
(13, 'Chicken Cashew nuts', 'One of our many varieties of stir fried chicken dishes with vegetables and a unique ingredient of cashew nuts that sets it apart from our other stir fry chicken dishes.', '13.jpg', '8.00', '5.00', 9, 0, 1, 'B010', 2, 1, 2, NULL, NULL),
(14, 'Chicken Fried Rice', 'Takes less than 15 minutes to cook. Our tender chicken mixed with vegetables and our favorite Jasmine rice is a good choice for your lunch or dinner needs.', '14.jpg', '9.00', '5.00', 9, 0, 1, 'B010', 2, 1, 2, NULL, NULL),
(15, 'Beef Long Soup', 'Slow cooked beef soup is tender and delicious, composed of beef chunk, comforting noodles with beans giving it a savory flavor.', '15.jpg', '9.00', '5.00', 10, 0, 1, 'B010', 2, 3, 2, NULL, NULL),
(16, 'Chicken Long Soup', 'A simple and healthy meal that is perfect for anyone. This chicken soup is loved for its broth with flavorful and colorful vegetables.', '16.jpg', '9.00', '5.00', 10, 0, 1, 'B010', 2, 1, 2, NULL, NULL),
(17, 'Chicken Spicy &amp; Sour Soup', 'Spicy and sour soup is perfect to beat the chills with its classic flavors. Composed of chicken pieces, simmered vegetables which gives a dainty flavor.', '17.jpg', '5.00', '5.00', 10, 0, 1, 'B010', 2, 1, 2, NULL, NULL),
(18, 'Chicken Sweet Corn Soup', 'A hearty, warming soup. Chicken sweet corn soup tastes absolutely delicious and is preferable for a super midday meal.', '18.jpg', '5.00', '5.00', 10, 0, 1, 'B010', 2, 1, 2, NULL, NULL),
(19, 'Combination Long Soup', 'Warm, nourishing soup which is perfect for everyone. This hearty combination of prawns, mushrooms, bean sprouts, noodles and sliced capsicums is just right for a midday meal or to beat the chills of a cold day at the office.', '19.jpg', '5.00', '5.00', 7, 0, 1, 'B010', 2, 7, 2, NULL, NULL),
(20, 'Chilli Chicken', 'Best dish made with chicken, peppers, garlic, chilli sauce &amp; soya sauce with a sprinkle of your favourite spring onions.', '20.jpg', '8.00', '5.00', 0, 0, 1, 'B020', 2, 1, 1, NULL, NULL),
(21, 'Chicken Chopsuey', 'Easy stir fry of healtht veggies in a mild sauce for a midday meal.', '21.jpg', '8.00', '5.00', 3, 0, 1, 'B020', 2, 1, 1, NULL, NULL),
(22, 'Chicken Fried Rice', 'Easy flavorful fried rice made with brown rice and lean chicken breast with mixed vegetables.', '22.jpg', '7.00', '5.00', 3, 0, 1, 'B020', 2, 1, 1, NULL, NULL),
(23, 'Egg Fried Rice', 'A delightful, scrumptious dish made ith vegeatables and fried eggs and a nice mix with rice.', '23.jpg', '5.50', '5.00', 4, 0, 1, 'B020', 2, 7, 1, NULL, NULL),
(24, 'Chicken Chowmein', 'Tasty dish made with tender noodles, fresh sauteed veggies, lean chicken and savory sauce.', '24.png', '8.00', '5.00', 4, 0, 1, 'B020', 2, 1, 1, NULL, NULL),
(25, 'Vegetable Chopsuey', 'Chop suey made with stir-fried colorful vegetables with thick sauce for a dainty flavour.', '25.jpg', '6.00', '5.00', 4, 0, 1, 'B020', 2, 4, 1, NULL, NULL),
(26, 'Vegetable Fried Rice', 'The fired rice that is loaded with vegetables only which takes less than 15 minutes to make.', '26.jpg', '5.00', '5.00', 4, 0, 1, 'B020', 2, 4, 1, NULL, NULL),
(27, 'Chicken Stir Fry', 'Stir fry packed with vegetables and chicken breasts  with tasty sauce.', '27.jpg', '8.00', '5.00', 4, 0, 1, 'B020', 2, 1, 1, NULL, NULL),
(28, 'Vegetable Stir Fry', 'Stir fry composed of colorful vegetables sauteed in a sweet and savory sauce.', '28.jpg', '6.00', '5.00', 4, 0, 1, 'B020', 2, 4, 1, NULL, NULL),
(29, 'Fruit Box', 'A box packed with healthy and nourishing fruits you can choose from to make you smile for the day.', '29.jpg', '5.00', '5.00', 4, 0, 1, 'B020', 2, 4, 1, NULL, NULL),
(30, 'Vegetable Curry &amp; Dhal with rice/roti', 'Veggie curry that is vegan with dhal and served with tasty rice or with our tender roti.', '30.jpg', '8.00', '5.00', 4, 0, 1, 'B020', 2, 4, 1, NULL, NULL),
(31, 'Chicken Curry &amp; Dhal with rice/roti', 'Chicken curry with dhal and served with jasmine rice or roti.', '31.jpg', '7.00', '5.00', 4, 0, 1, 'B020', 2, 1, 1, NULL, NULL),
(32, 'Spicy Chicken &amp; Dhal with rice/roti', 'Dish composed with spicy chicken breast, mild sauce and tasty dhal served with either rice or roti.', '32.jpg', '8.00', '5.00', 4, 0, 1, 'B020', 2, 1, 1, NULL, NULL),
(33, 'Lamb Curry &amp; Dhal with rice/roti', 'Lamb curry with dhal and served with rice or roti of your choice.', '33.jpg', '12.00', '5.00', 4, 0, 1, 'B020', 2, 6, 1, NULL, NULL),
(34, 'Chicken Liver &amp; Dhal with rice/roti', 'Chicken liver cooked with soothing dhal and served wih rice or roti.', '34.jpg', '7.00', '5.00', 4, 0, 1, 'B020', 2, 1, 1, NULL, NULL),
(35, 'Chicken Palau with Salad', 'A delicious dish made with chicken and rice mixed together by adding aromatic spices into it.', '35.jpg', '6.00', '5.00', 4, 0, 1, 'B020', 2, 1, 1, NULL, NULL),
(36, 'Egg Curry &amp; Dhal with rice/roti', 'Egg cooked with curry spices, dhal and can be served with rice or roti.', '36.jpg', '5.00', '5.00', 4, 0, 1, 'B020', 2, 7, 1, NULL, NULL),
(37, '3 pc Masala Fish &amp; Chips', 'Battered fish and chips that is very scrumptious and tasty with your favourite russet chips.', '37.jpg', '5.00', '5.00', 4, 0, 1, 'B020', 2, 5, 1, NULL, NULL),
(38, 'Goat Curry', 'Super tender and mosit curry goat that is perfect for a midday meal.', '38.jpg', '12.00', '5.00', 4, 0, 1, 'B020', 2, 7, 1, NULL, NULL),
(39, 'Chicken Briyani', 'Delightful dish made with chicken layered with long grain basmati rice which is slow cooked to perfection.', '39.jpg', '8.00', '5.00', 4, 0, 1, 'B020', 2, 1, 1, NULL, NULL),
(40, 'Fish Curry', 'Fish curry composed of curry powder and creamy coconut milk  and delicious when eaten with rice.', '40.jpg', '8.00', '5.00', 4, 0, 1, 'B020', 2, 5, 1, NULL, NULL),
(41, 'French Fries', 'Readily salted, delicious Fries', '41.jpg', '4.50', '5.00', 1, 0, 1, 'B030', 2, 4, 1, NULL, NULL),
(42, 'Beef Long Soup', 'Perfect cozy soup to mesmerize your day.', '42.jpg', '6.50', '5.00', 4, 0, 1, 'B030', 2, 3, 1, NULL, NULL),
(43, 'Combination Long Soup', 'A hearty combo of several authentic Asian ingredients to stir up the flavors.', '43.jpg', '7.50', '5.00', 4, 0, 1, 'B030', 2, 7, 1, NULL, NULL),
(44, 'Vegetable Long Soup', 'Hot and tasty, with a variety of vegetables to keep you wanting more.  A healthy choice.', '44.jpg', '6.00', '5.00', 4, 0, 1, 'B030', 2, 4, 1, NULL, NULL),
(45, 'Chicken Long Soup', 'Chicken, with its tasty broth with extra long and yummy noodles which will leave you wanting more.', '45.jpg', '6.50', '5.00', 4, 0, 1, 'B030', 2, 1, 1, NULL, NULL),
(46, 'Soy Sauce Chicken &amp; Chips', 'Deliciously cooked chicken marinated in soy sauce with mesmerizing flavors', '46.jpg', '6.50', '5.00', 4, 0, 1, 'B030', 2, 1, 1, NULL, NULL),
(47, 'Deep Fried Chicken &amp; Chips', 'Taste our scrumptious extra coated chicken served with our yummy salted fries.', '47.jpg', '6.50', '5.00', 4, 0, 1, 'B030', 2, 1, 1, NULL, NULL),
(48, 'Sausage &amp; Chips', 'Our standard sausage and chips dish will leave you wanting more for lunch &amp; dinner.', '48.jpg', '5.50', '5.00', 1, 0, 1, 'B030', 2, 7, 1, NULL, NULL),
(49, 'Fish &amp; Chips', 'Fish lovers can never go disappointed as our battered fish will satisfy you and your friends', '49.jpg', '5.50', '5.00', 4, 0, 1, 'B030', 2, 5, 1, NULL, NULL),
(50, 'Chilli Chicken(Bone)', 'A sweet, spicy and a bit sour chicken dish for those who love a mindblowing mix of all flavors with one bite.', '50.jpg', '7.00', '5.00', 4, 0, 1, 'B030', 2, 1, 1, NULL, NULL),
(51, 'Chicken in Blackbean (Bone)', 'A non-brainer, every day stir fry with its special ingredient of fermented black beans which gives its tasty sauce.', '51.jpg', '7.00', '5.00', 4, 0, 1, 'B030', 2, 1, 1, NULL, NULL),
(52, 'Chilli Lamb (Bone)', 'Chilli Lamb cutlets have a real kick and are so easy to prepare. Its chillies and spices give its sophisticated flavor.', '52.jpg', '7.80', '5.00', 4, 0, 1, 'B030', 2, 6, 1, NULL, NULL),
(53, 'Curry Chicken', 'Chicken pieces cooked in an aromatic, deliciously seasoned curry sauce.', '53.jpg', '7.00', '5.00', 4, 0, 1, 'B030', 2, 1, 1, NULL, NULL),
(54, 'Chicken Fried Rice', 'Easy one skillet recipe is ready in 20 minutes.Tenderly seasoned and a healthy well balanced choice.', '54.jpg', '6.80', '5.00', 4, 0, 1, 'B030', 2, 1, 1, NULL, NULL),
(55, 'Stir Fry Beef', 'Flavorful and easy, mixed with a number of vegetables with chunks of beef, this dish will satisfy your hunger instantly.', '55.jpg', '7.00', '5.00', 4, 0, 1, 'B030', 2, 3, 1, NULL, NULL),
(56, 'Beef Stew', 'A classic beef dish, with a thick delicious mouth watering stew, with a mix of veges. Good for those lookinf for a heavy lunch or dinner.', '56.jpg', '7.80', '5.00', 4, 0, 1, 'B030', 2, 3, 1, NULL, NULL),
(57, 'Stir Fry Fish Fillets', 'Stir fried with goodness, the sliced fish fillets gives a well mix to the vegetables and makes for a healthy lunch or dinner dish.', '57.jpg', '7.00', '5.00', 4, 0, 1, 'B030', 2, 5, 1, NULL, NULL),
(58, 'Stir Fry Vegetables', 'Want a healthy meal, try our stir fried vegetables, with its yumminess and crunchiness.', '58.jpg', '7.50', '5.00', 4, 0, 1, 'B030', 2, 4, 1, NULL, NULL),
(59, 'Vegetable Fried Rice', 'Fried rice with a mix of healthy vegetables for our vegetable lovers and vegetarians.', '59.jpg', '7.50', '5.00', 4, 0, 1, 'B030', 2, 4, 1, NULL, NULL),
(60, 'Vegetable Corn Soup', 'An Indo chinese style soup made with mixed vegetables, sweet corn kernels amongst other vegetables. Good for cold weather or if you are under the weather yourself', '60.jpg', '5.50', '5.00', 4, 0, 1, 'B030', 2, 4, 1, NULL, NULL),
(61, 'Vegetable Corn Soup w/ Egg', 'Our regular corn soup dish with an added egg ingredient. Good for cold weather or if you are under the weather yourself', '61.jpg', '5.50', '5.00', 4, 0, 1, 'B030', 2, 7, 1, NULL, NULL),
(62, 'Chicken Spicy &amp; Sour soup', 'Chicken soup with a spicy  and sour twist to it. For those who love sour &amp; spicy dishes.', '62.jpg', '5.50', '5.00', 4, 0, 1, 'B030', 2, 1, 1, NULL, NULL),
(63, 'Chicken Corn Soup', 'A healthy, tasty corn soup with sliced chunks of chicken to balance it out. With the savoured flavors, you wont be disappointed.', '63.jpg', '5.50', '5.00', 4, 0, 1, 'B030', 2, 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `item_type`
--

DROP TABLE IF EXISTS `item_type`;
CREATE TABLE `item_type` (
  `ItemTypeId` int(11) NOT NULL,
  `ItemTypeDescription` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `item_type`
--

INSERT INTO `item_type` (`ItemTypeId`, `ItemTypeDescription`) VALUES
(1, 'Item Type 1'),
(2, 'Item Type 2');

-- --------------------------------------------------------

--
-- Table structure for table `meal_type`
--

DROP TABLE IF EXISTS `meal_type`;
CREATE TABLE `meal_type` (
  `MealTypeId` int(11) NOT NULL,
  `MealTypeDescription` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `meal_type`
--

INSERT INTO `meal_type` (`MealTypeId`, `MealTypeDescription`) VALUES
(1, 'Chicken'),
(2, 'Pork'),
(3, 'Beef'),
(4, 'Vegeterian'),
(5, 'Fish'),
(6, 'Lamb'),
(7, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `MainOrderId` int(11) NOT NULL,
  `OrderId` int(11) NOT NULL,
  `CustomerId` varchar(9) NOT NULL,
  `BusinessId` varchar(15) NOT NULL,
  `DeliveryMethod` varchar(20) NOT NULL,
  `BuildingId` int(11) DEFAULT NULL,
  `RoomNo` varchar(10) DEFAULT NULL,
  `DeliveryTimeslotId` int(11) DEFAULT NULL,
  `OrderTotalPrice` decimal(10,2) NOT NULL,
  `OrderStatusId` int(11) NOT NULL,
  `OrderItemQuantity` int(11) NOT NULL,
  `itemId` int(11) NOT NULL,
  `DeliveryDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`MainOrderId`, `OrderId`, `CustomerId`, `BusinessId`, `DeliveryMethod`, `BuildingId`, `RoomNo`, `DeliveryTimeslotId`, `OrderTotalPrice`, `OrderStatusId`, `OrderItemQuantity`, `itemId`, `DeliveryDate`) VALUES
(1, 1, 's11123207', 'B010', '0', NULL, NULL, NULL, '5.00', 4, 1, 1, '1990-01-01'),
(3, 2, 's11123207', 'B010', '0', NULL, NULL, NULL, '9.00', 2, 1, 9, '1990-01-01'),
(7, 3, 's11123207', 'B010', '1', 10, 'ICT Q', 7, '9.00', 4, 1, 9, '2020-03-23'),
(8, 4, 's11123207', 'B010', '1', 10, 'ICT Q', 9, '9.00', 1, 1, 9, '2020-03-24'),
(9, 5, 's11123207', 'B010', '0', NULL, NULL, NULL, '9.00', 2, 1, 9, '1990-01-01'),
(10, 6, 's11123207', 'B010', '0', NULL, NULL, NULL, '9.00', 3, 1, 9, '1990-01-01'),
(11, 7, 's11123207', 'B010', '0', NULL, NULL, NULL, '9.00', 1, 1, 9, '1990-01-01'),
(12, 7, 's11123207', 'B010', '0', NULL, NULL, NULL, '24.00', 1, 3, 10, '1990-01-01');

-- --------------------------------------------------------

--
-- Table structure for table `order_item`
--

DROP TABLE IF EXISTS `order_item`;
CREATE TABLE `order_item` (
  `OrderId` int(11) NOT NULL,
  `ItemId` int(11) NOT NULL,
  `ItemQuantity` int(11) NOT NULL,
  `OrderTotalPrice` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_status`
--

DROP TABLE IF EXISTS `order_status`;
CREATE TABLE `order_status` (
  `OrderStatusId` int(11) NOT NULL,
  `OrderStatusDescription` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_status`
--

INSERT INTO `order_status` (`OrderStatusId`, `OrderStatusDescription`) VALUES
(0, 'Pending'),
(1, 'Incomplete'),
(2, 'Accepted'),
(3, 'Prepared'),
(4, 'Pending Delivery'),
(5, 'Delivered'),
(6, 'Cancelled'),
(7, 'Completed');

-- --------------------------------------------------------

--
-- Table structure for table `payment_info`
--

DROP TABLE IF EXISTS `payment_info`;
CREATE TABLE `payment_info` (
  `PaymentMethod` varchar(10) NOT NULL,
  `PaymentId` varchar(15) NOT NULL,
  `CustomerId` varchar(9) NOT NULL,
  `Status` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payment_info`
--

INSERT INTO `payment_info` (`PaymentMethod`, `PaymentId`, `CustomerId`, `Status`) VALUES
('Payroll', 'h900123070', 's11123207', 1);

-- --------------------------------------------------------

--
-- Table structure for table `payroll_info`
--

DROP TABLE IF EXISTS `payroll_info`;
CREATE TABLE `payroll_info` (
  `PayrollId` varchar(15) NOT NULL,
  `AvailableBalance` decimal(10,2) NOT NULL,
  `Status` int(11) NOT NULL DEFAULT 0,
  `Pin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payroll_info`
--

INSERT INTO `payroll_info` (`PayrollId`, `AvailableBalance`, `Status`, `Pin`) VALUES
('h900123070', '1432.00', 0, 1234),
('h900135562', '1151.00', 0, 1234),
('h900167003', '1151.00', 0, 1234),
('h900192689', '1788.00', 0, 1234),
('h900194566', '1678.00', 0, 1234),
('h900215129', '1412.00', 0, 1234),
('h900228996', '1524.00', 0, 1234),
('h900242353', '1174.00', 0, 1234),
('h900374663', '1587.00', 0, 1234),
('h900439841', '1310.00', 0, 1234),
('h900457671', '1133.00', 0, 1234),
('h900527689', '1059.00', 0, 1234),
('h900568659', '1938.00', 0, 1234),
('h900639370', '1183.00', 0, 1234),
('h900688630', '1465.00', 0, 1234),
('h900719734', '1216.00', 0, 1234),
('h900742119', '1246.00', 0, 1234),
('h900766966', '1440.00', 0, 1234),
('h900770995', '1404.00', 0, 1234),
('h900818468', '1121.00', 0, 1234),
('h900826731', '1052.00', 0, 1234),
('h900848495', '1152.00', 0, 1234),
('h900854303', '1353.00', 0, 1234);

-- --------------------------------------------------------

--
-- Table structure for table `staff_details`
--

DROP TABLE IF EXISTS `staff_details`;
CREATE TABLE `staff_details` (
  `StaffId` varchar(6) NOT NULL,
  `StaffFirstName` varchar(50) NOT NULL,
  `StaffLastName` varchar(50) NOT NULL,
  `StaffEmail` varchar(50) NOT NULL,
  `StaffGender` varchar(1) NOT NULL,
  `StaffPassword` varchar(100) NOT NULL,
  `StaffCreateTime` timestamp NOT NULL DEFAULT current_timestamp(),
  `StaffTypeId` int(11) NOT NULL,
  `StaffStatus` int(11) NOT NULL DEFAULT 1,
  `BusinessId` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `staff_details`
--

INSERT INTO `staff_details` (`StaffId`, `StaffFirstName`, `StaffLastName`, `StaffEmail`, `StaffGender`, `StaffPassword`, `StaffCreateTime`, `StaffTypeId`, `StaffStatus`, `BusinessId`) VALUES
('s00001', 'test', '1', 'test1@test.com', 'M', 'test', '2020-03-15 20:45:10', 2, 1, 'B010'),
('s00002', 'test', '2', 'test2@test.com', 'F', 'test', '2020-03-16 00:00:01', 2, 1, 'B020'),
('s00003', 'test', '3', 'test3@test.com', 'M', 'test', '2020-03-16 00:00:59', 2, 1, 'B030');

-- --------------------------------------------------------

--
-- Table structure for table `staff_type`
--

DROP TABLE IF EXISTS `staff_type`;
CREATE TABLE `staff_type` (
  `StaffTypeId` int(11) NOT NULL,
  `StaffTypeDescription` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `staff_type`
--

INSERT INTO `staff_type` (`StaffTypeId`, `StaffTypeDescription`) VALUES
(1, 'Admin'),
(2, 'Menu Manager');

-- --------------------------------------------------------

--
-- Table structure for table `tempdeliveryinfo`
--

DROP TABLE IF EXISTS `tempdeliveryinfo`;
CREATE TABLE `tempdeliveryinfo` (
  `customerID` varchar(9) NOT NULL,
  `buildingID` int(11) NOT NULL,
  `roomNo` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tempdeliveryinfo`
--

INSERT INTO `tempdeliveryinfo` (`customerID`, `buildingID`, `roomNo`) VALUES
('s11123207', 10, 'ICT Q');

-- --------------------------------------------------------

--
-- Table structure for table `tempdeliverytime`
--

DROP TABLE IF EXISTS `tempdeliverytime`;
CREATE TABLE `tempdeliverytime` (
  `CustomerID` varchar(9) NOT NULL,
  `DeliveryTimeSlot` int(11) NOT NULL,
  `DeliveryDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tempdeliverytime`
--

INSERT INTO `tempdeliverytime` (`CustomerID`, `DeliveryTimeSlot`, `DeliveryDate`) VALUES
('s11123207', 9, '2020-03-24');

-- --------------------------------------------------------

--
-- Table structure for table `timeslots`
--

DROP TABLE IF EXISTS `timeslots`;
CREATE TABLE `timeslots` (
  `TimeslotId` int(11) NOT NULL,
  `TimeslotDescription` time NOT NULL,
  `ItemCategoryId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `timeslots`
--

INSERT INTO `timeslots` (`TimeslotId`, `TimeslotDescription`, `ItemCategoryId`) VALUES
(1, '11:00:00', 2),
(2, '11:15:00', 2),
(3, '11:30:00', 2),
(4, '11:45:00', 2),
(5, '12:00:00', 2),
(6, '12:15:00', 2),
(7, '12:30:00', 2),
(8, '12:45:00', 2),
(9, '13:00:00', 2);

-- --------------------------------------------------------

--
-- Table structure for table `transactional_log`
--

DROP TABLE IF EXISTS `transactional_log`;
CREATE TABLE `transactional_log` (
  `TransactionId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `UserId` int(11) NOT NULL,
  `UserName` varchar(50) NOT NULL,
  `UserFirstName` varchar(50) NOT NULL,
  `UserLastName` varchar(50) NOT NULL,
  `UserEmail` varchar(50) DEFAULT NULL,
  `UserPhone` varchar(10) DEFAULT NULL,
  `UserAddress` varchar(100) DEFAULT NULL,
  `UserGender` varchar(1) DEFAULT NULL,
  `UserPassword` varchar(100) DEFAULT NULL,
  `UserCreateTime` timestamp NOT NULL DEFAULT current_timestamp(),
  `UserStatus` int(11) NOT NULL DEFAULT 1,
  `RoleId` int(11) NOT NULL,
  `BusinessId` varchar(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`UserId`, `UserName`, `UserFirstName`, `UserLastName`, `UserEmail`, `UserPhone`, `UserAddress`, `UserGender`, `UserPassword`, `UserCreateTime`, `UserStatus`, `RoleId`, `BusinessId`) VALUES
(1, 'admin1', 'Super', 'Admin1', NULL, NULL, NULL, NULL, 'admin1', '2020-03-26 10:40:21', 1, 1, NULL),
(2, 'mm1', 'Menu', 'Manager1', 'mm1@gmail.com', NULL, NULL, NULL, 'mm1', '2020-03-26 12:08:54', 1, 2, 'B010'),
(3, 'mm2', 'Menu', 'Manager2', '', NULL, NULL, NULL, 'mm2', '2020-03-26 12:27:09', 1, 2, 'B020'),
(4, 'mm3', 'Menu', 'Manager3', NULL, NULL, NULL, NULL, 'mm3', '2020-03-26 12:27:57', 1, 2, 'B030'),
(6, 'ds1', 'Delivery', 'Staff1', '', '', '', NULL, 'ds1', '2020-03-26 13:05:55', 1, 4, NULL),
(11, 'ds9', 'test', '1', 'test1@gmail.com', '1231243', '', NULL, 'password', '2020-03-29 15:49:02', 1, 4, NULL),
(13, 'cs1', 'Cafeteria', 'Staff1', 'cafeteriastaff1@gmail.com', '1231238', '', NULL, 'cs1', '2020-03-29 16:24:54', 1, 3, 'B010'),
(15, 'cs2', 'test', '1', 'test1@gmail.com', '1234567', 'Suva', NULL, 'password', '2020-03-29 17:19:40', 1, 3, 'B010'),
(16, 'cs16', 'test', '2', 'test2@gmail.com', '1231233', 'Suva', NULL, 'password', '2020-03-29 17:25:00', 1, 3, 'B010'),
(17, 'cs17', 'test', '1', 'test1@gmail.com', '1234567', 'Suva', NULL, 'password', '2020-03-29 17:47:25', 1, 3, 'B020'),
(18, 'cs18', 'test', 'user2', 'testuser2@gmail.com', '1231231', 'Suva', NULL, 'password', '2020-03-30 10:45:24', 1, 3, 'B030'),
(22, 'mm19', 'John', 'Doe', 'jd@gmail.com', NULL, NULL, NULL, '1234', '2020-04-06 23:48:23', 1, 2, 'B10004'),
(28, 'cs23', 'test', 'delete', 'testdelete@gmail.com', '1231232', 'Suva', NULL, '1234', '2020-04-07 00:55:08', 1, 3, 'B010'),
(29, 'ds29', 'test', 'delete', 'testdelete@gmail.com', '2342342', 'Suva', NULL, '1234', '2020-04-07 03:24:08', 1, 4, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE `user_roles` (
  `RoleId` int(11) NOT NULL,
  `RoleName` varchar(20) NOT NULL,
  `RoleDescription` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`RoleId`, `RoleName`, `RoleDescription`) VALUES
(1, 'Admin', NULL),
(2, 'Menu Manager', NULL),
(3, 'Cafeteria Staff', NULL),
(4, 'Delivery Staff', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `building_info`
--
ALTER TABLE `building_info`
  ADD PRIMARY KEY (`BuildingId`);

--
-- Indexes for table `business_details`
--
ALTER TABLE `business_details`
  ADD PRIMARY KEY (`BusinessId`),
  ADD UNIQUE KEY `Id` (`Id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD KEY `cart_item_details` (`ItemId`) USING BTREE,
  ADD KEY `cart_customer_details` (`CustomerId`);

--
-- Indexes for table `credit_card_info`
--
ALTER TABLE `credit_card_info`
  ADD PRIMARY KEY (`CreditCardNumber`);

--
-- Indexes for table `customer_details`
--
ALTER TABLE `customer_details`
  ADD PRIMARY KEY (`CustomerId`),
  ADD UNIQUE KEY `Email` (`CustomerEmail`),
  ADD KEY `customer_details_building_info` (`BuildingId`);

--
-- Indexes for table `delivery_timeslot`
--
ALTER TABLE `delivery_timeslot`
  ADD PRIMARY KEY (`DeliveryTimeslotId`),
  ADD KEY `delivery_timeslot_timeslots` (`TimeslotId`);

--
-- Indexes for table `item_category`
--
ALTER TABLE `item_category`
  ADD PRIMARY KEY (`ItemCategoryId`);

--
-- Indexes for table `item_details`
--
ALTER TABLE `item_details`
  ADD PRIMARY KEY (`ItemId`),
  ADD KEY `item_details_business_details` (`BusinessId`),
  ADD KEY `item_details_item_type` (`ItemTypeId`),
  ADD KEY `item_details_meal_type` (`MealTypeId`),
  ADD KEY `item_details_item_category` (`ItemCategoryId`) USING BTREE;

--
-- Indexes for table `item_type`
--
ALTER TABLE `item_type`
  ADD PRIMARY KEY (`ItemTypeId`);

--
-- Indexes for table `meal_type`
--
ALTER TABLE `meal_type`
  ADD PRIMARY KEY (`MealTypeId`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`MainOrderId`);

--
-- Indexes for table `order_status`
--
ALTER TABLE `order_status`
  ADD PRIMARY KEY (`OrderStatusId`);

--
-- Indexes for table `payroll_info`
--
ALTER TABLE `payroll_info`
  ADD PRIMARY KEY (`PayrollId`);

--
-- Indexes for table `staff_details`
--
ALTER TABLE `staff_details`
  ADD PRIMARY KEY (`StaffId`),
  ADD KEY `staff_details_business_details` (`BusinessId`),
  ADD KEY `staff_details_staff_type` (`StaffTypeId`);

--
-- Indexes for table `staff_type`
--
ALTER TABLE `staff_type`
  ADD PRIMARY KEY (`StaffTypeId`);

--
-- Indexes for table `timeslots`
--
ALTER TABLE `timeslots`
  ADD PRIMARY KEY (`TimeslotId`),
  ADD KEY `timeslot_item_category` (`ItemCategoryId`);

--
-- Indexes for table `transactional_log`
--
ALTER TABLE `transactional_log`
  ADD PRIMARY KEY (`TransactionId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`UserId`),
  ADD UNIQUE KEY `UserName` (`UserName`),
  ADD KEY `users_user_roles` (`RoleId`),
  ADD KEY `user_business_details` (`BusinessId`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`RoleId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `business_details`
--
ALTER TABLE `business_details`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10014;

--
-- AUTO_INCREMENT for table `delivery_timeslot`
--
ALTER TABLE `delivery_timeslot`
  MODIFY `DeliveryTimeslotId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `item_category`
--
ALTER TABLE `item_category`
  MODIFY `ItemCategoryId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `item_details`
--
ALTER TABLE `item_details`
  MODIFY `ItemId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `item_type`
--
ALTER TABLE `item_type`
  MODIFY `ItemTypeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `meal_type`
--
ALTER TABLE `meal_type`
  MODIFY `MealTypeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `MainOrderId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `staff_type`
--
ALTER TABLE `staff_type`
  MODIFY `StaffTypeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `UserId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `RoleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cart`
--
ALTER TABLE `cart`
  ADD CONSTRAINT `cart_customer_details` FOREIGN KEY (`CustomerId`) REFERENCES `customer_details` (`CustomerId`),
  ADD CONSTRAINT `cart_item_details` FOREIGN KEY (`ItemId`) REFERENCES `item_details` (`ItemId`);

--
-- Constraints for table `customer_details`
--
ALTER TABLE `customer_details`
  ADD CONSTRAINT `customer_details_building_info` FOREIGN KEY (`BuildingId`) REFERENCES `building_info` (`BuildingId`);

--
-- Constraints for table `delivery_timeslot`
--
ALTER TABLE `delivery_timeslot`
  ADD CONSTRAINT `delivery_timeslot_timeslots` FOREIGN KEY (`TimeslotId`) REFERENCES `timeslots` (`TimeslotId`);

--
-- Constraints for table `item_details`
--
ALTER TABLE `item_details`
  ADD CONSTRAINT `item_details_business_details` FOREIGN KEY (`BusinessId`) REFERENCES `business_details` (`BusinessId`),
  ADD CONSTRAINT `item_details_item_category` FOREIGN KEY (`ItemCategoryId`) REFERENCES `item_category` (`ItemCategoryId`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `item_details_item_type` FOREIGN KEY (`ItemTypeId`) REFERENCES `item_type` (`ItemTypeId`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `item_details_meal_type` FOREIGN KEY (`MealTypeId`) REFERENCES `meal_type` (`MealTypeId`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `staff_details`
--
ALTER TABLE `staff_details`
  ADD CONSTRAINT `staff_details_business_details` FOREIGN KEY (`BusinessId`) REFERENCES `business_details` (`BusinessId`),
  ADD CONSTRAINT `staff_details_staff_type` FOREIGN KEY (`StaffTypeId`) REFERENCES `staff_type` (`StaffTypeId`);

--
-- Constraints for table `timeslots`
--
ALTER TABLE `timeslots`
  ADD CONSTRAINT `timeslot_item_category` FOREIGN KEY (`ItemCategoryId`) REFERENCES `item_category` (`ItemCategoryId`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `user_business_details` FOREIGN KEY (`BusinessId`) REFERENCES `business_details` (`BusinessId`),
  ADD CONSTRAINT `users_user_roles` FOREIGN KEY (`RoleId`) REFERENCES `user_roles` (`RoleId`);
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
