<!-- Login Form -->
<div class="accountbox-wrapper">
    <div class="accountbox text-left">
        <ul class="nav accountbox__filters" id="myTab" role="tablist">
            <li>
                <a class="active" id="log-tab" data-toggle="tab" href="#log" role="tab" aria-controls="log" aria-selected="true">Login</a>
            </li>
            <li>
                <a id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Register</a>
            </li>
        </ul>
        <div class="accountbox__inner tab-content" id="myTabContent">
            <div class="accountbox__login tab-pane fade show active" id="log" role="tabpanel" aria-labelledby="log-tab">
                <form class="login100-form validate-form-login ajax" id="loginform" name="loginform" method="POST">

                    <div class="wrap-input100 validate-input-login" data-validate="Valid username is required: Employee/Student ID">
                        <input class="input100" type="text" name="username" id="username" placeholder="Username" required maxlength="9" >
                        <span class="focus-input100"></span>
                        <span class="symbol-input100">
                            <i class="fa fa-user" aria-hidden="true"></i>
                        </span>
                    </div>

                    <div class="wrap-input100 validate-input-login" data-validate="Password is required">
                        <input class="input100" type="password" name="userPassword" id="userPassword" placeholder="Password" required maxlength="50" >
                        <span class="focus-input100"></span>
                        <span class="symbol-input100">
                            <i class="fa fa-lock" aria-hidden="true"></i>
                        </span>
                    </div>
                    <div class="single-input">
                        <button class="food__btn" class="SubmitFormForLogin" onclick="_Login()"><span>LogIn</span></button>
                    </div>
                </form>
            </div>
            <div class="accountbox__register tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                <form class="login100-form validate-form ajax" id="signupform" name="signupform" method="post">
                    <div class="wrap-input100 validate-input" data-validate="Valid FistName is required: John">
                        <input class="input100" type="text" name="fistName" id="fistName" placeholder="FistName" required maxlength=50 >
                        <span class="focus-input100"></span>
                        <span class="symbol-input100">
                            <i class="fa fa-user" aria-hidden="true"></i>
                        </span>
                    </div>
                    <div class="wrap-input100 validate-input" data-validate="Valid SurName is required: Doe">
                        <input class="input100" type="text" name="surName" id="surName" placeholder="SurName" required maxlength=50>
                        <span class="focus-input100"></span>
                        <span class="symbol-input100">
                            <i class="fa fa-user" aria-hidden="true"></i>
                        </span>
                    </div>
                    <div class="wrap-input100 validate-input" data-validate="Valid EmployeeID/StudentID is required: s1111111">
                        <input class="input100" type="text" name="userID" id="userID" placeholder="EmployeeID/StudentID" required maxlength=9>
                        <span class="focus-input100"></span>
                        <span class="symbol-input100">
                            <i class="fa fa-user" aria-hidden="true"></i>
                        </span>
                    </div>
                    <div class="wrap-input100 validate-input" data-validate="Valid Email is required: example@example.com" maxlength=50>
                        <input class="input100" type="text" name="email" id="email" placeholder="Email" required>
                        <span class="focus-input100"></span>
                        <span class="symbol-input100">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                        </span>
                    </div>
                    <div class="wrap-input100 validate-input" data-validate="Valid Phone Number is required: 555 5555" maxlength=7>
                        <input class="input100" type="text" name="phoneNumber" id="phoneNumber" placeholder="phone Number" required>
                        <span class="focus-input100"></span>
                        <span class="symbol-input100">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                        </span>
                    </div>
                    <div class="wrap-input100 validate-input" data-validate="Password is required">
                        <input class="input100" type="password" name="password" id="password" placeholder="Password" required maxlength=50>
                        <span class="focus-input100"></span>
                        <span class="symbol-input100">
                            <i class="fa fa-lock" aria-hidden="true"></i>
                        </span>
                    </div>
                    <div class="wrap-input100 validate-input" data-validate="Password is required">
                        <input class="input100" type="password" name="confrimPassword" id="confrimPassword" placeholder="Confirm Password" required maxlength=50>
                        <span class="focus-input100"></span>
                        <span class="symbol-input100">
                            <i class="fa fa-lock" aria-hidden="true"></i>
                        </span>
                    </div>
                    <label style="margin-vertical: 1em;color:#000;">Select Default Delivery Location</label>
                    <div class="row">
                        <div class="col-md-6 col-12 mb--20">                                 
                            <select name="DeliverySelector" id="DeliverySelector">
                                <?php
                                if($retvalue = mysqli_query($connect,$getDeliveryLocationList)){
                                    while($row=mysqli_fetch_array($retvalue)){
                                        ?> <option  value="<?php echo $row["BuildingId"];?>"><?php echo $row["BuildingDescription"] ?></option><?php
                                    }
                                }
                                mysqli_close($connect);
                                ?>
                            </select>
                        </div>
                        <div class="wrap-input100 validate-input col-md-6 col-12 mb--20" data-validate="Valid Building Number is required.">
                            <input style="border-radius: 20;" type="text" name="roomNo" id="roomNo" placeholder="Room No." maxlength=10>
                            <span class="focus-input100"></span>
                        </div>
                    </div>
                    <label style="margin-bottom: 1em;color:#000;">Select Gender *</label>
                        <input style="margin-right: 1.5em;" type="radio" id="radioMale" name="demo01" value="M" checked="checked"/><label for="radioMale">Male</label>
                        <input style="margin-right: 1.5em;" type="radio" id="radio0Female" name="demo01" value="F" /><label for="radio0Female">Female</label>
                    <div class="single-input">
                        <button type="submit" class="food__btn" onclick="_SignIn()"><span>Sign Up</span></button>
                    </div>
                </form>
            </div>
            <span class="accountbox-close-button"><i class="zmdi zmdi-close"></i></span>
        </div>
    </div>
</div>
<!-- //Login Form -->