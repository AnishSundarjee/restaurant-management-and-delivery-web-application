 <!-- Start Footer Area -->
 <footer class="footer__area footer--1">
    <div class="footer__wrapper bg__cat--1 section-padding--lg">
        <div class="container">
            <div class="row">
                <!-- Start Single Footer -->
                <div class="col-md-6 col-lg-4 col-sm-12">
                    <div class="footer">
                        <h2 class="ftr__title">About Afredieti</h2>
                        <div class="footer__inner">
                            <div class="ftr__details">
                                <p>The true plessures in life begin with a single food delivery,</p>
                                <div class="ftr__address__inner">
                                    <div class="ftr__address">
                                        <div class="ftr__address__icon">
                                            <i class="zmdi zmdi-home"></i>
                                        </div>
                                        <div class="frt__address__details">
                                            <p>USP. ICT building TL3 Suva, Fiji</p>
                                        </div>
                                    </div>
                                    <div class="ftr__address">
                                        <div class="ftr__address__icon">
                                            <i class="zmdi zmdi-phone"></i>
                                        </div>
                                        <div class="frt__address__details">
                                            <p><a href="#">+679 555-5555</a></p>
                                            <p><a href="#">+679 555-5555</a></p>
                                        </div>
                                    </div>
                                    <div class="ftr__address">
                                        <div class="ftr__address__icon">
                                            <i class="zmdi zmdi-email"></i>
                                        </div>
                                        <div class="frt__address__details">
                                            <p><a href="#">bigboyvamps540@gmail.com</a></p>
                                        </div>
                                    </div>
                                </div>
                                <!-- <ul class="social__icon">
                                    <li><a href="#"><i class="zmdi zmdi-facebook"></i></a></li>
                                    <li><a href="#"><i class="zmdi zmdi-google"></i></a></li>
                                    <li><a href="#"><i class="zmdi zmdi-instagram"></i></a></li>
                                    <li><a href="#"><i class="zmdi zmdi-rss"></i></a></li>
                                </ul> -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Single Footer -->
                <!-- Start Single Footer -->
                <div class="col-md-6 col-lg-4 col-sm-12 sm--mt--40">
                    <div class="footer gallery">
                        <h2 class="ftr__title">Our Gallery</h2>
                        <div class="footer__inner">
                            <ul class="sm__gallery__list">
                                <li><a href="#"><img src="../images/gallery/sm-img/7.jpg" alt="gallery images"></a></li>
                                <li><a href="#"><img src="../images/gallery/sm-img/8.jpg" alt="gallery images"></a></li>
                                <li><a href="#"><img src="../images/gallery/sm-img/9.jpg" alt="gallery images"></a></li>
                                <li><a href="#"><img src="../images/gallery/sm-img/10.png" alt="gallery images"></a></li>
                                <li><a href="#"><img src="../images/gallery/sm-img/11.jpg" alt="gallery images"></a></li>
                                <li><a href="#"><img src="../images/gallery/sm-img/12.jpg" alt="gallery images"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- End Single Footer -->
                <!-- Start Single Footer -->
                <div class="col-md-6 col-lg-4 col-sm-12 md--mt--40 sm--mt--40">
                    <div class="footer">
                        <h2 class="ftr__title">Opening Time</h2>
                        <div class="footer__inner">
                            <ul class="opening__time__list">
                                <li>Monday<span>.......</span>9am to 11pm</li>
                                <li>Tuesday<span>.......</span>9am to 11pm</li>
                                <li>Wednesday<span>.......</span>9am to 11pm</li>
                                <li>Thursday<span>.......</span>9am to 11pm</li>
                                <li>Friday<span>.......</span>9am to 11pm</li>
                                <li>Saturday<span>.......</span>9am to 11pm</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- End Single Footer -->
                
            </div>
        </div>
    </div>
    <div class="copyright bg--theme">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="copyright__inner">
                        <div class="cpy__right--left">
                            <p>@All Right Reserved.<a href="https://freethemescloud.com/">Free themes Cloud</a></p>
                        </div>
                        <div class="cpy__right--right">
                            <a href="#">
                                <img src="../images/icon/shape/2.png" alt="payment images">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- End Footer Area -->