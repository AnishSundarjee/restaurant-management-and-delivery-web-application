 <!-- Start Header Area -->
 <header class="htc__header bg--white">
    <!-- Start Mainmenu Area -->
    <div id="sticky-header-with-topbar" class="mainmenu__wrap sticky__header">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-sm-4 col-md-6 order-1 order-lg-1">
                    <div class="logo">
                        <a href="index.php">
                            <img src="../images/logo/foody.png" alt="logo images">
                        </a>
                    </div>
                </div>
                <div class="col-lg-9 col-sm-4 col-md-2 order-3 order-lg-2">
                    <div class="main__menu__wrap">
                        <nav class="main__menu__nav d-none d-lg-block">
                            <ul class="mainmenu">
                                <li class="drop"><a href="index.php">Home</a></li>
                                <li class="drop"><a href="#">See Us</a>
                                    <ul class="dropdown__menu">
                                        <li><a href="about-us.php">About Us</a></li>
                                        <li><a href="contact.php">Contact</a></li>
                                    </ul>
                                </li>
                                <li class="drop"><a href="#">Menu Options</a>
                                    <ul class="dropdown__menu">
                                        <li><a href="menu-list.php">Menu List</a></li>
                                        <li><a href="resturantView.php">Business List</a></li>
                                    </ul>
                                </li>
                                
                                <?php
                                    if(isset($isLogedin)){
                                        if($isLogedin == '2') {
                                        ?>
                                            <li><a href="Cart.php">Cart</a></li>
                                        <?php
                                        }
                                    }
                                ?>
                                <?php
                                    if(isset($isLogedin)){
                                        if($isLogedin == '2') {
                                        ?>
                                            <li><a href="orderTracking.php">Order Tracking</a></li>
                                        <?php
                                        }
                                    }
                                ?>
                                
                                <?php
                                    if(isset($isLogedin)){
                                        if($isLogedin == '2') {
                                        ?>
                                            <li><a href="setupPayment.php">Profile</a></li>
                                        <?php
                                        }
                                    }
                                ?>
                            </ul>
                        </nav>
                        
                    </div>
                </div>
                <div class="col-lg-1 col-sm-4 col-md-4 order-2 order-lg-3">
                    <div class="header__right d-flex justify-content-end">
                        <?php
                            if(isset($isLogedin)){
                                if($isLogedin == '2') {
                                ?>
                                    <div class="log__in">
                                        <a href="../controller/logout.php">Logout  <i class="fa fa-close"></i></a>
                                    </div>
                                    <div class="shopping__cart">
                                        <a href="cart.php"><i class="zmdi zmdi-shopping-basket"></i></a>
                                        <?php
                                        if(isset($cartCountValue)){
                                            if($cartCountValue>0){
                                                ?>
                                                <div class="shop__qun">
                                                    <span><?php echo $cartCountValue;?></span>
                                                </div>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </div>
                                <?php
                                }
                                else{
                                    ?>
                                        <div class="log__in">
                                            <a class="accountbox-trigger" href="#">SignIn/SignUp  <i class="zmdi zmdi-account-o"></i></a>
                                        </div>
                                    <?php
                                }
                            }
                            else{
                                ?>
                                    <div class="log__in">
                                        <a class="accountbox-trigger" href="#">SignIn/SignUp<i class="zmdi zmdi-account-o"></i></a>
                                    </div>
                                <?php
                            }
                        ?>
                        
                    </div>
                </div>
            </div>
            <!-- Mobile Menu -->
            <div class="mobile-menu d-block d-lg-none"></div>
            <!-- Mobile Menu -->
        </div>
    </div>
    <!-- End Mainmenu Area -->
</header>
<!-- End Header Area -->